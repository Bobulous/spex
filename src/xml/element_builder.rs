use std::collections::HashMap;

use crate::{
    xml::Content, xml::Element, xml::LanguageTag, xml::Name, xml::ProcessingInstruction,
    xml::WhiteSpaceHandling,
};

pub struct ElementBuilder {
    pub name: Name,
    pub whitespace: Option<WhiteSpaceHandling>,
    pub lang: Option<LanguageTag>,
    pub attributes: HashMap<Name, Box<str>>,
    pub content: Vec<Content>,
}

impl ElementBuilder {
    pub(crate) fn new(name: Name) -> ElementBuilder {
        ElementBuilder {
            name,
            whitespace: None,
            lang: None,
            attributes: HashMap::new(),
            content: Vec::new(),
        }
    }

    // This method is only used for unit testing.
    #[cfg(test)]
    pub fn set_whitespace_handling(&mut self, whitespace: Option<WhiteSpaceHandling>) {
        self.whitespace = whitespace;
    }

    // This method is only used for unit testing.
    #[cfg(test)]
    pub fn set_language(&mut self, lang: Option<LanguageTag>) {
        self.lang = lang;
    }

    pub fn add_attribute<S>(&mut self, name: Name, value: S)
    where
        S: Into<Box<str>>,
    {
        self.attributes.insert(name, value.into());
    }

    pub fn add_child_element(&mut self, child: Element) {
        self.content.push(Content::Child(Box::new(child)));
    }

    pub fn add_text<S>(&mut self, text: S)
    where
        S: Into<Box<str>>,
    {
        self.content.push(Content::Text(text.into()));
    }

    pub fn add_processing_instruction(&mut self, pi: ProcessingInstruction) {
        self.content.push(Content::PI(Box::new(pi)));
    }
}
