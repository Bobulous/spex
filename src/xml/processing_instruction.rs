/**
Represents a processing instruction within an XML document or within an XML element.
*/
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct ProcessingInstruction {
    target: Box<str>,
    data: Box<str>,
}

impl ProcessingInstruction {
    pub(crate) fn new<S, T>(target: S, data: T) -> ProcessingInstruction
    where
        S: Into<Box<str>>,
        T: Into<Box<str>>,
    {
        ProcessingInstruction {
            target: target.into(),
            data: data.into(),
        }
    }

    /**
    Returns the target name of this processing instruction.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::XmlDocument, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <?document-instruction PIs don't have to be within the root element. ?>
    <root xmlns:prefix='example.com/some_namespace'>
        <?root-instruction \n\t But they can be within the root element. ?>
        <list>
            <?list-instruction Or, in fact, within any other element.?>
            <item>whatever</item>
        </list>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        if let Some(doc_pi) = xml_doc.processing_instructions().next() {
            assert_eq!(doc_pi.target(), "document-instruction");
        }
        # else {panic!("Expected document PI to exist!");}

        let root = xml_doc.root();
        if let Some(root_pi) = root.processing_instructions().next() {
            assert_eq!(root_pi.target(), "root-instruction");
        }
        # else {panic!("Expected root PI to exist!");}

        let list = root.req("list").element()?;
        if let Some(list_pi) = list.processing_instructions().next() {
            assert_eq!(list_pi.target(), "list-instruction");
        }
        # else {panic!("Expected list element PI to exist!");}

        Ok(())
    }
    ```
    */
    pub fn target(&self) -> &str {
        self.target.as_ref()
    }

    /**
    Returns the payload data of this processing instruction.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::XmlDocument, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <?document-instruction PIs don't have to be within the root element. ?>
    <root xmlns:prefix='example.com/some_namespace'>
        <?root-instruction \n\t But they can be within the root element. ?>
        <list>
            <?list-instruction Or, in fact, within any other element.?>
            <item>whatever</item>
        </list>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        if let Some(doc_pi) = xml_doc.processing_instructions().next() {
            assert_eq!(doc_pi.data(), "PIs don't have to be within the root element. ");
        }
        # else {panic!("Expected document PI to exist!");}

        let root = xml_doc.root();
        if let Some(root_pi) = root.processing_instructions().next() {
            // Note that *all* whitespace between the target name and the start of the actual
            // data is discarded, but any whitespace at the end of the data is preserved.
            assert_eq!(root_pi.data(), "But they can be within the root element. ");
        }
        # else {panic!("Expected root PI to exist!");}

        let list = root.req("list").element()?;
        if let Some(list_pi) = list.processing_instructions().next() {
            assert_eq!(list_pi.data(), "Or, in fact, within any other element.");
        }
        # else {panic!("Expected list element PI to exist!");}

        Ok(())
    }
    ```
    */
    pub fn data(&self) -> &str {
        self.data.as_ref()
    }
}
