use std::io::Error;

use crate::extraction::ExtendedLanguageRange;

// Crude representation of an xml:lang language tag as described by RFC-4646:
// https://datatracker.ietf.org/doc/html/rfc4646
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct LanguageTag {
    tag: Box<str>,
}

impl LanguageTag {
    pub(crate) fn new<S>(tag: S) -> Result<LanguageTag, Error>
    where
        S: AsRef<str>,
    {
        let s = tag.as_ref();
        if s.is_empty() {
            Err(Error::new(
                std::io::ErrorKind::InvalidData,
                "LanguageTag cannot be empty.",
            ))
        } else {
            Ok(LanguageTag {
                tag: tag.as_ref().to_string().into_boxed_str(),
            })
        }
    }

    /**
    Returns the value of this language tag, as it would have appeared within an `xml:lang`
    property within the original XML.
    */
    pub fn value(&self) -> &str {
        self.tag.as_ref()
    }

    /**
    Reports on whether this LanguageTag satisfies the given ExtendedLanguageRange.

    Filtering works using "extended filtering" as described in
    [RFC-4647](https://datatracker.ietf.org/doc/html/rfc4647#section-3.3.2).
    */
    pub fn satisfies_filter(&self, range: &ExtendedLanguageRange<'_>) -> bool {
        let subtags: Vec<&str> = self.tag.as_ref().split('-').collect();
        Self::lang_subtags_satisfy_range(&subtags, range)
    }

    fn lang_subtags_satisfy_range(subtags: &[&str], range: &ExtendedLanguageRange<'_>) -> bool {
        let mut subtag_index = 0;
        'range_loop: for range_part in range.subtags() {
            while let Some(lang_part) = subtags.get(subtag_index) {
                if subtag_index == 0 {
                    if *range_part != "*" && !range_part.eq_ignore_ascii_case(lang_part) {
                        return false;
                    }
                    subtag_index += 1;
                    continue 'range_loop;
                }
                if range_part.eq_ignore_ascii_case(lang_part) {
                    subtag_index += 1;
                    continue 'range_loop;
                }
                if lang_part.len() == 1 {
                    // Singleton
                    return false;
                }
                subtag_index += 1;
            }
            // The language range is too specific for the language tag, so no match.
            return false;
        }
        true
    }

    /**
    Reports on whether this LanguageTag satisfies **any** ExtendedLanguageRange within the given
    collection.

    Filtering works using "extended filtering" as described in
    [RFC-4647](https://datatracker.ietf.org/doc/html/rfc4647#section-3.3.2).
    */
    pub fn satisfies_any_filter<'a, I: IntoIterator<Item = &'a &'a ExtendedLanguageRange<'a>>>(
        &self,
        ranges: I,
    ) -> bool {
        let subtags: Vec<&str> = self.tag.as_ref().split('-').collect();
        for range in ranges {
            if Self::lang_subtags_satisfy_range(&subtags, range) {
                return true;
            }
        }
        false
    }
}

#[cfg(test)]
mod tests {
    use std::io::Error;

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    type TestOut = Result<(), Error>;

    #[test]
    fn empty_language_tag() -> TestOut {
        let tag = LanguageTag::new("");
        assert!(tag.is_err());
        Ok(())
    }

    #[test]
    fn satisfies_filter_rfc4647_example_extended_matches() -> TestOut {
        let lang_range = ExtendedLanguageRange::new("de-*-DE").unwrap();
        let matching_lang_tags = &[
            "de-DE",
            "de-de",
            "de-Latn-DE",
            "de-Latf-DE",
            "de-DE-x-goethe",
            "de-Latn-DE-1996",
            "de-Deva-DE",
        ];
        for example in matching_lang_tags {
            let tag = LanguageTag::new(example)?;
            assert!(tag.satisfies_filter(&lang_range));
        }
        Ok(())
    }

    #[test]
    fn satisfies_filter_rfc4647_example_extended_matches_synonym() -> TestOut {
        let lang_range = ExtendedLanguageRange::new("de-DE").unwrap();
        let matching_lang_tags = &[
            "de-DE",
            "de-de",
            "de-Latn-DE",
            "de-Latf-DE",
            "de-DE-x-goethe",
            "de-Latn-DE-1996",
            "de-Deva-DE",
        ];
        for example in matching_lang_tags {
            let tag = LanguageTag::new(example)?;
            assert!(tag.satisfies_filter(&lang_range));
        }
        Ok(())
    }

    #[test]
    fn satisfies_filter_rfc4647_example_extended_mismatches() -> TestOut {
        let lang_range = ExtendedLanguageRange::new("de-*-DE").unwrap();
        let non_matching_lang_tags = &["de", "de-x-de", "de-Deva"];
        for example in non_matching_lang_tags {
            let tag = LanguageTag::new(example)?;
            println!(
                "Testing range \"{:?}\" against language tag \"{}\".",
                lang_range, example
            );
            assert!(!tag.satisfies_filter(&lang_range));
        }
        Ok(())
    }

    #[test]
    fn satisfies_filter_rfc4647_example_extended_mismatches_synonym() -> TestOut {
        let lang_range = ExtendedLanguageRange::new("de-DE").unwrap();
        let non_matching_lang_tags = &["de", "de-x-de", "de-Deva"];
        for example in non_matching_lang_tags {
            let tag = LanguageTag::new(example)?;
            println!(
                "Testing range \"{:?}\" against language tag \"{}\".",
                &lang_range, example
            );
            assert!(!tag.satisfies_filter(&lang_range));
        }
        Ok(())
    }

    #[test]
    fn satisfies_filter_extended_matches_wildcard() -> TestOut {
        let lang_range = ExtendedLanguageRange::new("*").unwrap();
        let mixed_lang_tags = &[
            "de-DE",
            "de-de",
            "de-Latn-DE",
            "de-Latf-DE",
            "de-DE-x-goethe",
            "de-Latn-DE-1996",
            "de-Deva-DE",
            "de",
            "de-x-de",
            "de-Deva",
        ];
        for example in mixed_lang_tags {
            let tag = LanguageTag::new(example)?;
            assert!(tag.satisfies_filter(&lang_range));
        }
        Ok(())
    }

    #[test]
    fn satisfies_filter_extended_matches_wildcard_first_position() -> TestOut {
        // Warning: this unit test is based on my understanding of the effect of
        // a wildcard in the first subtag of an extended language range. RFC-4647
        // does not contain a specific example on which a unit test can be based.
        let lang_range = ExtendedLanguageRange::new("*-DE").unwrap();
        let mixed_lang_tags = &[
            "de-DE",
            "de-de",
            "de-Latn-DE",
            "de-Latf-DE",
            "de-DE-x-goethe",
            "de-Latn-DE-1996",
            "de-Deva-DE",
        ];
        for example in mixed_lang_tags {
            let tag = LanguageTag::new(example)?;
            assert!(tag.satisfies_filter(&lang_range));
        }
        Ok(())
    }

    #[test]
    fn satisfies_filter_extended_mismatches_wildcard_first_position() -> TestOut {
        // Warning: this unit test is based on my understanding of the effect of
        // a wildcard in the first subtag of an extended language range. RFC-4647
        // does not contain a specific example on which a unit test can be based.
        let lang_range = ExtendedLanguageRange::new("*-DE").unwrap();
        let mixed_lang_tags = &["de", "de-x-de", "de-Deva"];
        for example in mixed_lang_tags {
            let tag = LanguageTag::new(example)?;
            assert!(!tag.satisfies_filter(&lang_range));
        }
        Ok(())
    }

    #[test]
    fn satisfies_all_filters_true() -> TestOut {
        let english = ExtendedLanguageRange::new("en").unwrap();
        let svenska = ExtendedLanguageRange::new("sv").unwrap();
        let matching_lang_tags = &[
            "en",
            "en-GB",
            "en-US",
            "en-AU",
            "en-x-US",
            "en-a-bbb-x-a-ccc",
            "sv",
            "sv-SE",
            "sv-FI",
            "sv-AX",
        ];
        for example in matching_lang_tags {
            let tag = LanguageTag::new(example)?;
            assert!(tag.satisfies_any_filter(&[&english, &svenska]));
        }
        Ok(())
    }

    #[test]
    fn satisfies_all_filters_false() -> TestOut {
        let english = ExtendedLanguageRange::new("en").unwrap();
        let svenska = ExtendedLanguageRange::new("sv").unwrap();
        let rejected_lang_tags = &[
            "de",
            "de-CH",
            "de-Latg-1996",
            "de-CH-1996",
            "de-a-value",
            "es",
            "es-419",
            "fr-Latn-CA",
        ];
        for example in rejected_lang_tags {
            let tag = LanguageTag::new(example)?;
            assert!(!tag.satisfies_any_filter(&[&english, &svenska]));
        }
        Ok(())
    }

    #[test]
    fn satisfies_all_filters_false_array_reference() -> TestOut {
        let english = ExtendedLanguageRange::new("en").unwrap();
        let svenska = ExtendedLanguageRange::new("sv").unwrap();
        let languages = [&english, &svenska];
        let rejected_lang_tags = &[
            "de",
            "de-CH",
            "de-Latg-1996",
            "de-CH-1996",
            "de-a-value",
            "es",
            "es-419",
            "fr-Latn-CA",
        ];
        for example in rejected_lang_tags {
            let tag = LanguageTag::new(example)?;
            assert!(!tag.satisfies_any_filter(&languages));
        }
        Ok(())
    }

    #[test]
    fn satisfies_all_filters_false_vec_reference() -> TestOut {
        let english = ExtendedLanguageRange::new("en").unwrap();
        let svenska = ExtendedLanguageRange::new("sv").unwrap();
        let languages = vec![&english, &svenska];
        let rejected_lang_tags = &[
            "de",
            "de-CH",
            "de-Latg-1996",
            "de-CH-1996",
            "de-a-value",
            "es",
            "es-419",
            "fr-Latn-CA",
        ];
        for example in rejected_lang_tags {
            let tag = LanguageTag::new(example)?;
            assert!(!tag.satisfies_any_filter(&languages));
        }
        Ok(())
    }
}
