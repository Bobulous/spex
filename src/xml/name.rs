/**
Represents an "expanded name", consisting of a namespace name and a local part, used as the
name for an XML element or attribute.

See [Namespaces in XML 1.0](http://www.w3.org/TR/xml-names/) for the formal definition.
*/
#[derive(Eq, PartialEq, Clone, Hash, Debug)]
pub struct Name {
    local_part: Box<str>,
    namespace: Option<Box<str>>,
}

impl Name {
    pub(crate) fn new<S, T>(local_part: S, namespace: T) -> Name
    where
        S: Into<Box<str>>,
        T: Into<Box<str>>,
    {
        let namespace = namespace.into();
        Name {
            local_part: local_part.into(),
            namespace: if namespace.is_empty() {
                None
            } else {
                Some(namespace)
            },
        }
    }

    /**
    Returns the "local part" of this name.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::XmlDocument, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <root xmlns:prefix='example.com/some_namespace'>
        <child>some text</child>
        <prefix:child>some text</prefix:child>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();
        # let mut child_count = 0;
        for child in root.elements() {
            # child_count += 1;
            assert_eq!(child.name().local_part(), "child");
        }
        # assert_eq!(child_count, 2);
        Ok(())
    }
    ```
    */
    pub fn local_part(&self) -> &str {
        &self.local_part
    }

    /**
    Returns the "namespace name" of this name, or `None` if this name is not within a namespace
    (the namespace name is empty).

    Do not confuse this with the "namespace prefix". The prefixes used within the original XML
    are discarded by this processor, and cannot be retrieved. This method returns the "namespace
    name" which the prefix represented.

    # Examples

    Elements fall into a namespace if they or an ancestor element declared a default namespace
    (`xmlns=`) in the original XML. Or if the element name was prefixed with a declared
    namespace prefix (`xmlns:some_prefix=`) in the original XML.

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::XmlDocument, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <root xmlns='example.com/some_namespace' xmlns:prefix='example.com/some_namespace'>
        <prefix:child>explicit namespace taken from prefix</prefix:child>
        <child>inherits namespace from default namespace specified in root element</child>
        <other>also inherits namespace from default</other>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();
        # let mut child_count = 0;
        for child in root.elements() {
            # child_count += 1;
            assert_eq!(child.name().namespace(), Some("example.com/some_namespace"));
        }
        # assert_eq!(child_count, 3);
        Ok(())
    }
    ```

    Otherwise the element name is not within a namespace, and this method will return `None`.

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::XmlDocument, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <root xmlns:prefix='example.com/some_namespace'>
        <child>no default namespace, and no prefix used for this child element</child>
        <other>same as above</other>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();
        # let mut child_count = 0;
        for child in root.elements() {
            # child_count += 1;
            assert_eq!(child.name().namespace(), None);
        }
        # assert_eq!(child_count, 2);
        Ok(())
    }
    ```
    */
    pub fn namespace(&self) -> Option<&str> {
        match self.namespace {
            Some(_) => self.namespace.as_deref(),
            None => None,
        }
    }
}

impl From<&str> for Name {
    fn from(value: &str) -> Self {
        Name {
            local_part: value.to_string().into_boxed_str(),
            namespace: None,
        }
    }
}
