/*!
Provides an `Element` type which represents an XML element and its contents.
*/

use std::collections::HashMap;
use std::ops::Deref;

use crate::common::XmlError;
use crate::extraction::NameRef;
use crate::extraction::OptionalCursor;
use crate::extraction::PreCursor;
use crate::extraction::PresetNamespace;
use crate::extraction::RequiredCursor;
use crate::extraction::XmlPath;
use crate::xml::Content;
use crate::xml::ElementBuilder;
use crate::xml::LanguageTag;
use crate::xml::Name;
use crate::xml::ProcessingInstruction;
use crate::xml::WhiteSpaceHandling;

/**
Represents an XML element and all of its attributes and its content, including any child
elements which it contains.

This type is immutable once created, and can only be created by the [`XmlReader`] as part of
parsing an XML document into an [`XmlDocument`] structure.

# Examples

Once you've created an [`XmlDocument`] by reading an XML byte stream, you can get the root
element by calling `root()` which will give you a shared reference to an `Element` which
represents the root element of the XML document that was read. Then you can descend into any
child elements found within the root, and get references to any attributes, character data, and
processing instructions held within each element.

```
# use std::fs::File;
# use spex::{parsing::XmlReader, xml::XmlDocument, common::XmlError};
# fn main() {
#    // Panic if either step generates an error.
#    let xml_doc = read_xml().unwrap();
#    extract_xml(xml_doc).unwrap();
# }
#
// The XML parsing methods might throw an std::io::Error, so they go into their own method.
fn read_xml() -> Result<XmlDocument, std::io::Error> {
    let xml = "
<root date='2024-02-10T20:31Z'>
    <?sort by:id?>
    <child id='1'>some text content</child>
</root>
";
    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    xml_doc
}

// Most methods in Element are able to generate an XmlError, so put extraction code into a
// method which is able to return a Result carrying that type of error.
fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
    let root = xml_doc.root();

    // The root@date attribute may or may not appear, so use att_opt to check whether it exists.
    if let Some(root_date) = root.att_opt("date") {
        println!("XML document generation date: {}", root_date);
        # assert_eq!(root_date, "2024-02-10T20:31Z");
    }

    // The XML document design requires that a child element exists within the root element, so
    // so we use the `req` method to target it, then `element()` to ask for a reference to the
    // targeted element,  and then the `?` syntax to throw the XmlError which will occur if this
    // required element is not found.
    let child = root.req("child").element()?;

    // Similarly, the child@id attribute is also required by the XML document design, so we
    // use att_req to target it, and `?` to throw any XmlError which occurs if the attribute
    // is not found.
    let child_id = child.att_req("id")?;
    println!("Found child id {}", child_id);
    # assert_eq!(child_id, "1");

    // We also expect that the child element contains only simple text content (no child
    // elements and no processing instructions). So use `text()` to refer to that text, and use
    // the `?` syntax to throw the XmlError which will occur if the content of child is not
    // simple.
    let child_text = child.text()?;
    println!("child element text: \"{}\"", child_text);
    # assert_eq!(child_text, "some text content");

    Ok(())
}
```

[`XmlDocument`]: struct.XmlDocument.html
[`XmlReader`]: ../parsing/struct.XmlReader.html
*/
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Element {
    name: Name,
    whitespace: Option<WhiteSpaceHandling>,
    lang: Option<LanguageTag>,
    attributes: HashMap<Name, Box<str>>,
    content: Vec<Content>,
}

impl Element {
    pub(crate) fn new(builder: ElementBuilder) -> Element {
        let name = builder.name;
        let whitespace = builder.whitespace;
        let lang = builder.lang;
        let attributes = builder.attributes;
        let content = Self::optimise_content(builder.content);
        Element {
            name,
            whitespace,
            lang,
            attributes,
            content,
        }
    }

    fn optimise_content(content: Vec<Content>) -> Vec<Content> {
        if content.len() < 2 || !Self::consecutive_texts(&content) {
            return content;
        }
        let mut optimised = Vec::with_capacity(content.len() - 1);
        let mut previous_text: Option<String> = None;
        for c in content.into_iter() {
            match c {
                Content::Text(t) => match previous_text {
                    None => previous_text = Some(t.into_string()),
                    Some(mut p) => {
                        p.push_str(&t);
                        previous_text = Some(p);
                    }
                },
                o => {
                    match previous_text {
                        None => {}
                        Some(p) => {
                            optimised.push(Content::Text(p.into_boxed_str()));
                            previous_text = None;
                        }
                    }
                    optimised.push(o);
                }
            }
        }
        match previous_text {
            None => {}
            Some(p) => optimised.push(Content::Text(p.into_boxed_str())),
        }
        optimised
    }

    fn consecutive_texts(content: &[Content]) -> bool {
        let mut previous_was_text = false;
        for c in content.iter() {
            if matches!(c, Content::Text(_)) {
                if previous_was_text {
                    return true;
                }
                previous_was_text = true;
            } else {
                previous_was_text = false;
            }
        }
        false
    }

    /**
    Returns a reference to this element's name.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::XmlDocument, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <Poem xmlns='example.com/pxv'>
        <Line>I wandered as a cloud of some sort . . .  fluffy, I think.</Line>
        <Line>Floating high on something, maybe daffodils.</Line>
    </Poem>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();
        assert_eq!(root.name().local_part(), "Poem");
        assert_eq!(root.name().namespace(), Some("example.com/pxv"));
        Ok(())
    }
    ```
    */
    pub fn name(&self) -> &Name {
        &self.name
    }

    /**
    Reports on whether this element's name exactly matches the specified name.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::XmlDocument, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <root>
        <no_namespace>simple</no_namespace>
        <def_namespace xmlns='example.com/default'>default namespace</def_namespace>
        <pfx:prefixed xmlns:pfx='example.com/prefixed'>prefixed name</pfx:prefixed>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();
        # let mut found_first = false;
        # let mut found_second = false;
        # let mut found_third = false;

        for child in root.elements() {
            // Specifying the name of an element which is not in any namespace can be done by
            // providing just a single string slice.
            if child.is_named("no_namespace") {
                println!("Found the first type of child element!");
                # found_first = true;
            }

            // But if the element name is in a default namespace then you must specify the local
            // part of the name and the namespace name.
            if child.is_named(("def_namespace", "example.com/default")) {
                println!("Found the second type of child element!");
                # found_second = true;
            }

            // Similarly if the element name is in a explicit namespace (courtesy of a prefix
            // being used in the original XML) then you must specify the local part of the name
            // and the namespace name. Note that this is *NOT* the prefix. The prefix is
            // discarded by the `XmlReader` and only the resolved namespace name remains.
            if child.is_named(("prefixed", "example.com/prefixed")) {
                println!("Found the third type of child element!");
                # found_third = true;
            }
        }

        # assert!(found_first);
        # assert!(found_second);
        # assert!(found_third);
        Ok(())
    }
    ```
    */
    pub fn is_named<'a, 'b, N>(&'a self, name: N) -> bool
    where
        N: Into<NameRef<'b>>,
    {
        let name = name.into();
        self.name.local_part() == name.local_part() && self.name.namespace() == name.namespace()
    }

    /**
    Returns a reference to the language tag which holds sway over this element's text and
    attribute content.

    If no non-empty `xml:lang` was specified in the original XML for this element, and the
    parent of this element also lacks a language tag, then this method will return `None`.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::XmlDocument, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <root xml:lang='en-GB'>
        <child label='Something in English'>
            <first>Still in English</first>
            <second xml:lang='sv-SE'>Det här är något på svenska.</second>
            <third xml:lang='da-DK'>Det er noget på dansk.</third>
            <fourth xml:lang='no-NO'>Dette er noe på norsk.</fourth>
            <fifth xml:lang=''>who knows what this is?</fifth>
        </child>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();
        assert_eq!(root.language_tag().map(|lang| lang.value()), Some("en-GB"));

        let child = root.req("child").element()?;
        // Even though the XML for "child" did not specify an xml:lang, it inherits the
        // language of its parent element, so it has the same language as "root" does.
        assert_eq!(child.language_tag().map(|lang| lang.value()), Some("en-GB"));

        let first = child.req("first").element()?;
        // And again, the "first" element inherits from its parent, so it has the same language
        // as "child" and "root".
        assert_eq!(first.language_tag().map(|lang| lang.value()), Some("en-GB"));

        let second = child.req("second").element()?;
        // However, the XML for the "second" element did specify its own xml:lang, so its
        // language is not inherited from its parent.
        assert_eq!(second.language_tag().map(|lang| lang.value()), Some("sv-SE"));

        // Same for "third" and "fourth".
        let third = child.req("third").element()?;
        assert_eq!(third.language_tag().map(|lang| lang.value()), Some("da-DK"));
        let fourth = child.req("fourth").element()?;
        assert_eq!(fourth.language_tag().map(|lang| lang.value()), Some("no-NO"));

        let fifth = child.req("fifth").element()?;
        // However, because the XML for "fifth" contained xml:lang='' it clears the language
        // tag and so this method will return None.
        assert_eq!(fifth.language_tag().map(|lang| lang.value()), None);

        Ok(())
    }
    ```
    */
    pub fn language_tag(&self) -> Option<&LanguageTag> {
        self.lang.as_ref()
    }

    /**
    Returns the white space handling intention signalled by this element, or `None` if no
    intention was specified.

    If no `xml:space` was specified in the original XML for this element, and the ancestors of
    this element also lack any white space handling intention, then this method will return
    `None`.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <root>
        <credentials xml:space='preserve'>
            <username>John Smith</username>
            <password>pa55 w0rD</password>
            <note xml:space='default'>
            User frequently has a PICNIC.
            </note>
        </credentials>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();

        // By default, root gives no intention regarding white space handling, so this method
        // will return `None`.
        assert!(root.white_space_handling().is_none());

        // But the "credentials" element has specified a white space handling intention.
        let credentials = root.req("credentials").element()?;
        assert_eq!(credentials.white_space_handling(), Some(WhiteSpaceHandling::Preserve));

        // The "username" and "password" elements do not specify an intention, so they inherit
        // whatever white space handling intention is in effect on their parent.
        let username = credentials.req("username").element()?;
        assert_eq!(username.white_space_handling(), Some(WhiteSpaceHandling::Preserve));
        let password = credentials.req("password").element()?;
        assert_eq!(password.white_space_handling(), Some(WhiteSpaceHandling::Preserve));

        // The "note" element does explicitly specify a white space handling intention, so it
        // does not inherit from its parent.
        let note = credentials.req("note").element()?;
        assert_eq!(note.white_space_handling(), Some(WhiteSpaceHandling::Default));

        Ok(())
    }
    ```
    */
    pub fn white_space_handling(&self) -> Option<WhiteSpaceHandling> {
        self.whitespace
    }

    // This method is only used for unit testing.
    #[cfg(test)]
    pub(crate) fn attributes(&self) -> impl Iterator<Item = (&Name, &str)> {
        self.attributes.iter().map(|(n, v)| (n, v.as_ref()))
    }

    /**
    Returns the value of the specified **optional** attribute, or `None` if no such attribute
    exists within this element. This method should be used where the XML document structure
    states that this attribute is optional (may or may not exist within this element type) and
    you do not consider it an error for it to be absent.

    If the attribute name is not within a namespace then `target_name` can be a single string
    slice. But if the attribute name is within a namespace, then `target_name` must be a tuple
    of two string slices: the first being the local part of the name, and the second being the
    namespace name (**NOT** the prefix, but the namespace name referred to by the prefix).

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <root xmlns='example.com/default_namespace' xmlns:prefix='example.com/other_namespace'>
        <demo one='no namespace' prefix:one='within namespace'/>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();
        let demo = root.pre_ns("example.com/default_namespace").req("demo").element()?;

        // Note that an unprefixed attribute never inherits the default namespace, so to target
        // the attribute "one" which has no prefix in the original XML, we just pass the local
        // name "one".
        let no_namespace_one = demo.att_opt("one");
        assert_eq!(no_namespace_one, Some("no namespace"));

        // The prefixed attribute "prefix:one" belongs within the namespace named
        // "example.com/other_namespace" so we have to specify that namespace name in order to
        // target that attribute.
        let within_namespace_one = demo.att_opt(("one", "example.com/other_namespace"));
        assert_eq!(within_namespace_one, Some("within namespace"));

        Ok(())
    }
    ```
    */
    pub fn att_opt<'a, 'b, N>(&'a self, target_name: N) -> Option<&'a str>
    where
        N: Into<NameRef<'b>>,
    {
        let target_name = target_name.into();
        let name = target_name.local_part();
        // If the namespace is an (explicitly) empty string then convert it to None.
        let namespace = target_name.namespace().filter(|&n| !n.is_empty());
        // TODO: CONSIDER JUST USING THE HashMap FOR ATTRIBUTE NAME LOOKUPS !!!
        for (full_name, value) in &self.attributes {
            if full_name.local_part() == name && full_name.namespace() == namespace {
                return Some(value.as_ref());
            }
        }
        None
    }

    /**
    Returns the value of the specified **required** attribute. This method should be used where
    the XML document structure promises that this attribute will exist, so that you consider it
    an error if the attribute is not found within this element.

    If the attribute name is not within a namespace then `target_name` can be a single string
    slice. But if the attribute name is within a namespace, then `target_name` must be a tuple
    of two string slices: the first being the local part of the name, and the second being the
    namespace name (**NOT** the prefix, but the namespace name referred to by the prefix).

    # Errors

    This method will return an [`XmlError`] if this element does not contain an attribute with the
    exact given name.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <root xmlns='example.com/default_namespace' xmlns:prefix='example.com/other_namespace'>
        <demo one='no namespace' prefix:one='within namespace'/>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();
        let demo = root.pre_ns("example.com/default_namespace").req("demo").element()?;

        // Note that an unprefixed attribute never inherits the default namespace, so to target
        // the attribute "one" which has no prefix in the original XML, we just pass the local
        // name "one".
        let no_namespace_one = demo.att_req("one")?;
        assert_eq!(no_namespace_one, "no namespace");

        // The prefixed attribute "prefix:one" belongs within the namespace named
        // "example.com/other_namespace" so we have to specify that namespace name in order to
        // target that attribute.
        let within_namespace_one = demo.att_req(("one", "example.com/other_namespace"))?;
        assert_eq!(within_namespace_one, "within namespace");

        Ok(())
    }
    ```

    [`XmlError`]: ../common/enum.XmlError.html
    */
    pub fn att_req<'a, 'b, N>(&'a self, target_name: N) -> Result<&'a str, XmlError>
    where
        N: Into<NameRef<'b>>,
    {
        let target_name = target_name.into();
        let name = target_name.local_part();
        // If the namespace is an (explicitly) empty string then convert it to None.
        let namespace = target_name.namespace().filter(|&n| !n.is_empty());
        // TODO: CONSIDER JUST USING THE HashMap FOR ATTRIBUTE NAME LOOKUPS !!!
        for (full_name, value) in &self.attributes {
            if full_name.local_part() == name && full_name.namespace() == namespace {
                return Ok(value.as_ref());
            }
        }
        Err(XmlError::AttributeAbsent(
            format!("Required attribute {:?} not found.", &target_name).into_boxed_str(),
        ))
    }

    // Leave this visible only within the crate, because it's too difficult to use Content.
    // TODO: Come up with some easy way to work with mixed content (text and elements together).
    pub(crate) fn content(&self) -> &[Content] {
        self.content.as_slice()
    }

    /**
    Returns an iterator of references to all processing instructions contained within this
    element.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <?document this processing instruction (PI) is not contained within the root element ?>
    <root>
        <?inside-root \n\t this PI is contained within the root element ?>
        <?same-again also contained within root ?>
        <child><?inner this PI is contained within the child element ?></child>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();
        let proc_ins: Vec<&ProcessingInstruction> = root.processing_instructions().collect();
        # assert_eq!(proc_ins.len(), 2);
        if let Some(first) = proc_ins.get(0) {
            assert_eq!(first.target(), "inside-root");
            // Note that all whitespace between the PI target and the PI data is skipped, but
            // any whitespace at the end of the data is returned.
            assert_eq!(first.data(), "this PI is contained within the root element ");
        }
        # else {panic!("Expected root PI to exist!");}

        if let Some(second) = proc_ins.get(1) {
            assert_eq!(second.target(), "same-again");
            assert_eq!(second.data(), "also contained within root ");
        }
        # else {panic!("Expected second root PI to exist!");}

        let child = root.req("child").element()?;
        let proc_ins: Vec<&ProcessingInstruction> = child.processing_instructions().collect();
        # assert_eq!(proc_ins.len(), 1);
        if let Some(inner) = proc_ins.get(0) {
            assert_eq!(inner.target(), "inner");
            assert_eq!(inner.data(), "this PI is contained within the child element ");
        }
        # else {panic!("Expected child element PI to exist!");}

        Ok(())
    }
    ```
    */
    pub fn processing_instructions(&self) -> impl Iterator<Item = &ProcessingInstruction> {
        self.content.iter().flat_map(|c| match c {
            Content::PI(p) => Some(p.deref()),
            _ => None,
        })
    }

    /**
    Returns an iterator of all child elements contained within this element.

    Usually you won't need this method, as you'll grab child elements by their specific name.
    But if for some reason you have to work with (terrible) XML which contains dynamic element
    names then you can use this method to simply iterate through all of them and then operate
    on each as needed.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, Element, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <root>
        <empty/>
        <one>1</one>
        <two>2</two>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();
        for e in root.elements() {
            println!("Found element named \"{:?}\" with text content: \"{}\".",
                e.name(), e.text()?);
        }
        # let vec: Vec<&Element> = root.elements().collect();
        # let empty = vec.get(0).unwrap();
        # assert_eq!(empty.name().local_part(), "empty");
        # assert_eq!(empty.name().namespace(), None);
        # assert_eq!(empty.text().unwrap(), "");
        # let one = vec.get(1).unwrap();
        # assert_eq!(one.name().local_part(), "one");
        # assert_eq!(one.name().namespace(), None);
        # assert_eq!(one.text().unwrap(), "1");
        # let two = vec.get(2).unwrap();
        # assert_eq!(two.name().local_part(), "two");
        # assert_eq!(two.name().namespace(), None);
        # assert_eq!(two.text().unwrap(), "2");

        Ok(())
    }
    ```
    */
    pub fn elements(&self) -> impl Iterator<Item = &Element> {
        self.content.iter().flat_map(|c| match c {
            Content::Child(e) => Some(e.deref()),
            _ => None,
        })
    }

    // Return option of attempting to retrieve *first* matching child element.
    // IMPORTANT: DO NOT MAKE THIS METHOD FULLY PUBLIC UNTIL DECIDING ON NameRef<'_> VERSUS Into<NameRef<'_>>
    pub(crate) fn o_child<'a, 'b, N>(&'a self, full_name: N) -> Option<&'a Element>
    where
        N: Into<NameRef<'b>>,
    {
        let full_name = full_name.into();
        let name = full_name.local_part();
        // If the namespace is an (explicitly) empty string then convert it to None.
        let namespace = full_name.namespace().filter(|&n| !n.is_empty());
        for c in &self.content {
            if let Content::Child(e) = c {
                if e.name().local_part() == name && e.name().namespace() == namespace {
                    return Some(e);
                }
            }
        }
        None
    }

    // Return result of expecting to retrieve *first* matching child element.
    // IMPORTANT: DO NOT MAKE THIS METHOD FULLY PUBLIC UNTIL DECIDING ON NameRef<'_> VERSUS Into<NameRef<'_>>
    pub(crate) fn r_child<'a, 'b, N>(&'a self, full_name: N) -> Result<&'a Element, XmlError>
    where
        N: Into<NameRef<'b>>,
    {
        let full_name = full_name.into();
        let name = full_name.local_part();
        // If the namespace is an (explicitly) empty string then convert it to None.
        let namespace = full_name.namespace().filter(|&n| !n.is_empty());
        for c in &self.content {
            if let Content::Child(e) = c {
                if e.name().local_part() == name && e.name().namespace() == namespace {
                    return Ok(e);
                }
            }
        }
        Err(XmlError::ElementAbsent(
            format!("Failed to find required child with name {:?}", full_name).into_boxed_str(),
        ))
    }

    /**
    Returns a `PreCursor` which simply holds the preset namespace provided, so that chained
    calls to methods such as `req`, `opt`, `all`, `first`, etc, know which namespace name should
    be presumed if no explicit namespace name is provided when calling those methods as part of
    this chain.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, Element, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <root xmlns='example.com/default_namespace'>
        <child>
            <grandchild>
                <great_grandchild>Hi, I'm Chad!</great_grandchild>
            </grandchild>
        </child>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // Store the namespace names as static constants, to make it easier to type and read them.
    const NS_DEF: &str = "example.com/default_namespace";

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();

        // Because the XML for the root element contained `xmlns=`, both the root element and
        // its descendants have their names defined within that default namespace, unless they
        // explicitly specify their own new namespace.

        // This means that we must specify the namespace name to correctly target elements
        // within this namespace.

        // THE HARD WAY: We can do this by specifying the namespace name explicitly every time
        // we call methods such as `req` and `opt`, using `(&str, &str)` tuples like this:
        let gg = root
            .req(("child", NS_DEF))
            .req(("grandchild", NS_DEF))
            .req(("great_grandchild", NS_DEF))
            .element()?;
        assert_eq!(gg.text()?, "Hi, I'm Chad!");

        // THE EASY WAY: But if the namespace is the same all the way through a chain of method
        // calls, it's easier to call `pre_ns` at the start of the method chain, so that you
        // preset the namespace that should be assumed, and don't need to repeat yourself.
        let gg = root
            .pre_ns(NS_DEF)
            .req("child")
            .req("grandchild")
            .req("great_grandchild")
            .element()?;
        assert_eq!(gg.text()?, "Hi, I'm Chad!");

        // BEWARE: The effect of `pre_ns` only lasts until you call `element()` and grab the
        // element which the cursor is currently targeting. So don't make a mistake like this
        // where you call `element` and then forget to specify the namespace name for any
        // additional method calls.
        let oops = root
            .pre_ns(NS_DEF)
            .req("child")
            .req("grandchild")
            .element()?
            // The call to `element()` has cleared the preset namespace, so the next call to
            // `opt` will have no preset namespace and won't find the element we want.
            .opt("great_grandchild")
            .element();
        assert!(oops.is_none());
        Ok(())
    }
    ```
    */
    pub fn pre_ns<'a>(&'a self, default_namespace: &'static str) -> PreCursor<'a> {
        let default_namespace = if default_namespace.is_empty() {
            None
        } else {
            Some(PresetNamespace::new(default_namespace))
        };
        PreCursor::new(self, default_namespace)
    }

    /**
    Returns an `OptionalCursor` and points it at the **first** child element with the given name
    if the child element exists, otherwise marks the cursor as having an "acceptably absent"
    state.

    Once you have an `OptionalCursor` you can make additional calls to `opt` to target the
    child element within the element the cursor is currently pointing at. Or you can call
    `req` to convert the cursor to a `MixedCursor` (which alters the return type by adding a
    Result into the mix, so that an error can be returned if a required child element is not
    found).

    Once you have pointed the cursor to the desired target, you can call `element()` to attempt
    to get the element being pointed at, or `text()` to attempt to get the text-only content
    of the element being pointed at, or `att_opt` or `att_req` to attempt to get the value of
    an attribute belonging to the element being pointed at.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, Element, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <catalogue>
        <metadata>
            <totalProductCount>1,796</totalProductCount>
        </metadata>
    </catalogue>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();

        // Because we use `opt` all the way through this method chain, we will will be working
        // with an `OptionalCursor` and `element()` will simply return an `Option<&Element>`.
        let totalProductCount = root.opt("metadata").opt("totalProductCount").element();
        assert!(totalProductCount.is_some());

        // But if we start with `opt` (to indicate that we don't consider it an error if the
        // "metadata" element is not found) and then chain in a call to `req` (to indicate that
        // we consider it an error if "totalProductionCount" is not found within a "metadata"
        // element) then we have converted the cursor to a `MixedCursor` and `element()` will
        // return a `Result<Option<&Element>, XmlError>` to handle both the option of an
        // acceptably absent element, and the error of an unacceptably absent element.
        let totalProductCount = root.opt("metadata").req("totalProductCount").element();
        assert!(matches!(totalProductCount, Ok(Some(_))));

        // Note that `req` does not generate an error if an optional or mixed cursor is not
        // pointing at anything. For example, the following will simply return `Ok(None)`
        // because the previous `opt` specifies an element which does not exist, so there is no
        // need to actually check for the existence of any `req` calls after that.
        let non_existent = root.opt("not_here").req("also_not_here").element();
        assert!(non_existent.is_ok());
        let payload = non_existent.unwrap();
        assert!(payload.is_none());
        Ok(())
    }
    ```
    */
    pub fn opt<'a, 'b, N>(&'a self, full_name: N) -> OptionalCursor<'a>
    where
        N: Into<NameRef<'b>>,
    {
        match self.o_child(full_name.into()) {
            Some(e) => OptionalCursor::new(e, None),
            None => OptionalCursor::absent(),
        }
    }

    /**
    Returns an `RequiredCursor` which points at the **first** child element with the given
    name, or holds an "invalid/error" state if this element does not contain a child with that
    name. Note that this method will not immediately cause an error to be generated if the
    named child element does not exist; instead the `RequiredCursor` state is set to "invalid",
    so that an error is only returned when a call is made to `element()`, `text()`, `att_opt` or
    `att_req`. This allows you to chain method calls together without checking for an error at
    every step.

    Once you have a `RequiredCursor` you can make additional calls to `req` to target the child
    element within the element the cursor is currently pointing at. Or you can call `opt` to
    convert the cursor to a `MixedCursor` (which alters the return type by adding an Option into
    the mix, so that an error is avoided if an optional child element is not found).

    Once you have pointed the cursor to the desired target, you can call `element()` to attempt
    to get the element being pointed at, or `text()` to attempt to get the text-only content
    of the element being pointed at, or `att_opt` or `att_req` to attempt to get the value of
    an attribute belonging to the element being pointed at.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, Element, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <catalogue>
        <metadata>
            <totalProductCount>1,796</totalProductCount>
        </metadata>
    </catalogue>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();

        // Because we use `req` all the way through this method chain, we will will be working
        // with an `RequiredCursor` and `element()` will return an `Result<&Element, XmlError>`.
        let totalProductCount = root.req("metadata").req("totalProductCount").element();
        assert!(totalProductCount.is_ok());

        // But if we start with `req` (to indicate that we consider it an error if the
        // "metadata" element is not found) and then chain in a call to `opt` (to indicate that
        // we do NOT consider it an error if "totalProductionCount" is not found within a
        // "metadata" element) then we have converted the cursor to a `MixedCursor` and
        // `element()` will return a `Result<Option<&Element>, XmlError>` to handle both the
        // error of an unacceptably absent element, and the option of an acceptably absent
        // element.
        let totalProductCount = root.req("metadata").opt("totalProductCount").element();
        assert!(matches!(totalProductCount, Ok(Some(_))));

        // Note that an error recorded by `req` is not cleared just because a subsequent call
        // is made to `opt`. For example, because the "not_found" element does not exist, `req`
        // sets the cursor state to invalid/error and so the call to `opt` is actually ignored
        // because there is no way of recovering from the absence of a required child element.
        let non_existent = root.req("not_found").opt("whatever").element();
        assert!(non_existent.is_err());
        Ok(())
    }
    ```
    */
    pub fn req<'a, 'b, N>(&'a self, full_name: N) -> RequiredCursor<'a>
    where
        N: Into<NameRef<'b>>,
    {
        match self.r_child(full_name.into()) {
            Ok(e) => RequiredCursor::new(e, None),
            Err(x) => {
                let message = match x {
                    XmlError::ElementAbsent(f) => f,
                    _ => "Failed to find required child element."
                        .to_string()
                        .into_boxed_str(),
                };
                RequiredCursor::invalid(message)
            }
        }
    }

    fn path<'e, 'n>(&'e self) -> XmlPath<'e, 'n>
    where
        'e: 'n,
    {
        XmlPath::new(self, None)
    }

    /**
    Returns an `XmlPath` which uses this element as its starting point and then considers all
    child elements with the given name.

    Subsequent calls can be made to `all`, `first`, `last`, and `nth` to tell the `XmlPath` to
    consider the child elements of the element(s) considered in the previous step. Once the
    `XmlPath` has been fully specified all the way down to the desired descendant element, you
    can call `iter()` to return an iterator over all of the end-of-path elements that match the
    criteria.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, Element, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <root>
        <planet>
            <name>Asphodel</name>
            <colony>
                <name>Median</name>
                <atmosphere_processing_plant>
                    <sublevel id='1'>clear</sublevel>
                    <sublevel id='2'>clear</sublevel>
                    <sublevel id='3'>clear</sublevel>
                </atmosphere_processing_plant>
            </colony>
        </planet>
        <planet>
            <name>Acheron</name>
            <colony>
                <name>Hadley's Hope</name>
                <atmosphere_processing_plant>
                    <sublevel id='1'>clear</sublevel>
                    <sublevel id='2'>clear</sublevel>
                    <sublevel id='3'>xenomorphs</sublevel>
                </atmosphere_processing_plant>
            </colony>
        </planet>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();

        let mut specimen_found = false;
        for sublevel in root
            .all("planet")
            .all("colony")
            .first("atmosphere_processing_plant")
            .all("sublevel")
            .iter() {
            if sublevel.text()? == "xenomorphs" {
                specimen_found = true;
            }
        }
        if specimen_found {
            println!("Bioweapon specimen located . . . preparing extraction team.");
        }
        # assert!(specimen_found);

        Ok(())
    }
    ```
    */
    pub fn all<'e, 'n, N>(&'e self, name: N) -> XmlPath<'e, 'n>
    where
        'e: 'n,
        N: Into<NameRef<'n>>,
    {
        let mut path = self.path();
        path.all(name);
        path
    }

    /**
    Returns an `XmlPath` which uses this element as its starting point and then considers only
    the first child element with the given name.

    Subsequent calls can be made to `all`, `first`, `last`, and `nth` to tell the `XmlPath` to
    consider the child elements of the element(s) considered in the previous step. Once the
    `XmlPath` has been fully specified all the way down to the desired descendant element, you
    can call `iter()` to return an iterator over all of the end-of-path elements that match the
    criteria.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, Element, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <doors>
        <door id='1'>
            <box id='a'>
                <pouch id='I'>gold</pouch>
                <pouch id='II'>silver</pouch>
                <pouch id='III'>bronze</pouch>
            </box>
            <box id='a'>
                <pouch id='IV'>diamond</pouch>
                <pouch id='V'>ruby</pouch>
                <pouch id='VI'>opal</pouch>
            </box>
        </door>
        <door id='2'>
            <box id='d'>
                <pouch id='VII'>McDonald</pouch>
                <pouch id='VIII'>Arby</pouch>
                <pouch id='IX'>Denny</pouch>
                <pouch id='X'>Wendy</pouch>
            </box>
            <box id='e'>
                <pouch id='XI'>doom</pouch>
                <pouch id='XII'>doom 2</pouch>
                <pouch id='XIII'>doom 3</pouch>
            </box>
        </door>
    </doors>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();

        // Because we have not used `all` anywhere along the XmlPath, the iterator can give us
        // at most one element reference, so we may as well just use `next()` to see whether the
        // path matched anything.
        if let Some(first_branch) = root
            .first("door")
            .first("box")
            .first("pouch")
            .iter()
            .next() {
            assert_eq!(first_branch.text()?, "gold");
        }
        # else {panic!("Expected first_branch to be Some!");}

        // But if we throw `all` into the mix, we may now get more than one matching element,
        // so we need a loop (or some other means of processing multiple element references).
        # let mut count = 0;
        for pouch in root.first("door").all("box").all("pouch").iter() {
            println!("Found \"{}\" in a pouch behind the first door!", pouch.text()?);
            # count += 1;
        }
        # assert_eq!(count, 6);

        Ok(())
    }
    ```
    */
    pub fn first<'e, 'n, N>(&'e self, name: N) -> XmlPath<'e, 'n>
    where
        'e: 'n,
        N: Into<NameRef<'n>>,
    {
        let mut path = self.path();
        path.first(name);
        path
    }

    /**
    Returns an `XmlPath` which uses this element as its starting point and then considers only
    the last child element with the given name.

    Subsequent calls can be made to `all`, `first`, `last`, and `nth` to tell the `XmlPath` to
    consider the child elements of the element(s) considered in the previous step. Once the
    `XmlPath` has been fully specified all the way down to the desired descendant element, you
    can call `iter()` to return an iterator over all of the end-of-path elements that match the
    criteria.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, Element, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <doors>
        <door id='1'>
            <box id='a'>
                <pouch id='I'>gold</pouch>
                <pouch id='II'>silver</pouch>
                <pouch id='III'>bronze</pouch>
            </box>
            <box id='a'>
                <pouch id='IV'>diamond</pouch>
                <pouch id='V'>ruby</pouch>
                <pouch id='VI'>opal</pouch>
            </box>
        </door>
        <door id='2'>
            <box id='d'>
                <pouch id='VII'>McDonald</pouch>
                <pouch id='VIII'>Arby</pouch>
                <pouch id='IX'>Denny</pouch>
                <pouch id='X'>Wendy</pouch>
            </box>
            <box id='e'>
                <pouch id='XI'>doom</pouch>
                <pouch id='XII'>doom 2</pouch>
                <pouch id='XIII'>doom 3</pouch>
            </box>
        </door>
    </doors>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();

        // Because we have not used `all` anywhere along the XmlPath, the iterator can give us
        // at most one element reference, so we may as well just use `next()` to see whether the
        // path matched anything.
        if let Some(last_branch) = root.last("door").last("box").last("pouch").iter().next() {
            assert_eq!(last_branch.text()?, "doom 3");
        }
        # else {panic!("Expected last_branch to be Some!");}

        // But if we throw `all` into the mix, we may now get more than one matching element,
        // so we need a loop (or some other means of processing multiple element references).
        # let mut count = 0;
        for pouch in root.last("door").all("box").all("pouch").iter() {
            println!("Found \"{}\" in a pouch behind the last door!", pouch.text()?);
            # count += 1;
        }
        # assert_eq!(count, 7);

        Ok(())
    }
    ```
    */
    pub fn last<'e, 'n, N>(&'e self, name: N) -> XmlPath<'e, 'n>
    where
        'e: 'n,
        N: Into<NameRef<'n>>,
    {
        let mut path = self.path();
        path.last(name);
        path
    }

    /**
    Returns an `XmlPath` which uses this element as its starting point and then considers only
    the n-th (where n==0 points to the first) child element with the given name.

    Subsequent calls can be made to `all`, `first`, `last`, and `nth` to tell the `XmlPath` to
    consider the child elements of the element(s) considered in the previous step. Once the
    `XmlPath` has been fully specified all the way down to the desired descendant element, you
    can call `iter()` to return an iterator over all of the end-of-path elements that match the
    criteria.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, Element, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <surface>
        <mine>
            <pit>
                <shaft>Alpha</shaft>
                <shaft>Bravo</shaft>
                <shaft>Gamma</shaft>
                <shaft>Delta</shaft>
            </pit>
        </mine>
        <mine>
            <pit>
                <shaft>Epsilon</shaft>
                <shaft>Zeta</shaft>
            </pit>
            <pit>
                <shaft>Eta</shaft>
                <shaft>Theta</shaft>
                <shaft>Iota</shaft>
            </pit>
            <pit>
                <shaft>Kappa</shaft>
                <shaft>Lambda</shaft>
            </pit>
        </mine>
        <mine>
            <pit>
                <shaft>Mu</shaft>
            </pit>
            <pit>
                <shaft>Nu</shaft>
            </pit>
        </mine>
    </surface>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();

        // Because we have not used `all` anywhere along the XmlPath, the iterator can give us
        // at most one element reference, so we may as well just use `next()` to see whether the
        // path matched anything.
        if let Some(mid_shaft) = root
            .nth("mine", 1)
            .nth("pit", 1)
            .nth("shaft", 1)
            .iter()
            .next() {
            assert_eq!(mid_shaft.text()?, "Theta");
        }
        # else {panic!("Expected mid_shaft to be Some!");}

        // But if we throw `all` into the mix, we may now get more than one matching element,
        // so we need a loop (or some other means of processing multiple element references).
        # let mut count = 0;
        for pouch in root.nth("mine", 1).nth("pit", 1).all("shaft").iter() {
            println!("Found mine shaft named \"{}\" in the second pit in the second mine!",
                pouch.text()?);
            # count += 1;
        }
        # assert_eq!(count, 3);

        Ok(())
    }
    ```
    */
    pub fn nth<'e, 'n, N>(&'e self, name: N, n: usize) -> XmlPath<'e, 'n>
    where
        'e: 'n,
        N: Into<NameRef<'n>>,
    {
        let mut path = self.path();
        path.nth(name, n);
        path
    }

    // Returns true if this element is empty or contains only text content.
    // This method is only used for unit testing.
    #[cfg(test)]
    pub(crate) fn contains_only_text(&self) -> bool {
        self.content.is_empty()
            || (self.content.len() == 1
                && matches!(self.content.first().unwrap(), Content::Text(_)))
    }

    /**
    Returns a reference to the simple, text-only content of this element. Do not use this method
    if the XML document structure states that this element is permitted to contain any child
    elements.

    # Errors

    This method will return an [`XmlError`] if this element contains child elements or
    processing instructions.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, Element, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <root>
        <child>simple text</child>
        <empty />
        <commented>Comments<!-- like this one --> have no effect on text content</commented>
        <mixed>text <b>mixed</b> in with <em>other</em> elements</mixed>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();

        let child = root.req("child").element()?;
        // Because the "child" element contains nothing but text content, we can use `text()`
        // without error.
        assert!(child.text().is_ok());
        assert_eq!(child.text()?, "simple text");

        let empty = root.req("empty").element()?;
        // It is not an error to call `text()` on an empty element. This method will simply
        // return a reference to an empty string slice.
        assert!(empty.text().is_ok());
        assert_eq!(empty.text()?, "");

        let commented = root.req("commented").element()?;
        // It's also fine to call `text()` on an element which contained XML comments within the
        // original XML source. Comments are completely ignored, so they have no effect on the
        // text content of the element.
        assert!(commented.text().is_ok());
        assert_eq!(commented.text()?, "Comments have no effect on text content");

        let mixed = root.req("mixed").element()?;
        // However, calling `text()` on an element which contains child elements will cause an
        // error to be returned, because there's no simple way to represent the text content.
        assert!(mixed.text().is_err());

        Ok(())
    }
    ```

    [`XmlError`]: ../common/enum.XmlError.html
    */
    pub fn text(&self) -> Result<&str, XmlError> {
        match self.content.len() {
            1 => match self.content.first().unwrap() {
                Content::Text(t) => Ok(t.as_ref()),
                _ => Err(XmlError::NotPureText),
            },
            0 => Ok(""),
            _ => Err(XmlError::NotPureText),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::io::Error;

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    type TestOut = Result<(), Error>;

    #[test]
    fn elements_root_empty() -> TestOut {
        let builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(builder);
        assert_eq!(root.elements().count(), 0);
        Ok(())
    }

    #[test]
    fn elements_root_one_child() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_text("Some text");
        root_builder.add_child_element(Element::new(child_builder));
        let root = Element::new(root_builder);
        assert_eq!(root.elements().count(), 1);
        Ok(())
    }

    #[test]
    fn elements_root_many_child() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        for _ in 0..3 {
            let mut child_builder = ElementBuilder::new(Name::new("child", ""));
            child_builder.add_text("Some text");
            root_builder.add_child_element(Element::new(child_builder));
        }
        let root = Element::new(root_builder);
        assert_eq!(root.elements().count(), 3);
        Ok(())
    }

    #[test]
    fn elements_root_mixed_content() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let pi = ProcessingInstruction::new("target".to_string(), "data".to_string());
        root_builder.add_processing_instruction(pi);
        for _ in 0..3 {
            let pi = ProcessingInstruction::new("othertarget".to_string(), "data".to_string());
            root_builder.add_processing_instruction(pi);
            let mut child_builder = ElementBuilder::new(Name::new("child", ""));
            child_builder.add_text("Some text");
            root_builder.add_child_element(Element::new(child_builder));
            root_builder.add_text("text content mingling with child elements!");
        }
        let pi = ProcessingInstruction::new("posttarget".to_string(), "data".to_string());
        root_builder.add_processing_instruction(pi);
        let root = Element::new(root_builder);
        assert_eq!(root.elements().count(), 3);
        Ok(())
    }

    #[test]
    fn content_root_consecutive_text() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_text("\n\t\t");
        root_builder.add_text("Some text from a <[CDATA[section!]");
        root_builder.add_text("]>");
        let root = Element::new(root_builder);
        assert!(root.contains_only_text());
        assert_eq!(root.content().len(), 1);
        assert_eq!(
            root.content()
                .iter()
                .filter(|c| matches!(c, Content::Text(_)))
                .count(),
            1
        );
        assert_eq!(
            root.text().unwrap(),
            "\n\t\tSome text from a <[CDATA[section!]]>"
        );
        Ok(())
    }

    #[test]
    fn content_root_non_consecutive_text() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_text("Text before processing instruction.");
        root_builder.add_processing_instruction(ProcessingInstruction::new(
            "target".to_string(),
            "data".to_string(),
        ));
        root_builder.add_text("Text after processing instruction.");
        let root = Element::new(root_builder);
        assert!(!root.contains_only_text());
        assert_eq!(root.content().len(), 3);
        assert_eq!(
            root.content()
                .iter()
                .filter(|c| matches!(c, Content::Text(_)))
                .count(),
            2
        );
        assert!(matches!(root.content()[1], Content::PI(_)));
        assert!(root.text().is_err());
        Ok(())
    }

    #[test]
    fn content_child_consecutive_text() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_text("\n\t\t");
        child_builder.add_text("Some text from a <[CDATA[section!]");
        child_builder.add_text("]>");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let child = root.r_child("child").unwrap();
        assert!(child.contains_only_text());
        assert_eq!(child.content().len(), 1);
        assert_eq!(
            child
                .content()
                .iter()
                .filter(|c| matches!(c, Content::Text(_)))
                .count(),
            1
        );
        assert_eq!(
            child.text().unwrap(),
            "\n\t\tSome text from a <[CDATA[section!]]>"
        );
        Ok(())
    }

    #[test]
    fn content_child_non_consecutive_text() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_text("Text before PI.");
        child_builder.add_processing_instruction(ProcessingInstruction::new(
            "target".to_string(),
            "data".to_string(),
        ));
        child_builder.add_text("Text after PI.");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let child = root.r_child("child").unwrap();
        assert!(!child.contains_only_text());
        assert_eq!(child.content().len(), 3);
        assert_eq!(
            child
                .content()
                .iter()
                .filter(|c| matches!(c, Content::Text(_)))
                .count(),
            2
        );
        assert!(matches!(child.content()[1], Content::PI(_)));
        assert!(child.text().is_err());
        Ok(())
    }

    #[test]
    fn name_root_and_child() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_text("Simple");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let child = root.r_child("child").unwrap();
        assert_eq!(root.name().local_part(), "root");
        assert_eq!(root.name().namespace(), None);
        assert_eq!(child.name().local_part(), "child");
        assert_eq!(child.name().namespace(), None);
        Ok(())
    }

    #[test]
    fn named_root_and_child() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_text("Simple");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let child = root.r_child("child").unwrap();
        assert!(root.is_named(NameRef::new("root", None)));
        assert!(child.is_named(NameRef::new("child", None)));
        assert!(!root.is_named(NameRef::new("child", None)));
        assert!(!child.is_named(NameRef::new("root", None)));
        Ok(())
    }

    #[test]
    fn language_tag() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.set_language(Some(LanguageTag::new("sv-SE").unwrap()));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.set_language(Some(LanguageTag::new("en-GB").unwrap()));
        child_builder.add_text("Simple");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let child = root.r_child("child").unwrap();
        assert_eq!(root.language_tag().map(|t| t.value()), Some("sv-SE"));
        assert_eq!(child.language_tag().map(|t| t.value()), Some("en-GB"));
        Ok(())
    }

    #[test]
    fn white_space_handling() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.set_whitespace_handling(None);
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.set_whitespace_handling(Some(WhiteSpaceHandling::Preserve));
        child_builder.add_text("Simple");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let child = root.r_child("child").unwrap();
        assert_eq!(root.white_space_handling(), None);
        assert_eq!(
            child.white_space_handling(),
            Some(WhiteSpaceHandling::Preserve)
        );
        Ok(())
    }

    #[test]
    fn attributes_root_none() -> TestOut {
        let root_builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(root_builder);
        assert_eq!(root.attributes().count(), 0);
        Ok(())
    }

    #[test]
    fn attributes_root_some() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_attribute(Name::new("id", ""), "1");
        let root = Element::new(root_builder);
        assert_eq!(root.attributes().count(), 1);
        let (att_name, att_value) = root.attributes().next().unwrap();
        assert_eq!(att_name.local_part(), "id");
        assert_eq!(att_name.namespace(), None);
        assert_eq!(att_value, "1");
        Ok(())
    }

    #[test]
    fn attributes_root_and_child_namespaced() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_attribute(Name::new("id", "example.com/outer"), "1");
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_attribute(Name::new("id", "example.com/inner"), "2");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        assert_eq!(root.attributes().count(), 1);
        let (att_name, att_value) = root.attributes().next().unwrap();
        assert_eq!(att_name.local_part(), "id");
        assert_eq!(att_name.namespace().unwrap(), "example.com/outer");
        assert_eq!(att_value, "1");

        let child = root.r_child("child").unwrap();
        assert_eq!(child.attributes().count(), 1);
        let (att_name, att_value) = child.attributes().next().unwrap();
        assert_eq!(att_name.local_part(), "id");
        assert_eq!(att_name.namespace().unwrap(), "example.com/inner");
        assert_eq!(att_value, "2");

        Ok(())
    }

    #[test]
    fn att_opt_root_and_child() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_attribute(Name::new("id", "example.com/outer"), "1");
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_attribute(Name::new("id", "example.com/inner"), "2");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let child = root.r_child("child").unwrap();
        assert_eq!(root.att_opt(("id", "example.com/outer")), Some("1"));
        assert_eq!(child.att_opt(("id", "example.com/inner")), Some("2"));
        assert_eq!(root.att_opt(("id", "example.com/inner")), None);
        assert_eq!(child.att_opt(("id", "example.com/outer")), None);
        assert_eq!(root.att_opt(("pid", "example.com/outer")), None);
        assert_eq!(child.att_opt(("pid", "example.com/inner")), None);
        assert_eq!(root.att_opt("id"), None);
        assert_eq!(child.att_opt("id"), None);
        Ok(())
    }

    #[test]
    fn att_req_root_and_child() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_attribute(Name::new("id", "example.com/outer"), "1");
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_attribute(Name::new("id", "example.com/inner"), "2");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let child = root.r_child("child").unwrap();
        assert_eq!(root.att_req(("id", "example.com/outer")).unwrap(), "1");
        assert_eq!(child.att_req(("id", "example.com/inner")).unwrap(), "2");
        assert!(root.att_req(("id", "example.com/inner")).is_err());
        assert!(child.att_req(("id", "example.com/outer")).is_err());
        assert!(root.att_req(("pid", "example.com/outer")).is_err());
        assert!(child.att_req(("pid", "example.com/inner")).is_err());
        assert!(root.att_req("id").is_err());
        assert!(child.att_req("id").is_err());
        Ok(())
    }

    #[test]
    fn processing_instructions_root() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_processing_instruction(ProcessingInstruction::new(
            "target".to_string(),
            "data".to_string(),
        ));
        root_builder.add_processing_instruction(ProcessingInstruction::new(
            "target2".to_string(),
            "data2".to_string(),
        ));
        root_builder.add_processing_instruction(ProcessingInstruction::new(
            "target3".to_string(),
            "data3".to_string(),
        ));
        let root = Element::new(root_builder);
        assert_eq!(root.processing_instructions().count(), 3);
        let proins: Vec<&ProcessingInstruction> = root.processing_instructions().collect();
        assert_eq!(proins.first().unwrap().target(), "target");
        assert_eq!(proins.first().unwrap().data(), "data");
        assert_eq!(proins.get(1).unwrap().target(), "target2");
        assert_eq!(proins.get(1).unwrap().data(), "data2");
        assert_eq!(proins.get(2).unwrap().target(), "target3");
        assert_eq!(proins.get(2).unwrap().data(), "data3");
        Ok(())
    }

    #[test]
    fn processing_instructions_root_and_child() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_processing_instruction(ProcessingInstruction::new(
            "root_target".to_string(),
            "root_data".to_string(),
        ));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_text("Text before PI.");
        child_builder.add_processing_instruction(ProcessingInstruction::new(
            "child_target".to_string(),
            "child_data".to_string(),
        ));
        child_builder.add_processing_instruction(ProcessingInstruction::new(
            "child_target2".to_string(),
            "child_data2".to_string(),
        ));
        child_builder.add_text("Text after PI.");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let child = root.r_child("child").unwrap();
        assert_eq!(root.processing_instructions().count(), 1);
        let root_proins: Vec<&ProcessingInstruction> = root.processing_instructions().collect();
        assert_eq!(root_proins.first().unwrap().target(), "root_target");
        assert_eq!(root_proins.first().unwrap().data(), "root_data");

        assert_eq!(child.processing_instructions().count(), 2);
        let child_proins: Vec<&ProcessingInstruction> = child.processing_instructions().collect();
        assert_eq!(child_proins.first().unwrap().target(), "child_target");
        assert_eq!(child_proins.first().unwrap().data(), "child_data");
        assert_eq!(child_proins.get(1).unwrap().target(), "child_target2");
        assert_eq!(child_proins.get(1).unwrap().data(), "child_data2");
        Ok(())
    }

    #[test]
    fn o_child_root_empty() -> TestOut {
        let root_builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(root_builder);
        assert!(root.o_child("child").is_none());
        Ok(())
    }

    #[test]
    fn o_child_root_with_child_and_grandchild() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        let mut grandchild_builder = ElementBuilder::new(Name::new("grandchild", ""));
        grandchild_builder.add_text("more text");
        let grandchild = Element::new(grandchild_builder);
        child_builder.add_child_element(grandchild);
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let o = root.o_child("child");
        assert!(o.is_some());
        let c = o.unwrap();
        assert_eq!(c.name().local_part(), "child");
        assert_eq!(c.name().namespace(), None);

        let o = c.o_child("grandchild");
        assert!(o.is_some());
        let g = o.unwrap();
        assert_eq!(g.name().local_part(), "grandchild");
        assert_eq!(g.name().namespace(), None);
        assert_eq!(g.text().unwrap(), "more text");
        assert!(g.o_child("greatgrandchild").is_none());
        Ok(())
    }

    #[test]
    fn r_child_root_empty() -> TestOut {
        let root_builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(root_builder);
        assert!(root.r_child("child").is_err());
        Ok(())
    }

    #[test]
    fn r_child_root_with_child_and_grandchild() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        let mut grandchild_builder = ElementBuilder::new(Name::new("grandchild", ""));
        grandchild_builder.add_text("more text");
        let grandchild = Element::new(grandchild_builder);
        child_builder.add_child_element(grandchild);
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let o = root.r_child("child");
        assert!(o.is_ok());
        let c = o.unwrap();
        assert_eq!(c.name().local_part(), "child");
        assert_eq!(c.name().namespace(), None);

        let o = c.r_child("grandchild");
        assert!(o.is_ok());
        let g = o.unwrap();
        assert_eq!(g.name().local_part(), "grandchild");
        assert_eq!(g.name().namespace(), None);
        assert_eq!(g.text().unwrap(), "more text");
        assert!(g.r_child("greatgrandchild").is_err());
        Ok(())
    }

    #[test]
    fn opt_root_empty() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_text("text");
        let root = Element::new(root_builder);
        let o = root.opt("child").element();
        assert!(o.is_none());
        Ok(())
    }

    #[test]
    fn opt_root_with_child_and_grandchild() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        let mut grandchild_builder = ElementBuilder::new(Name::new("grandchild", ""));
        grandchild_builder.add_text("more text");
        let grandchild = Element::new(grandchild_builder);
        child_builder.add_child_element(grandchild);
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let o = root.opt("child").element();
        assert!(o.is_some());
        let c = o.unwrap();
        assert_eq!(c.name().local_part(), "child");
        assert_eq!(c.name().namespace(), None);

        let o = c.opt("grandchild").element();
        assert!(o.is_some());
        let g = o.unwrap();
        assert_eq!(g.name().local_part(), "grandchild");
        assert_eq!(g.name().namespace(), None);
        assert_eq!(g.text().unwrap(), "more text");
        assert!(g.opt("greatgrandchild").element().is_none());
        Ok(())
    }

    #[test]
    fn req_root_empty() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_text("text");
        let root = Element::new(root_builder);
        let o = root.req("child").element();
        assert!(o.is_err());
        Ok(())
    }

    #[test]
    fn req_root_with_child_and_grandchild() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        let mut grandchild_builder = ElementBuilder::new(Name::new("grandchild", ""));
        grandchild_builder.add_text("more text");
        let grandchild = Element::new(grandchild_builder);
        child_builder.add_child_element(grandchild);
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let o = root.req("child").element();
        assert!(o.is_ok());
        let c = o.unwrap();
        assert_eq!(c.name().local_part(), "child");
        assert_eq!(c.name().namespace(), None);

        let o = c.req("grandchild").element();
        assert!(o.is_ok());
        let g = o.unwrap();
        assert_eq!(g.name().local_part(), "grandchild");
        assert_eq!(g.name().namespace(), None);
        assert_eq!(g.text().unwrap(), "more text");
        assert!(g.req("greatgrandchild").element().is_err());
        Ok(())
    }

    #[test]
    fn contains_only_text_root_empty() -> TestOut {
        let root_builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(root_builder);
        assert!(root.contains_only_text());
        Ok(())
    }

    #[test]
    fn contains_only_text_root_text_content() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_text("text");
        let root = Element::new(root_builder);
        assert!(root.contains_only_text());
        Ok(())
    }

    #[test]
    fn contains_only_text_root_processing_instruction() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_processing_instruction(ProcessingInstruction::new(
            "target".to_string(),
            "data".to_string(),
        ));
        root_builder.add_text("text");
        let root = Element::new(root_builder);
        assert!(!root.contains_only_text());
        Ok(())
    }

    #[test]
    fn contains_only_text_root_mixed_child_empty() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_text("Before child element!");
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_text("text");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        root_builder.add_text("After child element!");
        let root = Element::new(root_builder);
        let child = root.r_child("child").unwrap();
        assert!(!root.contains_only_text());
        assert!(child.contains_only_text());
        Ok(())
    }

    #[test]
    fn contains_only_text_root_child_processing_instruction() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_processing_instruction(ProcessingInstruction::new(
            "target".to_string(),
            "data".to_string(),
        ));
        child_builder.add_text("text");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let child = root.r_child("child").unwrap();
        assert!(!root.contains_only_text());
        assert!(!child.contains_only_text());
        Ok(())
    }

    #[test]
    fn contains_only_text_root_child_grandchild() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        let mut grandchild_builder = ElementBuilder::new(Name::new("grandchild", ""));
        grandchild_builder.add_text("just text");
        let grandchild = Element::new(grandchild_builder);
        child_builder.add_child_element(grandchild);
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);

        let child = root.r_child("child").unwrap();
        let grandchild = child.r_child("grandchild").unwrap();
        assert!(!root.contains_only_text());
        assert!(!child.contains_only_text());
        assert!(grandchild.contains_only_text());
        Ok(())
    }

    #[test]
    fn text_root_empty() -> TestOut {
        let root_builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(root_builder);
        assert_eq!(root.text().unwrap(), "");
        Ok(())
    }

    #[test]
    fn text_root_text() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_text("some text");
        let root = Element::new(root_builder);
        assert_eq!(root.text().unwrap(), "some text");
        Ok(())
    }

    #[test]
    fn text_root_mixed() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_text("inner text");
        let child = Element::new(child_builder);
        root_builder.add_text("some text");
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        assert!(root.text().is_err());
        Ok(())
    }

    #[test]
    fn text_root_processing_instruction() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_processing_instruction(ProcessingInstruction::new(
            "target".to_string(),
            "data".to_string(),
        ));
        root_builder.add_text("some text");
        let root = Element::new(root_builder);
        assert!(root.text().is_err());
        Ok(())
    }

    #[test]
    fn text_root_mixed_child_empty() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_text("Before child element!");
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_text("text");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        root_builder.add_text("After child element!");
        let root = Element::new(root_builder);
        let child = root.r_child("child").unwrap();
        assert!(root.text().is_err());
        assert_eq!(child.text().unwrap(), "text");
        Ok(())
    }

    #[test]
    fn text_root_child_processing_instruction() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_processing_instruction(ProcessingInstruction::new(
            "target".to_string(),
            "data".to_string(),
        ));
        child_builder.add_text("text");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        let child = root.r_child("child").unwrap();
        assert!(root.text().is_err());
        assert!(child.text().is_err());
        Ok(())
    }

    #[test]
    fn text_root_child_grandchild() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        let mut grandchild_builder = ElementBuilder::new(Name::new("grandchild", ""));
        grandchild_builder.add_text("just text");
        let grandchild = Element::new(grandchild_builder);
        child_builder.add_child_element(grandchild);
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);

        let child = root.r_child("child").unwrap();
        let grandchild = child.r_child("grandchild").unwrap();
        assert!(root.text().is_err());
        assert!(child.text().is_err());
        assert_eq!(grandchild.text().unwrap(), "just text");
        Ok(())
    }

    #[test]
    fn all_root_empty() -> TestOut {
        let root_builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(root_builder);
        assert!(root
            .all("child")
            .iter()
            .collect::<Vec<&Element>>()
            .is_empty());
        Ok(())
    }

    #[test]
    fn all_root_text() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_text("text");
        let root = Element::new(root_builder);
        assert!(root
            .all("child")
            .iter()
            .collect::<Vec<&Element>>()
            .is_empty());
        Ok(())
    }

    #[test]
    fn all_root_children() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        for _ in 0..5 {
            let child = Element::new(ElementBuilder::new(Name::new("child", "")));
            root_builder.add_child_element(child);
        }
        let root = Element::new(root_builder);
        assert_eq!(root.all("child").iter().count(), 5);
        Ok(())
    }

    #[test]
    fn first_root_empty() -> TestOut {
        let root_builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(root_builder);
        assert!(root
            .first("child")
            .iter()
            .collect::<Vec<&Element>>()
            .is_empty());
        Ok(())
    }

    #[test]
    fn first_root_text() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_text("text");
        let root = Element::new(root_builder);
        assert!(root
            .first("child")
            .iter()
            .collect::<Vec<&Element>>()
            .is_empty());
        Ok(())
    }

    #[test]
    fn first_root_children() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        for i in 0..5 {
            let mut child_builder = ElementBuilder::new(Name::new("child", ""));
            child_builder.add_text(format!("text{}", i + 1));
            let child = Element::new(child_builder);
            root_builder.add_child_element(child);
        }
        let root = Element::new(root_builder);
        let first = root.first("child").iter().next().unwrap();
        assert_eq!(first.name().local_part(), "child");
        assert_eq!(first.name().namespace(), None);
        assert_eq!(first.text().unwrap(), "text1");
        Ok(())
    }

    #[test]
    fn last_root_empty() -> TestOut {
        let root_builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(root_builder);
        assert!(root
            .last("child")
            .iter()
            .collect::<Vec<&Element>>()
            .is_empty());
        Ok(())
    }

    #[test]
    fn last_root_text() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_text("text");
        let root = Element::new(root_builder);
        assert!(root
            .last("child")
            .iter()
            .collect::<Vec<&Element>>()
            .is_empty());
        Ok(())
    }

    #[test]
    fn last_root_children() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        for i in 0..5 {
            let mut child_builder = ElementBuilder::new(Name::new("child", ""));
            child_builder.add_text(format!("text{}", i + 1));
            let child = Element::new(child_builder);
            root_builder.add_child_element(child);
        }
        let root = Element::new(root_builder);
        let last = root.last("child").iter().next().unwrap();
        assert_eq!(last.name().local_part(), "child");
        assert_eq!(last.name().namespace(), None);
        assert_eq!(last.text().unwrap(), "text5");
        Ok(())
    }

    #[test]
    fn nth_root_empty() -> TestOut {
        let root_builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(root_builder);
        assert!(root
            .nth("child", 2)
            .iter()
            .collect::<Vec<&Element>>()
            .is_empty());
        Ok(())
    }

    #[test]
    fn nth_root_text() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        root_builder.add_text("text");
        let root = Element::new(root_builder);
        assert!(root
            .nth("child", 2)
            .iter()
            .collect::<Vec<&Element>>()
            .is_empty());
        Ok(())
    }

    #[test]
    fn nth_root_children() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        for i in 0..5 {
            let mut child_builder = ElementBuilder::new(Name::new("child", ""));
            child_builder.add_text(format!("text{}", i + 1));
            let child = Element::new(child_builder);
            root_builder.add_child_element(child);
        }
        let root = Element::new(root_builder);
        let third = root.nth("child", 2).iter().next().unwrap();
        assert_eq!(third.name().local_part(), "child");
        assert_eq!(third.name().namespace(), None);
        assert_eq!(third.text().unwrap(), "text3");
        Ok(())
    }

    #[test]
    fn nth_root_not_enough_children() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        for i in 0..2 {
            let mut child_builder = ElementBuilder::new(Name::new("child", ""));
            child_builder.add_text(format!("text{}", i + 1));
            let child = Element::new(child_builder);
            root_builder.add_child_element(child);
        }
        let root = Element::new(root_builder);
        let third = root.nth("child", 2).iter().next();
        assert!(third.is_none());
        Ok(())
    }

    #[test]
    fn namespace_root_no_default_namespace() -> TestOut {
        let root_builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(root_builder);
        let pre_cursor = root.pre_ns("");
        assert!(pre_cursor.default_namespace().is_none());
        Ok(())
    }

    #[test]
    fn namespace_root_some_default_namespace() -> TestOut {
        let root_builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(root_builder);
        let pre_cursor = root.pre_ns("some-namespace");
        assert_eq!(
            pre_cursor.default_namespace().unwrap().value(),
            "some-namespace"
        );
        Ok(())
    }

    /*
    OLD UNIT TESTS WHICH WERE EFFECTIVELY A DRY RUN FOR THE ACTUAL XML PARSER LOOP.
    */

    #[test]
    fn test_nested_xml() -> TestOut {
        // Manually build strucuture to represent `<root><child>text</child></root>`
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_text("text");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);
        let root = Element::new(root_builder);
        assert_eq!(root.name().local_part(), "root");
        assert_eq!(root.content().len(), 1);
        if let Content::Child(c) = root.content().first().unwrap() {
            assert_eq!(c.name().local_part(), "child");
            assert_eq!(c.content().len(), 1);
            if let Content::Text(t) = c.content().first().unwrap() {
                assert_eq!(t.as_ref(), "text");
            }
        } else {
            panic!("Child was expected to be element!")
        }
        Ok(())
    }

    #[test]
    fn test_sibling_xml() -> TestOut {
        // Manually build strucuture to represent `<root><child>first</child><child>second</child><child>third</child></root>`
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));

        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_text("first");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);

        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_text("second");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);

        let mut child_builder = ElementBuilder::new(Name::new("child", ""));
        child_builder.add_text("third");
        let child = Element::new(child_builder);
        root_builder.add_child_element(child);

        let root = Element::new(root_builder);
        assert_eq!(root.name().local_part(), "root");
        assert_eq!(root.content().len(), 3);

        if let Content::Child(c) = root.content().first().unwrap() {
            assert_eq!(c.name().local_part(), "child");
            assert_eq!(c.content().len(), 1);
            if let Content::Text(t) = c.content().first().unwrap() {
                assert_eq!(t.as_ref(), "first");
            } else {
                panic!("First child content was expected to be text!")
            }
        } else {
            panic!("First child was expected to be element!")
        }

        if let Content::Child(c) = root.content().get(1).unwrap() {
            assert_eq!(c.name().local_part(), "child");
            assert_eq!(c.content().len(), 1);
            if let Content::Text(t) = c.content().first().unwrap() {
                assert_eq!(t.as_ref(), "second");
            } else {
                panic!("Second child content was expected to be text!")
            }
        } else {
            panic!("Second child was expected to be element!")
        }

        if let Content::Child(c) = root.content().get(2).unwrap() {
            assert_eq!(c.name().local_part(), "child");
            assert_eq!(c.content().len(), 1);
            if let Content::Text(t) = c.content().first().unwrap() {
                assert_eq!(t.as_ref(), "third");
            } else {
                panic!("Third child content was expected to be text!")
            }
        } else {
            panic!("Third child was expected to be element!")
        }
        Ok(())
    }

    fn find_namespace_from_prefix<'a>(prefix: &str, stack: &'a [HashMap<&str, &str>]) -> &'a str {
        for map in stack.iter().rev() {
            if let Some(n) = map.get(prefix) {
                return n;
            }
        }
        ""
    }

    fn determine_element_name(raw_name: &str, stack: &[HashMap<&str, &str>]) -> Name {
        if let Some((prefix, local_part)) = raw_name.split_once(':') {
            let namespace = find_namespace_from_prefix(prefix, stack);
            Name::new(local_part, namespace)
        } else {
            let namespace = find_namespace_from_prefix("", stack);
            Name::new(raw_name, namespace)
        }
    }

    #[test]
    fn test_sibling_xml_namespaces() -> TestOut {
        // Manually build strucuture to represent
        // <demo xmlns="example.com/demo">
        //     <author xmlns:d="example.com/dublin">
        //         <d:name>Barry</d:name>
        //         <d:id>12345</d:id>
        //         <d:confusing xmlns:d="example.com/confusing">nested identical namespace prefix</d:confusing>
        //     </author>
        //     <meta xmlns:m="example.com/metadata">
        //         <m:modified>2023-11-25T17:45:00Z</m:modified>
        //         <m:hash>A3FBC9CD345E</m:hash>
        //         <weird xmlns="example.com/oddity">whatever</weird>
        //     </meta>
        // </demo>

        let mut parent_stack: Vec<ElementBuilder> = Vec::with_capacity(1);
        let mut namespace_stack: Vec<HashMap<&str, &str>> = Vec::new();

        // Read demo start-tag:
        let mut root_namespaces = HashMap::with_capacity(1);
        root_namespaces.insert("", "example.com/demo");
        namespace_stack.push(root_namespaces);
        let root_builder = ElementBuilder::new(determine_element_name("demo", &namespace_stack));
        parent_stack.push(root_builder);

        // Read author start-tag:
        let mut author_namespaces = HashMap::with_capacity(1);
        author_namespaces.insert("d", "example.com/dublin");
        namespace_stack.push(author_namespaces);
        let author_builder =
            ElementBuilder::new(determine_element_name("author", &namespace_stack));
        parent_stack.push(author_builder);

        // Read d:name start-tag
        let name_namespaces = HashMap::new();
        namespace_stack.push(name_namespaces);
        let author_name_builder =
            ElementBuilder::new(determine_element_name("d:name", &namespace_stack));
        parent_stack.push(author_name_builder);
        // Read text content
        parent_stack.last_mut().unwrap().add_text("Barry");
        // Read d:name end-tag
        let closed = parent_stack.pop().unwrap();
        namespace_stack.pop();
        parent_stack
            .last_mut()
            .unwrap()
            .add_child_element(Element::new(closed));

        // Read d:id start-tag
        let id_namespaces = HashMap::new();
        namespace_stack.push(id_namespaces);
        let id_name_builder = ElementBuilder::new(determine_element_name("d:id", &namespace_stack));
        parent_stack.push(id_name_builder);
        // Read text content
        parent_stack.last_mut().unwrap().add_text("12345");
        // Read d:id end-tag
        let closed = parent_stack.pop().unwrap();
        namespace_stack.pop();
        parent_stack
            .last_mut()
            .unwrap()
            .add_child_element(Element::new(closed));

        // Read d:confusing start-tag
        let mut confusing_namespaces = HashMap::with_capacity(1);
        confusing_namespaces.insert("d", "example.com/confusing");
        namespace_stack.push(confusing_namespaces);
        let confusing_builder =
            ElementBuilder::new(determine_element_name("d:confusing", &namespace_stack));
        parent_stack.push(confusing_builder);
        // Read text content
        parent_stack
            .last_mut()
            .unwrap()
            .add_text("nested identical namespace prefix");
        // Read d:confusing end-tag
        let closed = parent_stack.pop().unwrap();
        namespace_stack.pop();
        parent_stack
            .last_mut()
            .unwrap()
            .add_child_element(Element::new(closed));

        // Read author end-tag:
        let closed = parent_stack.pop().unwrap();
        namespace_stack.pop();
        parent_stack
            .last_mut()
            .unwrap()
            .add_child_element(Element::new(closed));

        // Read meta start-tag
        let mut meta_namespaces = HashMap::with_capacity(1);
        meta_namespaces.insert("m", "example.com/metadata");
        namespace_stack.push(meta_namespaces);
        let meta_builder = ElementBuilder::new(determine_element_name("meta", &namespace_stack));
        parent_stack.push(meta_builder);

        // Read m:modified start-tag
        let modified_namespaces = HashMap::new();
        namespace_stack.push(modified_namespaces);
        let modified_builder =
            ElementBuilder::new(determine_element_name("m:modified", &namespace_stack));
        parent_stack.push(modified_builder);
        // Read text content
        parent_stack
            .last_mut()
            .unwrap()
            .add_text("2023-11-25T17:45:00Z");
        // Read m:modified end-tag
        let closed = parent_stack.pop().unwrap();
        namespace_stack.pop();
        parent_stack
            .last_mut()
            .unwrap()
            .add_child_element(Element::new(closed));

        // Read m:hash start-tag
        let hash_namespaces = HashMap::new();
        namespace_stack.push(hash_namespaces);
        let hash_builder = ElementBuilder::new(determine_element_name("m:hash", &namespace_stack));
        parent_stack.push(hash_builder);
        // Read text content
        parent_stack.last_mut().unwrap().add_text("A3FBC9CD345E");
        // Read m:hash end-tag
        let closed = parent_stack.pop().unwrap();
        namespace_stack.pop();
        parent_stack
            .last_mut()
            .unwrap()
            .add_child_element(Element::new(closed));

        // Read weird start-tag
        let mut weird_namespaces = HashMap::with_capacity(1);
        weird_namespaces.insert("", "example.com/oddity");
        namespace_stack.push(weird_namespaces);
        let weird_builder = ElementBuilder::new(determine_element_name("weird", &namespace_stack));
        parent_stack.push(weird_builder);
        // Read text content
        parent_stack.last_mut().unwrap().add_text("whatever");
        // Read m:weird end-tag
        let closed = parent_stack.pop().unwrap();
        namespace_stack.pop();
        parent_stack
            .last_mut()
            .unwrap()
            .add_child_element(Element::new(closed));

        // Read meta end-tag
        let closed = parent_stack.pop().unwrap();
        namespace_stack.pop();
        parent_stack
            .last_mut()
            .unwrap()
            .add_child_element(Element::new(closed));

        // Read demo end-tag
        let root_builder = parent_stack.pop().unwrap();
        namespace_stack.pop();

        let root = Element::new(root_builder);
        assert_eq!(root.name().local_part(), "demo");
        assert_eq!(root.name().namespace().unwrap(), "example.com/demo");
        assert_eq!(root.content().len(), 2);

        let author = root.o_child(("author", "example.com/demo"));
        assert!(author.is_some());
        let author = author.unwrap();
        assert_eq!(author.content().len(), 3);

        let name = author.o_child(("name", "example.com/dublin"));
        assert!(name.is_some());
        let name = name.unwrap();
        assert_eq!(name.name().local_part(), "name");
        assert_eq!(name.name().namespace().unwrap(), "example.com/dublin");
        assert!(name.contains_only_text());
        assert_eq!(name.text().unwrap(), "Barry"); // CONTINUE FROM HERE !!!

        let id = author.o_child(("id", "example.com/dublin"));
        assert!(id.is_some());
        let id = id.unwrap();
        assert_eq!(id.name().local_part(), "id");
        assert_eq!(id.name().namespace().unwrap(), "example.com/dublin");
        assert!(id.contains_only_text());
        assert_eq!(id.text().unwrap(), "12345");

        let confusing = author.o_child(("confusing", "example.com/confusing"));
        assert!(confusing.is_some());
        let confusing = confusing.unwrap();
        assert_eq!(confusing.name().local_part(), "confusing");
        assert_eq!(
            confusing.name().namespace().unwrap(),
            "example.com/confusing"
        );
        assert!(confusing.contains_only_text());
        assert_eq!(
            confusing.text().unwrap(),
            "nested identical namespace prefix"
        );

        let meta = root.o_child(("meta", "example.com/demo"));
        assert!(meta.is_some());
        let meta = meta.unwrap();
        assert_eq!(meta.content().len(), 3);
        // for c in meta.content() {
        //     match c {
        //         Content::Child(e) => println!("meta contains {:?}", e),
        //         _ => {}
        //     }
        // }
        let modified = meta.r_child(("modified", "example.com/metadata"));
        assert!(modified.is_ok());
        assert_eq!(modified.unwrap().text().unwrap(), "2023-11-25T17:45:00Z");

        let meta_modified = root
            .opt(("meta", "example.com/demo"))
            .req(("modified", "example.com/metadata"))
            .element();
        assert!(matches!(meta_modified, Ok(Some(_))));
        assert_eq!(
            meta_modified.unwrap().unwrap().text().unwrap(),
            "2023-11-25T17:45:00Z"
        );
        assert_eq!(
            root.opt(("meta", "example.com/demo"))
                .req(("hash", "example.com/metadata"))
                .text()
                .unwrap()
                .unwrap(),
            "A3FBC9CD345E"
        );

        assert_eq!(
            meta.req(("weird", "example.com/oddity")).text().unwrap(),
            "whatever"
        );

        Ok(())
    }

    // TODO: ADD TESTS FOR MIXED (MARKUP) CONTENT !!!
}
