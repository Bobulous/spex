use crate::{xml::Element, xml::ProcessingInstruction};

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Content {
    Text(Box<str>),
    Child(Box<Element>),
    PI(Box<ProcessingInstruction>),
}
