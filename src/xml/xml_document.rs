use crate::{xml::Element, xml::ProcessingInstruction};

/**
Represents the XML document, containing the root element and any processing instructions which
appear outside of the root element.
*/
#[derive(Debug)]
pub struct XmlDocument {
    processing_instructions: Vec<ProcessingInstruction>,
    root: Element,
}

impl XmlDocument {
    pub(crate) fn new(
        processing_instructions: Vec<ProcessingInstruction>,
        root: Element,
    ) -> XmlDocument {
        XmlDocument {
            processing_instructions,
            root,
        }
    }

    /**
    Returns an iterator of references to all processing instructions contained within this
    XML document **outside** of the root element.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <?document \n\t this processing instruction (PI) is not contained within the root element ?>
    <root>
        <?inside-root \n\t this PI is contained within the root element ?>
        <?same-again also contained within root ?>
        <child><?inner this PI is contained within the child element ?></child>
    </root>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let proc_ins: Vec<&ProcessingInstruction> = xml_doc.processing_instructions().collect();
        # assert_eq!(proc_ins.len(), 1);
        if let Some(first) = proc_ins.get(0) {
            assert_eq!(first.target(), "document");
            // Note that all whitespace between the PI target and the PI data is skipped, but
            // any whitespace at the end of the data is returned.
            assert_eq!(first.data(),
                "this processing instruction (PI) is not contained within the root element ");
        }
        # else {panic!("Expected document processing instruction to exist!");}

        Ok(())
    }
    ```
    */
    pub fn processing_instructions(&self) -> impl Iterator<Item = &ProcessingInstruction> {
        self.processing_instructions.iter()
    }

    /**
    Returns a reference to the root element of this XML document.

    # Examples

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    <PlayingNow>
        <Track>
            <Name>Lie</Name>
            <Length>4m19s</Length>
            <ByArtist>Black Light Burns</ByArtist>
            <FromAlbum>Cruel Melody</FromAlbum>
        </Track>
    </PlayingNow>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #

    // The above XML is turned into an XmlDocument and passed to this method.
    fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
        let root = xml_doc.root();
        assert_eq!(root.name().local_part(), "PlayingNow");
        Ok(())
    }
    ```
    */
    pub fn root(&self) -> &Element {
        &self.root
    }
}
