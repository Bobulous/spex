/*!
Structures used to represent an `XmlDocument` and its `Element` tree, and the values and
properties held within.
*/
mod content;
mod element;
mod element_builder;
mod language_tag;
mod name;
mod processing_instruction;
mod white_space_handling;
mod xml_document;

pub(crate) use self::content::Content;
pub use self::element::Element;
pub(crate) use self::element_builder::ElementBuilder;
pub(crate) use self::language_tag::LanguageTag;
pub use self::name::Name;
pub use self::processing_instruction::ProcessingInstruction;
pub use self::white_space_handling::WhiteSpaceHandling;
pub use self::xml_document::XmlDocument;
