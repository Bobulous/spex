/**
The intended handling of white space within element text content, where the original XML
explicitly signalled an intention regarding white space handling.
*/
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum WhiteSpaceHandling {
    /**
    The original XML signalled (using `xml:space="default"`) that white space within the element
    content can be handled by whatever is the consuming application's default handling of white
    space. For example, the application might decide to remove leading and trailing space, or
    replace newlines and tabs with simple spaces.
    */
    Default,
    /**
    The original XML signalled (using `xml:space="preserve"`) that white space within the
    element content is intended to be preserved. For example, the element content might be a
    password where the white space is part of the password value and cannot be removed or
    normalised without invalidating the password.
    */
    Preserve,
}
