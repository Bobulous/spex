use std::collections::HashMap;

use crate::xml::WhiteSpaceHandling;

// Properties which are inherited by XML child elements.
#[derive(Debug)]
pub(crate) struct InheritableProperties {
    pub namespaces: HashMap<Box<str>, Box<str>>,
    pub whitespace: Option<WhiteSpaceHandling>,
    pub lang: Option<Box<str>>,
}
