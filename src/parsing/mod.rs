/*!
Structures used in parsing raw XML into an `XmlDocument` containing an `Element` tree.
*/
mod inheritable_properties;
mod xml_parser;
mod xml_reader;
mod xml_supported_character_encoding;

use self::inheritable_properties::InheritableProperties;
use self::xml_parser::XmlParser;
pub use self::xml_reader::XmlReader;
use self::xml_supported_character_encoding::XmlSupportedCharacterEncoding;
