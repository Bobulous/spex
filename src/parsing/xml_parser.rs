use std::{
    collections::HashMap,
    io::{Error, Read},
};

use crate::{
    parsing::InheritableProperties, xml::Element, xml::ElementBuilder, xml::LanguageTag, xml::Name,
    xml::ProcessingInstruction, xml::WhiteSpaceHandling, xml::XmlDocument,
};
use sipp::{decoder::ByteStreamCharDecoder, parser::Parser};

// A function which reports on whether a given character is valid as the *first*
// character of an XML element or attribute name within a namespace-aware document.
// Because namespaces are considered, the colon is *NOT* a valid character within
// an XML element or attribute name.
const NO_COLON_NAME_START_CHAR: fn(char) -> bool = |c| {
    c.is_ascii_alphabetic() || c == '_' || {
        let d = &(c as u32);
        (0xC0..=0xD6).contains(d)
            || (0xD8..=0xF6).contains(d)
            || (0xF8..=0x2FF).contains(d)
            || (0x370..=0x37D).contains(d)
            || (0x37F..=0x1FFF).contains(d)
            || (0x200C..=0x200D).contains(d)
            || (0x2070..=0x218F).contains(d)
            || (0x2C00..=0x2FEF).contains(d)
            || (0x3001..=0xD7FF).contains(d)
            || (0xF900..=0xFDCF).contains(d)
            || (0xFDF0..=0xFFFD).contains(d)
            || (0x10000..=0xEFFFF).contains(d)
    }
};

// A function which reports on whether a given char is valid within an
// XML element or attribute name *after* the first character of the name.
const NAME_CHAR: fn(char) -> bool = |c| {
    c.is_ascii_digit() || c == '-' || c == '.' || NO_COLON_NAME_START_CHAR(c) || {
        let d = &(c as u32);
        d == &(0xB7) || (0x300..=0x36F).contains(d) || (0x203F..=0x2040).contains(d)
    }
};

// A function which reports on whether a given char is valid within XML text content.
const TEXT_CHAR: fn(char) -> bool = |c| {
    let d = &(c as u32);
    (0x20..=0xD7FF).contains(d)
        || d == &0x0A
        || d == &0x0D
        || d == &0x09
        || (0xE000..=0xFFFD).contains(d)
        || (0x10000..=0x10FFFF).contains(d)
};

type BoxedStrMap = HashMap<Box<str>, Box<str>>;

pub struct XmlParser<D, R> {
    parser: Parser<D, R>,
}

impl<D, R> XmlParser<D, R>
where
    R: Read,
    D: ByteStreamCharDecoder<R>,
{
    pub(crate) fn wrap(d: D) -> XmlParser<D, R> {
        XmlParser {
            parser: Parser::wrap(d),
        }
    }

    pub(crate) fn parse_encodings(
        &mut self,
        valid_declared_encodings: Option<&[&str]>,
    ) -> Result<XmlDocument, Error> {
        let mut processing_instructions = Vec::new();

        let mut found_tag_opener = self.parser.accept('<')?;
        if found_tag_opener && self.parser.accept('?')? {
            let target_name = self.read_qualified_name()?;
            if target_name == "xml" {
                self.read_xml_declaration(valid_declared_encodings)?;
            } else {
                let pi = self.read_processing_instruction(target_name)?;
                processing_instructions.push(pi);
            }
            found_tag_opener = false;
        }

        if !found_tag_opener {
            self.parser.skip_while(Self::whitespace)?;
            self.parser.require('<')?;
        }

        let mut root = None;
        loop {
            if self.parser.accept('?')? {
                let target_name = self.read_qualified_name()?;
                if target_name == "xml" {
                    return Err(Error::new(
                        std::io::ErrorKind::InvalidData,
                        "XML declaration found in invalid position!",
                    ));
                }
                let pi = self.read_processing_instruction(target_name)?;
                processing_instructions.push(pi);
            } else if self.parser.accept('!')? {
                self.skip_comment()?;
            } else {
                root = Some(self.read_root_element()?);
            }
            self.parser.skip_while(Self::whitespace)?;
            if !self.parser.has_more()? {
                match root {
                    Some(e) => return Ok(XmlDocument::new(processing_instructions, e)),
                    None => {
                        return Err(Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Input does not contain any root element!",
                        ))
                    }
                }
            }
            if !self.parser.accept('<')? {
                return Err(Error::new(
                    std::io::ErrorKind::InvalidData,
                    format!(
                        "Unexpected content {} root element!",
                        match root {
                            Some(_) => "after",
                            None => "before",
                        }
                    ),
                ));
            }
        }
    }

    fn read_xml_declaration(
        &mut self,
        valid_declared_encodings: Option<&[&str]>,
    ) -> Result<(), Error> {
        self.parser.skip_while(Self::whitespace)?;
        self.check_xml_version()?;
        let mut found_whitespace = self.parser.skip_while(Self::whitespace)?;
        if found_whitespace && self.parser.peek_sees('e')? {
            self.check_encoding_declaration(valid_declared_encodings)?;
            found_whitespace = self.parser.skip_while(Self::whitespace)?;
        }
        if found_whitespace && self.parser.peek_sees('s')? {
            self.check_standalone_declaration()?
        }
        self.parser.skip_while(Self::whitespace)?;
        self.parser.require('?')?;
        self.parser.require('>')?;
        Ok(())
    }

    fn check_xml_version(&mut self) -> Result<(), Error> {
        self.parser.require_str("version")?;
        let version_number = self.read_assignment()?;
        match version_number.as_ref() {
            "1.0" => {}
            "1.1" => {}
            _ => {
                return Err(Error::new(
                    std::io::ErrorKind::InvalidData,
                    format!("Unexpected XML version number '{}'.", version_number),
                ));
            }
        }
        Ok(())
    }

    fn read_assignment(&mut self) -> Result<String, Error> {
        self.parser.skip_while(Self::whitespace)?;
        self.parser.require('=')?;
        self.parser.skip_while(Self::whitespace)?;
        let wrapper = match self.parser.read()? {
            '\'' => '\'',
            '"' => '"',
            _ => {
                return Err(Error::new(
                    std::io::ErrorKind::InvalidData,
                    "Assignment value is not wrapped with expected double-quote or single-quote.",
                ));
            }
        };
        let mut value = String::with_capacity(32);
        self.read_attribute_value(&mut value, wrapper)?;
        self.parser.require(wrapper)?;
        Ok(value)
    }

    fn check_encoding_declaration(
        &mut self,
        valid_declared_encodings: Option<&[&str]>,
    ) -> Result<(), Error> {
        self.parser.require_str("encoding")?;
        // XML 1.0 says processors SHOULD match encodings in a case-insensitive way.
        let encoding = self.read_assignment()?.to_uppercase();
        if let Some(valid_set) = valid_declared_encodings {
            let mut found_valid = false;
            for &valid in valid_set {
                if encoding == *valid {
                    found_valid = true;
                    break;
                }
            }
            if !found_valid {
                return Err(Error::new(
                    std::io::ErrorKind::InvalidData,
                    format!(
                        "Unsupported encoding in XML declaration! Expected {} {:?}, based on byte-order mark and/or initial byte sequence.",
                        {match valid_set.len() {
                            1 => "encoding to be",
                            _ => "one of",
                        }}, valid_set
                    ),
                ));
            }
        }
        Ok(())
    }

    fn check_standalone_declaration(&mut self) -> Result<(), Error> {
        self.parser.require_str("standalone")?;
        let value = self.read_assignment()?;
        match value.as_ref() {
            "yes" => Ok(()),
            "no" => Ok(()),
            _ => Err(Error::new(
                std::io::ErrorKind::InvalidData,
                "Illegal value of standalone in XML declaration.",
            )),
        }
    }

    fn read_processing_instruction(
        &mut self,
        target_name: String,
    ) -> Result<ProcessingInstruction, Error> {
        self.parser.skip_while(Self::whitespace)?;
        let mut data = String::with_capacity(32);
        loop {
            let c = self.parser.read()?;
            match c {
                '?' => {
                    if self.parser.accept('>')? {
                        break;
                    }
                    data.push('?');
                }
                _ => {
                    if !TEXT_CHAR(c) {
                        return Err(Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Processing instruction contains invalid character.",
                        ));
                    }
                    data.push(c);
                }
            }
        }
        // data.shrink_to_fit();
        Ok(ProcessingInstruction::new(target_name, data))
    }

    fn read_cdata_section(&mut self) -> Result<String, Error> {
        self.parser.require_str("CDATA[")?;
        let mut s = String::with_capacity(32);
        let mut close_counter = 0;
        loop {
            let c = self.parser.read()?;
            match c {
                ']' => {
                    close_counter += 1;
                }
                '>' => {
                    if close_counter > 0 {
                        if close_counter > 1 {
                            for _ in 0..(close_counter - 2) {
                                s.push(']');
                            }
                            return Ok(s);
                        } else {
                            s.push(']');
                        }
                    }
                    close_counter = 0;
                    s.push('>');
                }
                c => {
                    if !TEXT_CHAR(c) {
                        return Err(Error::new(
                            std::io::ErrorKind::InvalidData,
                            "CDATA section contains invalid character!",
                        ));
                    }
                    for _ in 0..close_counter {
                        s.push(']');
                    }
                    close_counter = 0;
                    s.push(c);
                }
            }
        }
    }

    fn skip_comment(&mut self) -> Result<(), Error> {
        self.parser.require_str("--")?;
        let mut hyphen_counter = 0_u8;
        loop {
            match self.parser.read()? {
                '-' => {
                    if hyphen_counter == 0 {
                        hyphen_counter += 1;
                    } else {
                        self.parser.require('>')?;
                        return Ok(());
                    }
                }
                c => {
                    if !TEXT_CHAR(c) {
                        return Err(Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Comment contains invalid character!",
                        ));
                    }
                    hyphen_counter = 0;
                }
            }
        }
    }

    fn read_root_element(&mut self) -> Result<Element, Error> {
        let mut parent_stack: Vec<ElementBuilder> = Vec::with_capacity(4);
        let mut inheritable_stack: Vec<InheritableProperties> = Vec::with_capacity(4);
        let root_builder = self.builder_from_start_tag(&mut inheritable_stack)?;
        if self.parser.accept('/')? {
            // Self-closing tag indicates empty element. We're done here.
            self.parser.require('>')?;
            inheritable_stack.pop();
            Ok(Element::new(root_builder))
        } else {
            self.parser.require('>')?;
            parent_stack.push(root_builder);
            let root = self.read_root_contents(&mut parent_stack, &mut inheritable_stack)?;
            Ok(root)
        }
    }

    // Reads from after the '<' of a start tag (or self-closing tag), to the end of (well-formed) the tag.
    // Returns an ElementBuilder which holds the namespace-resolved element name and attributes.
    // Also updates the given namespace_stack to add this element's namespaces.
    fn builder_from_start_tag(
        &mut self,
        inheritable_stack: &mut Vec<InheritableProperties>,
    ) -> Result<ElementBuilder, Error> {
        let raw_name = self.read_qualified_name()?;
        let (mut raw_attributes, mut declared_namespaces) = self.read_attributes()?;
        if inheritable_stack.is_empty() {
            // For the root element only, inject the implicit "xml" namespace prefix.
            declared_namespaces.insert(
                "xml".to_string().into_boxed_str(),
                "http://www.w3.org/XML/1998/namespace"
                    .to_string()
                    .into_boxed_str(),
            );
        }
        let lang = raw_attributes.remove("xml:lang");
        let whitespace = if let Some(space) = raw_attributes.remove("xml:space") {
            match space.as_ref() {
                "default" => Some(WhiteSpaceHandling::Default),
                "preserve" => Some(WhiteSpaceHandling::Preserve),
                _ => {
                    return Err(Error::new(
                        std::io::ErrorKind::InvalidData,
                        "XML element contains illegal value for xml:space attribute.",
                    ));
                }
            }
        } else {
            None
        };
        let inheritable_properties = InheritableProperties {
            namespaces: declared_namespaces,
            whitespace,
            lang,
        };

        inheritable_stack.push(inheritable_properties);
        let resolved_attributes = Self::resolve_attribute_names(raw_attributes, inheritable_stack)?;
        let element_name = Self::determine_element_name(&raw_name, inheritable_stack)?;

        let mut builder = ElementBuilder::new(element_name);
        for (name, value) in resolved_attributes {
            builder.add_attribute(name, value);
        }
        builder.whitespace = Self::determine_whitespace_handling(inheritable_stack);
        builder.lang = Self::determine_language_tag(inheritable_stack)?;
        Ok(builder)
    }

    // Reads an XML element or attribute QName (qualified name) which may be
    // with or without namespace prefix.
    fn read_qualified_name(&mut self) -> Result<String, Error> {
        let mut name = String::with_capacity(16);
        self.read_nc_name(&mut name)?;
        if self.parser.accept(':')? {
            // This is a prefixed name, so read the local part now.
            name.push(':');
            self.read_nc_name(&mut name)?;
        }
        // name.shrink_to_fit();
        Ok(name)
    }

    // Reads an XML NCName (no-colon name) which may be a namespace prefix,
    // or the local part of an element or attribute name.
    fn read_nc_name(&mut self, name: &mut String) -> Result<(), Error> {
        let name_start = self.parser.read()?;
        if !NO_COLON_NAME_START_CHAR(name_start) {
            return Err(Error::new(
                std::io::ErrorKind::InvalidData,
                "XML name begins with invalid character!",
            ));
        }
        name.push(name_start);
        while let Some(c) = self.parser.peek()? {
            if NAME_CHAR(c) {
                name.push(self.parser.read()?);
            } else {
                break;
            }
        }
        Ok(())
    }

    fn read_attributes(&mut self) -> Result<(BoxedStrMap, BoxedStrMap), Error> {
        let mut raw_attributes = HashMap::new();
        let mut namespaces = HashMap::new();
        let mut found_whitespace = self.parser.skip_while(Self::whitespace)?;
        while !(self.parser.peek_sees('/')? || self.parser.peek_sees('>')?) {
            if !found_whitespace {
                return Err(Error::new(
                    std::io::ErrorKind::InvalidData,
                    "Input contains ill-formed start tag: whitespace expected between elements and attributes.",
                ));
            }
            let raw_name = self.read_qualified_name()?;
            let value = self.read_assignment()?;
            // TODO: REFACTOR THIS SO THAT IT CHECKS FOR xmlns AT START OF raw_name AND ACTS ACCORDINGLY !!!
            if let Some((prefix, local)) = raw_name.split_once(':') {
                if prefix == "xmlns" {
                    let existing_prefix = namespaces
                        .insert(local.to_string().into_boxed_str(), value.into_boxed_str());
                    if existing_prefix.is_some() {
                        return Err(Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Input contains same namespace prefix (xmlns:prefix=) declaration more than once within the same element.",
                        ));
                    } else if local == "xmlns" {
                        return Err(Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Input declares xmlns prefix (xmlns:xmlns=) which is forbidden in XML.",
                        ));
                    }
                } else {
                    // We don't have all the namespaces yet, so we can't resolve expanded attribute names yet.
                    // Just shove the attributes names into the map raw, and we'll resolve them after this method.
                    let existing =
                        raw_attributes.insert(raw_name.into_boxed_str(), value.into_boxed_str());
                    if existing.is_some() {
                        return Err(Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Input contains same prefixed attribute name (prefix:name=) more than once within the same element.",
                        ));
                    }
                };
            } else if raw_name == "xmlns" {
                let existing_default =
                    namespaces.insert("".to_string().into_boxed_str(), value.into_boxed_str());
                if existing_default.is_some() {
                    return Err(Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Input contains same default namespace (xmlns=) declaration more than once within the same element.",
                        ));
                }
            } else {
                let existing =
                    raw_attributes.insert(raw_name.into_boxed_str(), value.into_boxed_str());
                if existing.is_some() {
                    return Err(Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Input contains unprefixed attribute name (name=) more than once within the same element.",
                        ));
                }
            }
            found_whitespace = self.parser.skip_while(Self::whitespace)?;
        }
        Ok((raw_attributes, namespaces))
    }

    fn resolve_attribute_names(
        raw_attributes: BoxedStrMap,
        inheritable_stack: &[InheritableProperties],
    ) -> Result<HashMap<Name, Box<str>>, Error> {
        let mut attributes = HashMap::new();
        for (raw_name, value) in raw_attributes {
            let (prefix, local) = {
                if let Some((prefix, local)) = raw_name.split_once(':') {
                    (prefix, local.to_string().into_boxed_str())
                } else {
                    ("", raw_name)
                }
            };
            let namespace = if prefix.is_empty() {
                // Attributes without a prefix do NOT inherit the default namespace from this element and its ancestors.
                ""
            } else {
                let resolved_namespace =
                    Self::find_namespace_from_prefix(prefix, inheritable_stack);
                if resolved_namespace.is_empty() {
                    return Err(Error::new(
                        std::io::ErrorKind::InvalidData,
                        format!("Attribute namespace prefix {} not declared!", prefix),
                    ));
                }
                resolved_namespace
            };
            let already_present = attributes.insert(Name::new(local.as_ref(), namespace), value);
            if already_present.is_some() {
                return Err(Error::new(
                    std::io::ErrorKind::InvalidData,
                    format!("Attribute with local name {} and namespace {} declared more than once in same element!", local, namespace),
                ));
            }
        }
        Ok(attributes)
    }

    fn find_namespace_from_prefix<'a>(
        prefix: &str,
        inheritable_stack: &'a [InheritableProperties],
    ) -> &'a str {
        for layer in inheritable_stack.iter().rev() {
            if let Some(n) = layer.namespaces.get(prefix) {
                return n;
            }
        }
        ""
    }

    fn determine_element_name(
        raw_name: &str,
        inheritable_stack: &[InheritableProperties],
    ) -> Result<Name, Error> {
        if let Some((prefix, local_part)) = raw_name.split_once(':') {
            let namespace = Self::find_namespace_from_prefix(prefix, inheritable_stack);
            if namespace.is_empty() {
                return Err(Error::new(
                    std::io::ErrorKind::InvalidData,
                    format!("Namespace prefix \"{}\" not declared!", prefix),
                ));
            }
            Ok(Name::new(local_part, namespace))
        } else {
            let namespace = Self::find_namespace_from_prefix("", inheritable_stack);
            Ok(Name::new(raw_name, namespace))
        }
    }

    fn determine_whitespace_handling(
        inheritable_stack: &[InheritableProperties],
    ) -> Option<WhiteSpaceHandling> {
        // Whitespace handling is inherited from ancestor elements.
        for layer in inheritable_stack.iter().rev() {
            if let Some(n) = layer.whitespace {
                return Some(n);
            }
        }
        // If no ancestor has ever specified how to handle whitespace handling,
        // then it remains a mystery.
        None
    }

    fn determine_language_tag(
        inheritable_stack: &[InheritableProperties],
    ) -> Result<Option<LanguageTag>, Error> {
        // Language is inherited from ancestor elements.
        for layer in inheritable_stack.iter().rev() {
            if let Some(n) = &layer.lang {
                if n.is_empty() {
                    // If the effective xml:lang (after inheritance from ancestors has been
                    // considered) is empty, then we don't need a LanguageTag for this element.
                    return Ok(None);
                } else {
                    return Ok(Some(LanguageTag::new(n)?));
                }
            }
        }
        // If no ancestor had an xml:lang attribute, then there is no default.
        Ok(None)
    }

    fn read_root_contents(
        &mut self,
        parent_stack: &mut Vec<ElementBuilder>,
        inheritable_stack: &mut Vec<InheritableProperties>,
    ) -> Result<Element, Error> {
        loop {
            if self.parser.accept('<')? {
                if self.parser.accept('/')? {
                    let closed_element = self.read_end_tag(parent_stack, inheritable_stack)?;
                    if parent_stack.is_empty() {
                        // This was the end-tag for the root element.
                        return Ok(closed_element);
                    } else {
                        parent_stack
                            .last_mut()
                            .unwrap()
                            .add_child_element(closed_element);
                    }
                } else if self.parser.accept('!')? {
                    if self.parser.accept('[')? {
                        let text = self.read_cdata_section()?;
                        parent_stack.last_mut().unwrap().add_text(text);
                    } else {
                        self.skip_comment()?;
                    }
                } else if self.parser.accept('?')? {
                    let target_name = self.read_qualified_name()?;
                    let pi = self.read_processing_instruction(target_name)?;
                    parent_stack
                        .last_mut()
                        .unwrap()
                        .add_processing_instruction(pi);
                } else {
                    self.read_start_tag(parent_stack, inheritable_stack)?;
                }
            } else {
                let mut processed_text = String::with_capacity(32);
                self.read_char_data(&mut processed_text)?;
                parent_stack.last_mut().unwrap().add_text(processed_text);
            }
        }
    }

    fn read_start_tag(
        &mut self,
        parent_stack: &mut Vec<ElementBuilder>,
        inheritable_stack: &mut Vec<InheritableProperties>,
    ) -> Result<(), Error> {
        let builder = self.builder_from_start_tag(inheritable_stack)?;
        if self.parser.accept('/')? {
            // Self-closing tag indicates empty element.
            self.parser.require('>')?;
            inheritable_stack.pop();
            parent_stack
                .last_mut()
                .unwrap()
                .add_child_element(Element::new(builder));
        } else {
            self.parser.require('>')?;
            parent_stack.push(builder);
        }
        Ok(())
    }

    fn read_end_tag(
        &mut self,
        parent_stack: &mut Vec<ElementBuilder>,
        inheritable_stack: &mut Vec<InheritableProperties>,
    ) -> Result<Element, Error> {
        let raw_name = self.read_qualified_name()?;
        let element_name = Self::determine_element_name(&raw_name, inheritable_stack)?;
        let closed_element = parent_stack.pop().unwrap();
        inheritable_stack.pop();
        if element_name != closed_element.name {
            return Err(Error::new(
                std::io::ErrorKind::InvalidData,
                format!(
                    "Found end tag for element \"{}\" which is not the open element.",
                    &raw_name
                ),
            ));
        }
        let element = Element::new(closed_element);
        self.parser.skip_while(Self::whitespace)?;
        self.parser.require('>')?;
        Ok(element)
    }

    // Reads char data, replacing entities with real characters, and normalising end-of-line characters.
    fn read_attribute_value(&mut self, text: &mut String, wrapper: char) -> Result<(), Error> {
        while !self.parser.peek_sees(wrapper)? {
            // TODO: REWRITE THIS FOR EFFICIENCY (capture value with read() and then check whether it falls within/below common intervals) !!!
            if self.parser.accept('&')? {
                self.read_xml_reference_into_string(text)?;
            } else if self.parser.accept('\r')? {
                // Whether or not a carriage-return is followed by a line-feed, it/they should be replaced by a space.
                self.parser.accept('\n')?;
                text.push(' ');
            } else if self.parser.accept('\n')? || self.parser.accept('\t')? {
                // Newlines (and thus carriage returns) and horizontal tabs are normalised into standard spaces within attribute values.
                text.push(' ');
            } else if self.parser.accept(']')? {
                if self.parser.accept(']')? {
                    if self.parser.peek_sees('>')? {
                        return Err(Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Character data contains sequence ]]> which is not permitted.",
                        ));
                    }
                    text.push(']');
                }
                text.push(']');
            } else {
                let c = self.parser.read()?;
                if TEXT_CHAR(c) {
                    text.push(c);
                } else {
                    return Err(Error::new(
                        std::io::ErrorKind::InvalidData,
                        format!("Input contains invalid XML text character: '{}'", c),
                    ));
                }
            }
        }
        Ok(())
    }

    // Reads char data, replacing entities with real characters, and normalising end-of-line characters.
    fn read_char_data(&mut self, text: &mut String) -> Result<(), Error> {
        while !self.parser.peek_sees('<')? {
            if self.parser.accept('&')? {
                self.read_xml_reference_into_string(text)?;
            } else if self.parser.accept('\r')? {
                // Whether or not a carriage-return is followed by a line-feed, it/they should be replaced by a line-feed.
                self.parser.accept('\n')?;
                text.push('\n');
            } else if self.parser.accept(']')? {
                if self.parser.accept(']')? {
                    if self.parser.peek_sees('>')? {
                        return Err(Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Character data contains sequence ]]> which is not permitted.",
                        ));
                    }
                    text.push(']');
                }
                text.push(']');
            } else {
                let c = self.parser.read()?;
                if TEXT_CHAR(c) {
                    text.push(c);
                } else {
                    return Err(Error::new(
                        std::io::ErrorKind::InvalidData,
                        format!("Input contains invalid XML text character: '{}'", c),
                    ));
                }
            }
        }
        Ok(())
    }

    fn read_xml_reference_into_string(&mut self, s: &mut String) -> Result<(), Error> {
        if self.parser.accept('#')? {
            self.read_character_reference_into_string(s)?;
        } else {
            self.read_entity_reference_into_string(s)?;
        }
        self.parser.require(';')?;
        Ok(())
    }

    fn read_character_reference_into_string(&mut self, s: &mut String) -> Result<(), Error> {
        let codepoint: u32 = if self.parser.accept('x')? {
            if let Some(hex_code) = self.parser.read_up_to(';')? {
                u32::from_str_radix(&hex_code, 16).map_err(|_| {
                    Error::new(
                        std::io::ErrorKind::InvalidData,
                        format!(
                            "Character reference contains invalid hexadecimal '{}'.",
                            &hex_code
                        ),
                    )
                })?
            } else {
                return Err(Error::new(
                    std::io::ErrorKind::InvalidData,
                    "Incomplete hexadecimal character reference.",
                ));
            }
        } else if let Some(decimal_code) = self.parser.read_up_to(';')? {
            decimal_code.parse::<u32>().map_err(|_| {
                Error::new(
                    std::io::ErrorKind::InvalidData,
                    format!(
                        "Character reference contains invalid decimal number '{}'.",
                        &decimal_code
                    ),
                )
            })?
        } else {
            return Err(Error::new(
                std::io::ErrorKind::InvalidData,
                "Incomplete decimal character reference.",
            ));
        };
        let c = char::from_u32(codepoint).ok_or_else(|| {
            Error::new(
                std::io::ErrorKind::InvalidData,
                "Character reference resolves to number which is not a valid Unicode codepoint.",
            )
        })?;
        s.push(c);
        Ok(())
    }

    fn read_entity_reference_into_string(&mut self, s: &mut String) -> Result<(), Error> {
        if let Some(name) = self.parser.read_up_to(';')? {
            let c = match name.as_str() {
                "lt" => '<',
                "gt" => '>',
                "amp" => '&',
                "quot" => '"',
                "apos" => '\'',
                _ => {
                    return Err(Error::new(
                        std::io::ErrorKind::InvalidData,
                        format!("Entity reference \"{}\" not recognised.", &name),
                    ));
                }
            };
            s.push(c);
        } else {
            return Err(Error::new(
                std::io::ErrorKind::InvalidData,
                "Incomplete entity reference.",
            ));
        }
        Ok(())
    }

    fn whitespace(c: char) -> bool {
        c == ' ' || c == '\n' || c == '\r' || c == '\t'
    }
}

#[cfg(test)]
mod tests {
    use std::io::ErrorKind;

    use sipp::decoder::Utf8Decoder;

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    type TestOut = Result<(), Error>;

    fn parse_raw_xml(raw_xml: &str) -> Result<XmlDocument, Error> {
        let bytes = raw_xml.as_bytes();
        let decoder = Utf8Decoder::wrap(bytes);
        let mut xml_parser = XmlParser::wrap(decoder);
        let doc = xml_parser.parse_encodings(Some(&["UTF-8"]))?;
        Ok(doc)
    }

    #[test]
    fn empty_input() -> TestOut {
        let xml = "";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn no_root_element() -> TestOut {
        let xml = "<?xml version='1.0' encoding='UTF-8'?>
        <!-- We have comments -->
        <!-- We have processing instructions -->
        <?cgi exec='some_sript.cgi' ?>
        <?cgi exec='get_busy.cgi' ?>
        <!-- But we've forgotten to actually include any XML elements! -->";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        let error = outcome.err().unwrap();
        assert!(matches!(error.kind(), ErrorKind::InvalidData));
        let text = error.into_inner().unwrap().to_string();
        assert_eq!(text, "Input does not contain any root element!");
        Ok(())
    }

    #[test]
    fn illegal_text_content_before_root() -> TestOut {
        let xml = "<?xml version='1.0' encoding='UTF-8'?>
        Text cannot appear before the root element!
        <root>Legitimate text content</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn illegal_text_content_after_root() -> TestOut {
        let xml = "<?xml version='1.0' encoding='UTF-8'?>
        <root>Legitimate text content</root>
        Text cannot appear before the root element!";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn test_self_closing_root() -> TestOut {
        let xml = "<root/>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert_eq!(root.content().len(), 0);
        assert_eq!(root.text().unwrap(), "");
        Ok(())
    }

    #[test]
    fn test_empty_root() -> TestOut {
        let xml = "<root></root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert_eq!(root.content().len(), 0);
        assert!(root.contains_only_text());
        assert_eq!(root.text().unwrap(), "");
        Ok(())
    }

    #[test]
    fn just_text() -> TestOut {
        let xml = "<root>text</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert!(root.contains_only_text());
        assert_eq!(root.text().unwrap(), "text");
        Ok(())
    }

    #[test]
    fn attributes() -> TestOut {
        let xml = "<root id=\"10\">text</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert_eq!(root.attributes().count(), 1);
        assert_eq!(root.att_opt("id").unwrap(), "10");
        assert!(root.contains_only_text());
        assert_eq!(root.text().unwrap(), "text");
        Ok(())
    }

    #[test]
    fn attributes_with_same_name() -> TestOut {
        let xml = "<root id=\"10\" id='10'>text</root>";
        let outcome = parse_raw_xml(xml);
        println!("outcome: {:?}", outcome);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn attribute_name_starts_with_underscore() -> TestOut {
        let xml =
            "<root _a=\"value\">Underscores are an acceptable start character in XML names.</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert_eq!(root.attributes().count(), 1);
        assert_eq!(root.att_opt("_a").unwrap(), "value");
        Ok(())
    }

    #[test]
    fn attribute_name_starts_with_illegal_start_character() -> TestOut {
        let xml =
            "<root 8a=\"value\">Digits are not a valid start character for an attribute!</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn namespaced_attributes() -> TestOut {
        let xml = "<root xmlns:a=\"example.com/a\" a:id='T3' id=\"10\">text</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert_eq!(root.attributes().count(), 2);
        assert_eq!(root.att_opt("id").unwrap(), "10");
        assert_eq!(root.att_opt(("id", "example.com/a")).unwrap(), "T3");
        assert!(root.contains_only_text());
        assert_eq!(root.text().unwrap(), "text");
        Ok(())
    }

    #[test]
    fn namespace_prefix_contains_colons() -> TestOut {
        let xml = "<root xmlns:a:z='example.com/a'>text</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn element_name_local_part_contains_colons() -> TestOut {
        let xml = "<a:root:element xmlns:a='example.com/a'>text</a:root:element>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn attribute_name_local_part_contains_colons() -> TestOut {
        let xml = "<a:root xmlns:a='example.com/a' a:double:barrel='oops'>text</a:root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn attribute_name_local_part_illegal_start_character() -> TestOut {
        let xml = "<root xmlns:a=\"example.com/a\" a:-name=\"oops\">Hyphen not a valid start character for a name part!</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn attribute_name_prefix_illegal_start_character() -> TestOut {
        let xml = "<root -8:name=\"oops\">Hyphen not a valid start character for a prefix!
        (Also, this -8 prefix is never declared, nor can it be.)</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn root_name_prefix_illegal_start_character() -> TestOut {
        let xml = "<-8:root><child>Hyphen not a valid start character for a prefix!
        (Also, this -8 prefix is never declared, nor can it be.)</child></-8:root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn element_name_prefix_illegal_start_character() -> TestOut {
        let xml = "<root><-8:child>Hyphen not a valid start character for a prefix!
        (Also, this -8 prefix is never declared, nor can it be.)</-8:child></root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn root_element_name_local_part_illegal_start_character() -> TestOut {
        let xml = "<8ball>Digit not a valid start character for a name!</8ball>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn element_name_local_part_illegal_start_character() -> TestOut {
        let xml = "<root><8ball>Digit not a valid start character for a name!</8ball></root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn namespace_prefix_declaration_starts_with_illegal_start_character() -> TestOut {
        let xml = "<root xmlns:.a=\"example.com/a\">Full stop not a valid start character for a namespace prefix!</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn namespace_prefix_declaration_for_xmlns() -> TestOut {
        let xml = "<root xmlns:xmlns=\"http://www.w3.org/2000/xmlns/\">Not allowed to declare xmlns as a namespace prefix!</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn namespaced_attributes_resolving_to_same_extended_name() -> TestOut {
        let xml = "<root xmlns:a=\"example.com/attributes\" a:id=\"T3\" b:id=\"10\" xmlns:b='example.com/attributes'>text</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn default_namespace_twice() -> TestOut {
        let xml = "<root xmlns=\"example.com/default\" xmlns='example.com/default'>text</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn namespace_prefix_declared_twice() -> TestOut {
        let xml = "<t:root xmlns:t=\"example.com/tango\" xmlns:t='example.com/Tango'>text</t:root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn namespace_prefix_reuse_party() -> TestOut {
        let xml = "<root xmlns='example.com/default'>
            <container xmlns:prefix='example.com/alpha'>
                <record xmlns='example.com/beta'>Beta</record>
                <record xmlns='example.com/gamma'>Gamma</record>
                <prefix:record>Alpha</prefix:record>
                <prefix:record xmlns:prefix='example.com/delta'>Delta</prefix:record>
                <prefix:record xmlns:prefix='example.com/epsilon'>Epsilon</prefix:record>
                <prefix:record xmlns:prefix='example.com/zeta'>Zeta</prefix:record>
                <prefix:record>Alpha</prefix:record>
                <record>Default</record>
                <record>Default</record>
                <record>Default</record>
                <record>Default</record>
                <record xmlns='example.com/eta'>Eta</record>
            </container>
        </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().namespace().unwrap(), "example.com/default");
        let container = root.r_child(("container", "example.com/default")).unwrap();
        let records: Vec<&Element> = container.elements().collect();
        let records = records.as_slice();
        assert_eq!(records[0].name().namespace().unwrap(), "example.com/beta");
        assert_eq!(records[1].name().namespace().unwrap(), "example.com/gamma");
        assert_eq!(records[2].name().namespace().unwrap(), "example.com/alpha");
        assert_eq!(records[3].name().namespace().unwrap(), "example.com/delta");
        assert_eq!(
            records[4].name().namespace().unwrap(),
            "example.com/epsilon"
        );
        assert_eq!(records[5].name().namespace().unwrap(), "example.com/zeta");
        assert_eq!(records[6].name().namespace().unwrap(), "example.com/alpha");
        assert_eq!(
            records[7].name().namespace().unwrap(),
            "example.com/default"
        );
        assert_eq!(
            records[8].name().namespace().unwrap(),
            "example.com/default"
        );
        assert_eq!(
            records[9].name().namespace().unwrap(),
            "example.com/default"
        );
        assert_eq!(
            records[10].name().namespace().unwrap(),
            "example.com/default"
        );
        assert_eq!(records[11].name().namespace().unwrap(), "example.com/eta");

        Ok(())
    }

    #[test]
    fn attribute_contains_whitespace() -> TestOut {
        let xml = "<root burst=\"START:newline\ncr-nl\r\ntab\tSTOP\">text</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(
            root.att_opt("burst").unwrap(),
            "START:newline cr-nl tab STOP"
        );
        Ok(())
    }

    #[test]
    fn references_within_element_text() -> TestOut {
        let xml = "<java>if (a &lt; b &amp;&amp; b &gt; c) {
            System.out.println(&quot;b&apos;s biggest!&quot;);
        }</java>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(
            root.text().unwrap(),
            "if (a < b && b > c) {
            System.out.println(\"b's biggest!\");
        }"
        );
        Ok(())
    }

    #[test]
    fn entity_references_within_attribute_value() -> TestOut {
        let xml = "<root id='b&lt;7&gt;&amp;X'><field type='&lt;City&gt; &amp; &lt;Country&gt;'>Göteborg, SE</field></root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.attributes().count(), 1);
        let attribute = root.att_opt("id");
        assert_eq!(attribute.unwrap(), "b<7>&X");

        let field = root.r_child("field").unwrap();
        assert_eq!(field.attributes().count(), 1);
        let attribute = field.att_opt("type");
        assert_eq!(attribute.unwrap(), "<City> & <Country>");
        Ok(())
    }

    #[test]
    fn character_references_within_attribute_values() -> TestOut {
        let xml = "<root id='&#x391;&#916;&#x3a9;'><record locality='&#x4EAC;&#x90fd;'>Ritual failed</record></root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.attributes().count(), 1);
        assert_eq!(root.att_opt("id").unwrap(), "ΑΔΩ");

        let record = root.r_child("record").unwrap();
        assert_eq!(record.att_opt("locality").unwrap(), "京都");
        Ok(())
    }

    #[test]
    fn carriage_return_reference_in_attribute_value() -> TestOut {
        let xml = "<mode player='Zeal&#xA;Ardor'><line chant='ODAC&#xD;DARA&#xD;ARAD&#xD;CADO'>I Caught You</line></mode>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.attributes().count(), 1);
        assert_eq!(root.att_opt("player").unwrap(), "Zeal\nArdor");

        let line = root.r_child("line").unwrap();
        assert_eq!(line.att_opt("chant").unwrap(), "ODAC\rDARA\rARAD\rCADO");
        Ok(())
    }

    #[test]
    fn with_declaration() -> TestOut {
        let xml = "<?xml version='1.0' encoding=\"UTF-8\" standalone='yes'?><root>text</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert!(root.contains_only_text());
        assert_eq!(root.text().unwrap(), "text");
        Ok(())
    }

    #[test]
    fn with_simplest_declaration() -> TestOut {
        let xml = "<?xml version='1.1'?><root>text</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert!(root.contains_only_text());
        assert_eq!(root.text().unwrap(), "text");
        Ok(())
    }

    #[test]
    fn with_declaration_no_encoding() -> TestOut {
        let xml = "<?xml version='1.0' standalone='yes'?><root>text</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert!(root.contains_only_text());
        assert_eq!(root.text().unwrap(), "text");
        Ok(())
    }

    #[test]
    fn nested_xml_with_declaration_no_encoding() -> TestOut {
        let xml = "<?xml version='1.0' standalone='yes'?><root><child>text</child></root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert_eq!(root.content().len(), 1);
        assert!(root.r_child("child").is_ok());
        Ok(())
    }

    #[test]
    fn xml_declaration_not_first_to_appear() -> TestOut {
        let xml = "<!-- Can't have anything before XML declaration! --><?xml version='1.0' standalone='yes'?><root><child>text</child></root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn nested_and_split_over_lines() -> TestOut {
        let xml = "<?xml version='1.0' standalone='yes'?>
        <root>
            <child>text</child>
        </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert_eq!(root.elements().count(), 1);
        assert!(root.r_child("child").is_ok());
        Ok(())
    }

    #[test]
    fn deep_nested() -> TestOut {
        let xml = "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>
        <catalogue>
            <product>
                <id>1</id>
                <name>Paint</name>
                <dimensions>
                    <mass>
                        <amount>3</amount>
                        <unit>kg</unit>
                    </mass>
                    <length>
                        <width>
                            <amount>20</amount>
                            <unit>cm</unit>
                        </width>
                        <depth>
                            <amount>20</amount>
                            <unit>cm</unit>
                        </depth>
                        <height>
                            <amount>30</amount>
                            <unit>cm</unit>
                        </height>
                    </length>
                </dimensions>
            </product>
        </catalogue>";
        let doc = parse_raw_xml(xml)?;
        let catalogue = doc.root();
        assert_eq!(catalogue.name().local_part(), "catalogue");
        println!("catalogue: {:?}", &catalogue);
        assert_eq!(catalogue.elements().count(), 1);
        let product = catalogue.r_child("product").unwrap();
        assert_eq!(product.elements().count(), 3);
        Ok(())
    }

    #[test]
    fn comment_before_root() -> TestOut {
        let xml = "<!-- This is a well-short XML document!-->
        <root>text</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert!(root.contains_only_text());
        assert_eq!(root.text().unwrap(), "text");
        Ok(())
    }

    #[test]
    fn comment_within_root() -> TestOut {
        let xml = "<root><!-- Tuck a comment inside the root element! -->text</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert!(root.contains_only_text());
        assert_eq!(root.text().unwrap(), "text");
        Ok(())
    }

    #[test]
    fn comment_after_root() -> TestOut {
        let xml = "<root>text</root><!--Shove a comment after the root element! -->";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.name().local_part(), "root");
        assert!(root.contains_only_text());
        assert_eq!(root.text().unwrap(), "text");
        Ok(())
    }

    #[test]
    fn comment_ends_with_hyphen() -> TestOut {
        let xml = "<!-- Comments cannot end with a hyphen like this ---><root>text</root>";
        let doc = parse_raw_xml(xml);
        assert!(doc.is_err());
        Ok(())
    }

    #[test]
    fn comment_contains_double_hyphen() -> TestOut {
        let xml = "<!-- Comments cannot contain -- double hyphen within comment text! --><root>text</root>";
        let doc = parse_raw_xml(xml);
        assert!(doc.is_err());
        Ok(())
    }

    #[test]
    fn comment_contains_left_angle_bracket() -> TestOut {
        let xml = "<!-- Comments can contain > left-angle bracket within comment text! -->
        <root><!-- Comments can contain > left-angle bracket within comment text! -->text</root>
        <!-- Comments can contain > left-angle bracket within comment text! -->";
        let doc = parse_raw_xml(xml);
        assert!(doc.is_ok());
        Ok(())
    }

    #[test]
    fn comment_contains_right_angle_bracket() -> TestOut {
        let xml = "<!-- Comments can contain > right-angle bracket within comment text! -->
        <root><!-- Comments can contain > right-angle bracket within comment text! -->text</root>
        <!-- Comments can contain > right-angle bracket within comment text! -->";
        let doc = parse_raw_xml(xml);
        assert!(doc.is_ok());
        Ok(())
    }

    #[test]
    fn comment_w3c_example() -> TestOut {
        let xml = "<!-- declarations for <head> & <body> -->
        <root>text<!-- declarations for <head> & <body> --></root>
        <!-- declarations for <head> & <body> -->";
        let doc = parse_raw_xml(xml);
        assert!(doc.is_ok());
        Ok(())
    }

    #[test]
    fn processing_instruction_before_root() -> TestOut {
        let xml = "<?publisher some sort of instruction to the publisher software?><root></root>";
        let doc = parse_raw_xml(xml)?;
        assert_eq!(doc.processing_instructions().count(), 1);
        let pi = doc.processing_instructions().next().unwrap();
        assert_eq!(pi.target(), "publisher");
        assert_eq!(
            pi.data(),
            "some sort of instruction to the publisher software"
        );
        let root = doc.root();
        assert_eq!(root.elements().count(), 0);
        Ok(())
    }

    #[test]
    fn processing_instructions_before_root() -> TestOut {
        let xml = "<?xml-stylesheet type=\"text/xsl\" href=\"style.xsl\"?><?publisher some sort of instruction to the publisher software?><root></root>";
        let doc = parse_raw_xml(xml)?;
        assert_eq!(doc.processing_instructions().count(), 2);
        let mut pis = doc.processing_instructions();
        let pi = pis.next().unwrap();
        assert_eq!(pi.target(), "xml-stylesheet");
        assert_eq!(pi.data(), "type=\"text/xsl\" href=\"style.xsl\"");
        let pi = pis.next().unwrap();
        assert_eq!(pi.target(), "publisher");
        assert_eq!(
            pi.data(),
            "some sort of instruction to the publisher software"
        );
        let root = doc.root();
        assert_eq!(root.elements().count(), 0);
        Ok(())
    }

    #[test]
    fn processing_instructions_within_root() -> TestOut {
        let xml = "<?xml version='1.0'?>
        <?xml-stylesheet type=\"text/xsl\" href=\"style.xsl\"?>
        <root>
            <?sort by-id?>
            <item id='1'>
                <?highlight disabled?>
                <name>Mercury</name>
            </item>
            <item id='2'>
                <?highlight disabled?>
                <name>Venus</name>
            </item>
            <item id='3'>
                <?highlight enabled?>
                <name>Earth</name>
            </item>
        </root>";
        let doc = parse_raw_xml(xml)?;
        assert_eq!(doc.processing_instructions().count(), 1);
        let pi = doc.processing_instructions().next().unwrap();
        assert_eq!(pi.target(), "xml-stylesheet");
        assert_eq!(pi.data(), "type=\"text/xsl\" href=\"style.xsl\"");

        let root = doc.root();
        assert_eq!(root.processing_instructions().count(), 1);
        let mut pi = root.processing_instructions();
        let pi = pi.next().unwrap();
        assert_eq!(pi.target(), "sort");
        assert_eq!(pi.data(), "by-id");

        assert_eq!(root.elements().count(), 3);
        let mut root_child_elements = root.elements();

        let item = root_child_elements.next().unwrap();
        assert_eq!(item.name().local_part(), "item");
        assert_eq!(item.processing_instructions().count(), 1);
        let pi = item.processing_instructions().next().unwrap();
        assert_eq!(pi.target(), "highlight");
        assert_eq!(pi.data(), "disabled");
        assert_eq!(item.r_child("name").unwrap().text().unwrap(), "Mercury");

        let item = root_child_elements.next().unwrap();
        assert_eq!(item.name().local_part(), "item");
        assert_eq!(item.processing_instructions().count(), 1);
        let pi = item.processing_instructions().next().unwrap();
        assert_eq!(pi.target(), "highlight");
        assert_eq!(pi.data(), "disabled");
        assert_eq!(item.r_child("name").unwrap().text().unwrap(), "Venus");

        let item = root_child_elements.next().unwrap();
        assert_eq!(item.name().local_part(), "item");
        assert_eq!(item.processing_instructions().count(), 1);
        let pi = item.processing_instructions().next().unwrap();
        assert_eq!(pi.target(), "highlight");
        assert_eq!(pi.data(), "enabled");
        assert_eq!(item.r_child("name").unwrap().text().unwrap(), "Earth");

        Ok(())
    }

    #[test]
    fn processing_instruction_without_xml_declaration() -> TestOut {
        let xml = "<?xml-stylesheet type=\"text/xsl\" href=\"style.xsl\"?>
        <root>
            <child>text</child>
        </root>";
        let doc = parse_raw_xml(xml)?;
        assert_eq!(doc.processing_instructions().count(), 1);
        let pi = doc.processing_instructions().next().unwrap();
        assert_eq!(pi.target(), "xml-stylesheet");
        assert_eq!(pi.data(), "type=\"text/xsl\" href=\"style.xsl\"");
        assert_eq!(doc.root().r_child("child").unwrap().text().unwrap(), "text");
        Ok(())
    }

    #[test]
    fn processing_instruction_before_root_contains_invalid_character() -> TestOut {
        let xml = "<?cgi key='\u{FFFF}' ?>
        <root>
            <child>text</child>
        </root>";
        let doc = parse_raw_xml(xml);
        assert!(doc.is_err());
        Ok(())
    }

    #[test]
    fn processing_instruction_within_root_contains_invalid_character() -> TestOut {
        let xml = "
        <root>
            <?cgi key='\u{FFFF}' ?>
            <child>text</child>
        </root>";
        let doc = parse_raw_xml(xml);
        assert!(doc.is_err());
        Ok(())
    }

    #[test]
    fn processing_instruction_after_root_contains_invalid_character() -> TestOut {
        let xml = "<root>
            <child>text</child>
        </root><?cgi key='\u{FFFF}' ?>";
        let doc = parse_raw_xml(xml);
        assert!(doc.is_err());
        Ok(())
    }

    #[test]
    fn processing_instruction_contains_question_mark() -> TestOut {
        let xml = "<?cgi url='example.com/path/page?query=Q&mode=fetch' ?>
        <root>
            <child>text</child>
        </root>";
        let doc = parse_raw_xml(xml)?;
        assert_eq!(doc.processing_instructions().count(), 1);
        let pi = doc.processing_instructions().next().unwrap();
        assert_eq!(pi.target(), "cgi");
        assert_eq!(pi.data(), "url='example.com/path/page?query=Q&mode=fetch' ");
        Ok(())
    }

    #[test]
    fn processing_instruction_contains_line_breaks() -> TestOut {
        let xml = "<?whte_rbt.obj
call link.sst
{security, perimeter}
set to off.
?>
        <curV>
            <Handl>ssm.dt</Handl>
            <tempRgn>itm.dd2</tempRgn>
        </curV>";
        let doc = parse_raw_xml(xml)?;
        assert_eq!(doc.processing_instructions().count(), 1);
        let pi = doc.processing_instructions().next().unwrap();
        assert_eq!(pi.target(), "whte_rbt.obj");
        assert_eq!(
            pi.data(),
            "call link.sst\n{security, perimeter}\nset to off.\n"
        );
        let root = doc.root();
        assert_eq!(root.name().local_part(), "curV");
        let handl = root.r_child("Handl").unwrap();
        assert_eq!(handl.text().unwrap(), "ssm.dt");
        let temp_rgn = root.r_child("tempRgn").unwrap();
        assert_eq!(temp_rgn.text().unwrap(), "itm.dd2");
        assert_eq!(root.elements().count(), 2);
        Ok(())
    }

    #[test]
    fn world_bank_example() -> TestOut {
        let xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<Root xmlns:wb=\"http://www.worldbank.org\">
  <data>
    <record>
      <field name=\"Country or Area\" key=\"ABW\">Aruba</field>
      <field name=\"Item\" key=\"AG.LND.FRST.ZS\">Forest area (% of land area)</field>
      <field name=\"Year\">1960</field>
      <field name=\"Value\" />
    </record>
  </data>
</Root>";
        parse_raw_xml(xml)?;
        Ok(())
    }

    #[test]
    fn cdata_section_only() -> TestOut {
        let xml = "<root>
            <one><![CDATA[<name>Lapsis Beeftech</name>]]></one>
            <two><![CDATA[]]]]><![CDATA[>]]></two>
        </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let one = root.r_child("one").unwrap();
        assert_eq!(one.text().unwrap(), "<name>Lapsis Beeftech</name>");

        let two = root.r_child("two").unwrap();
        assert_eq!(two.text().unwrap(), "]]>");
        Ok(())
    }

    #[test]
    fn cdata_section_mixed_into_text() -> TestOut {
        let xml = "<root>
            <one>Just type: `<![CDATA[exec {file_descriptor}<\"./data.csv\"
        exec {file_descriptor}>&-]]>`</one>
            <two>Unescaped character data can go into a <![CDATA[<![CDATA[]]]]><![CDATA[>]]> section.</two>
        </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let one = root.r_child("one").unwrap();
        assert_eq!(
            one.text().unwrap(),
            "Just type: `exec {file_descriptor}<\"./data.csv\"
        exec {file_descriptor}>&-`"
        );

        let two = root.r_child("two").unwrap();
        assert_eq!(
            two.text().unwrap(),
            "Unescaped character data can go into a <![CDATA[]]> section."
        );
        Ok(())
    }

    #[test]
    fn cdata_section_right_square_bracket_party() -> TestOut {
        let xml = "<root>
            <one><![CDATA[CDATA section content can contain ]] and also ]> but not two right-square brackets followed by a right-angle bracket.]]> (Info)</one>
            <two><![CDATA[Also nothing wrong with ]]]]]]]] nor ]]] nor >>>> nor ]>]>]>]> neither!]]> (Info)</two>
        </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let one = root.r_child("one").unwrap();
        assert_eq!(
            one.text().unwrap(),
            "CDATA section content can contain ]] and also ]> but not two right-square brackets followed by a right-angle bracket. (Info)"
        );

        let two = root.r_child("two").unwrap();
        assert_eq!(
            two.text().unwrap(),
            "Also nothing wrong with ]]]]]]]] nor ]]] nor >>>> nor ]>]>]>]> neither! (Info)"
        );
        Ok(())
    }

    #[test]
    fn cdata_section_contains_invalid_character() -> TestOut {
        let xml = "<root>
            <one>Just type: `<![CDATA[exec \u{FFFF}{file_descriptor}<\"./data.csv\"
        exec {file_descriptor}>&-]]>`</one>
            <two>Unescaped character data can go into a <![CDATA[<![CDATA[]]]]><![CDATA[>]]> section.</two>
        </root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn xml_lang_in_root() -> TestOut {
        let xml =
            "<root xml:lang=\"en-GB\"><child><text>Blimey, Mary Poppins!</text></child></root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.language_tag().unwrap().value(), "en-GB");
        let child = root.r_child("child").unwrap();
        assert_eq!(child.language_tag().unwrap().value(), "en-GB");
        let text = child.r_child("text").unwrap();
        assert_eq!(text.language_tag().unwrap().value(), "en-GB");
        Ok(())
    }

    #[test]
    fn xml_lang_in_child() -> TestOut {
        let xml =
            "<root><child xml:lang=\"en-GB\"><text>Blimey, Mary Poppins!</text></child></root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert!(root.language_tag().is_none());
        let child = root.r_child("child").unwrap();
        assert_eq!(child.language_tag().unwrap().value(), "en-GB");
        let text = child.r_child("text").unwrap();
        assert_eq!(text.language_tag().unwrap().value(), "en-GB");
        Ok(())
    }

    #[test]
    fn xml_lang_in_every_layer() -> TestOut {
        let xml = "<root xml:lang=\"en\">
                <child xml:lang=\"sv\">
                    <text xml:lang=\"sv-SE\">tvättmaskin</text>
                    <text xml:lang=\"sv-AX\">bykmaskin</text>
                </child>
            </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.language_tag().unwrap().value(), "en");
        let child = root.r_child("child").unwrap();
        assert_eq!(child.language_tag().unwrap().value(), "sv");
        let first_text = child.first("text").iter().next().unwrap();
        assert_eq!(first_text.language_tag().unwrap().value(), "sv-SE");
        let second_text = child.nth("text", 1).iter().next().unwrap();
        assert_eq!(second_text.language_tag().unwrap().value(), "sv-AX");
        Ok(())
    }

    #[test]
    fn xml_lang_empty_in_descendant() -> TestOut {
        let xml = "<root xml:lang=\"en\">
                <child xml:lang=\"\">
                    <text xml:lang=\"sv-SE\">tvättmaskin</text>
                    <text xml:lang=\"sv-AX\">bykmaskin</text>
                    <text>washing machine</text>
                </child>
            </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        println!("root:\n{:?}", root);
        assert_eq!(root.language_tag().unwrap().value(), "en");
        let child = root.r_child("child").unwrap();
        assert!(child.language_tag().is_none());
        let first_text = child.first("text").iter().next().unwrap();
        assert_eq!(first_text.language_tag().unwrap().value(), "sv-SE");
        let second_text = child.nth("text", 1).iter().next().unwrap();
        assert_eq!(second_text.language_tag().unwrap().value(), "sv-AX");
        let third_text = child.nth("text", 2).iter().next().unwrap();
        assert!(third_text.language_tag().is_none());
        Ok(())
    }

    #[test]
    fn xml_lang_empty_in_leaf() -> TestOut {
        let xml = "<root xml:lang=\"en\">
                <child xml:lang=\"sv\">
                    <text xml:lang=\"sv-SE\">tvättmaskin</text>
                    <text xml:lang=\"sv-AX\">bykmaskin</text>
                    <text xml:lang=\"\">washing machine</text>
                </child>
            </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.language_tag().unwrap().value(), "en");
        let child = root.r_child("child").unwrap();
        assert_eq!(child.language_tag().unwrap().value(), "sv");
        let first_text = child.first("text").iter().next().unwrap();
        assert_eq!(first_text.language_tag().unwrap().value(), "sv-SE");
        let second_text = child.nth("text", 1).iter().next().unwrap();
        assert_eq!(second_text.language_tag().unwrap().value(), "sv-AX");
        let third_text = child.nth("text", 2).iter().next().unwrap();
        assert!(third_text.language_tag().is_none());
        Ok(())
    }

    #[test]
    fn xml_lang_empty_in_self_closing_leaf() -> TestOut {
        let xml = "<root xml:lang=\"en\">
                <child xml:lang=\"sv\">
                    <text xml:lang=\"sv-SE\" value='tvättmaskin'/>
                    <text xml:lang=\"sv-AX\" value='bykmaskin'/>
                    <text xml:lang=\"\" value='washing machine'/>
                </child>
            </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(root.language_tag().unwrap().value(), "en");
        let child = root.r_child("child").unwrap();
        assert_eq!(child.language_tag().unwrap().value(), "sv");
        let first_text = child.first("text").iter().next().unwrap();
        assert_eq!(first_text.language_tag().unwrap().value(), "sv-SE");
        let second_text = child.nth("text", 1).iter().next().unwrap();
        assert_eq!(second_text.language_tag().unwrap().value(), "sv-AX");
        let third_text = child.nth("text", 2).iter().next().unwrap();
        assert!(third_text.language_tag().is_none());
        Ok(())
    }

    #[test]
    fn xml_space_empty_in_root() -> TestOut {
        let xml = "<root xml:space=''></root>";
        let outcome = parse_raw_xml(xml);
        // It is an error for xml:space to have any value other than "default" or "preserve".
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn xml_space_invalid_in_root() -> TestOut {
        let xml = "<root xml:space='whatever'></root>";
        let outcome = parse_raw_xml(xml);
        // It is an error for xml:space to have any value other than "default" or "preserve".
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn xml_space_in_root() -> TestOut {
        let xml = "<root xml:space='default'><child><inner>    text adrift in insignificant whitespace     </inner></child></root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(
            root.white_space_handling().unwrap(),
            WhiteSpaceHandling::Default
        );
        let child = root.r_child("child").unwrap();
        assert_eq!(
            child.white_space_handling().unwrap(),
            WhiteSpaceHandling::Default
        );
        let inner = child.r_child("inner").unwrap();
        assert_eq!(
            inner.white_space_handling().unwrap(),
            WhiteSpaceHandling::Default
        );
        Ok(())
    }

    #[test]
    fn xml_space_empty_in_child() -> TestOut {
        let xml = "<root><child xml:space=''>text</child></root>";
        let outcome = parse_raw_xml(xml);
        // It is an error for xml:space to have any value other than "default" or "preserve".
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn xml_space_invalid_in_child() -> TestOut {
        let xml = "<root><child xml:space='whatevs'>text</child></root>";
        let outcome = parse_raw_xml(xml);
        // It is an error for xml:space to have any value other than "default" or "preserve".
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn xml_space_in_child() -> TestOut {
        let xml = "<root><child xml:space='default'><inner>    text adrift in insignificant whitespace     </inner></child></root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert!(root.white_space_handling().is_none());
        let child = root.r_child("child").unwrap();
        assert_eq!(
            child.white_space_handling().unwrap(),
            WhiteSpaceHandling::Default
        );
        let inner = child.r_child("inner").unwrap();
        assert_eq!(
            inner.white_space_handling().unwrap(),
            WhiteSpaceHandling::Default
        );
        Ok(())
    }

    #[test]
    fn xml_space_in_every_layer() -> TestOut {
        let xml = "<root xml:space='default'><child xml:space='default'><inner xml:space='preserve'> p45Sw0rD inc1ud1n9 wh17e5P4CE  </inner></child></root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(
            root.white_space_handling().unwrap(),
            WhiteSpaceHandling::Default
        );
        let child = root.r_child("child").unwrap();
        assert_eq!(
            child.white_space_handling().unwrap(),
            WhiteSpaceHandling::Default
        );
        let inner = child.r_child("inner").unwrap();
        assert_eq!(
            inner.white_space_handling().unwrap(),
            WhiteSpaceHandling::Preserve
        );
        Ok(())
    }

    #[test]
    fn xml_space_in_multiple_leaf_elements() -> TestOut {
        let xml = "<root xml:space='default'>
            <child xml:space='default'>
                <inner type='password' xml:space='preserve'> p45Sw0rD inc1ud1n9 wh17e5P4CE  </inner>
                <inner type='username' xml:space='default'>chad.chadderson</inner>
                <inner type='bio' xml:space='default'>Totally just livin' my best life, bro.</inner>
            </child>
        </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        assert_eq!(
            root.white_space_handling().unwrap(),
            WhiteSpaceHandling::Default
        );
        let child = root.r_child("child").unwrap();
        assert_eq!(
            child.white_space_handling().unwrap(),
            WhiteSpaceHandling::Default
        );
        let password = child
            .first("inner")
            .with_attribute("type", "password")
            .iter()
            .next()
            .unwrap();
        assert_eq!(
            password.white_space_handling(),
            Some(WhiteSpaceHandling::Preserve)
        );
        let username = child
            .first("inner")
            .with_attribute("type", "username")
            .iter()
            .next()
            .unwrap();
        assert_eq!(
            username.white_space_handling(),
            Some(WhiteSpaceHandling::Default)
        );
        let bio = child
            .first("inner")
            .with_attribute("type", "bio")
            .iter()
            .next()
            .unwrap();
        assert_eq!(
            bio.white_space_handling(),
            Some(WhiteSpaceHandling::Default)
        );
        Ok(())
    }

    #[test]
    fn invalid_character_in_root_text_content() -> TestOut {
        let xml = "<root>Character \u{FFFF} is not valid within XML text content!</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn invalid_character_in_child_text_content() -> TestOut {
        let xml =
            "<root><child>Character \u{FFFE} is not valid within XML text content!</child></root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn invalid_character_in_root_attribute_content() -> TestOut {
        let xml = "<root value='Character \u{FFFE} is not valid within XML attribute content!'>Uneventful text</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn invalid_character_in_child_attribute_content() -> TestOut {
        let xml =
            "<root><child value='Character \u{FFFF} is not valid within XML attribute content!'>Boring text</child></root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn invalid_character_in_pre_root_comment() -> TestOut {
        let xml =
            "<!--Character \u{FFFE}  is not valid within XML comments!--><root>Uneventful text</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn invalid_character_within_root_comment() -> TestOut {
        let xml = "<root><!--Character \u{FFFF} is not valid within XML comments!-->Uneventful text</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn invalid_character_in_post_root_comment() -> TestOut {
        let xml = "<root>Uneventful text</root><!--Character \u{FFFE} is not valid within XML comments!-->";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn namespace_cursors() -> TestOut {
        let xml = "<root>
            <child xmlns='ns1'>
                <grandchild xmlns='ns1'>
                    <greatgrandchild xmlns='ns1'>
                        <greatgreatgrandchild xmlns='ns1'/>
                        <greatgreatgrandchild xmlns=''>Blank namespace!</greatgreatgrandchild>
                    </greatgrandchild>
                </grandchild>
            </child>
        </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let namespaced_descendant = root
            .pre_ns("ns1")
            .req("child")
            .req("grandchild")
            .req("greatgrandchild")
            .opt("greatgreatgrandchild")
            .element()
            .unwrap()
            .unwrap();
        assert_eq!(
            namespaced_descendant.name().local_part(),
            "greatgreatgrandchild"
        );
        assert_eq!(namespaced_descendant.name().namespace().unwrap(), "ns1");
        let default_namespace_descendant = root
            .pre_ns("ns1")
            .req("child")
            .req("grandchild")
            .req("greatgrandchild")
            // By explicitly specifying an empty string here, we ask for an element which is not
            // in any namespace, and we ignore the cursor namespace specified above.
            .opt(("greatgreatgrandchild", ""))
            .element()
            .unwrap()
            .unwrap();
        assert_eq!(
            default_namespace_descendant.name().local_part(),
            "greatgreatgrandchild"
        );
        assert!(default_namespace_descendant.name().namespace().is_none());
        Ok(())
    }

    #[test]
    fn namespace_cursors_toggle_namespace() -> TestOut {
        let xml = "<root>
            <child xmlns='ns1'>
                <grandchild xmlns='ns1'>
                    <greatgrandchild xmlns='ns2'>
                        <greatgreatgrandchild xmlns='ns2'/>
                        <greatgreatgrandchild xmlns=''>Blank namespace!</greatgreatgrandchild>
                    </greatgrandchild>
                </grandchild>
            </child>
        </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let namespaced_descendant = root
            .pre_ns("ns1")
            .req("child")
            .req("grandchild")
            .pre_ns("ns2")
            .req("greatgrandchild")
            .opt("greatgreatgrandchild")
            .element()
            .unwrap()
            .unwrap();
        assert_eq!(
            namespaced_descendant.name().local_part(),
            "greatgreatgrandchild"
        );
        assert_eq!(namespaced_descendant.name().namespace().unwrap(), "ns2");
        let default_namespace_descendant = root
            .pre_ns("ns1")
            .req("child")
            .req("grandchild")
            // Rather than call `namespace` to change the cursor namespace, we can still just
            // explicitly use a (name, namespace) tuple to specify the target namespace for this
            // specific child (without altering the cursor namespace).
            .req(("greatgrandchild", "ns2"))
            // By explicitly specifying an empty string here, we ask the cursor to target the
            // blank namespace.
            .pre_ns("")
            .opt("greatgreatgrandchild")
            .element()
            .unwrap()
            .unwrap();
        assert_eq!(
            default_namespace_descendant.name().local_part(),
            "greatgreatgrandchild"
        );
        assert!(default_namespace_descendant.name().namespace().is_none());
        Ok(())
    }

    #[test]
    fn namespace_path() -> TestOut {
        let xml = "<Boroughs xmlns:ns1=\"namespace1\" xmlns:ns2=\"namespace2\">
            <!-- Note: this is not a full list of boroughs, nor wards within each borough. -->
            <ns1:Borough>
                <ns2:Name>City Of Westminster</ns2:Name>
                <ns1:Wards>
                    <ns1:Ward>
                        <ns2:Name>Abbey Road</ns2:Name>
                        <fileUnder>A</fileUnder>
                    </ns1:Ward>
                    <ns1:Ward>
                        <ns2:Name>Bayswater</ns2:Name>
                        <fileUnder>B</fileUnder>
                    </ns1:Ward>
                    <ns1:Ward>
                        <ns2:Name>Church Street</ns2:Name>
                        <fileUnder>C</fileUnder>
                    </ns1:Ward>
                    <ns1:Ward>
                        <ns2:Name>Harrow Road</ns2:Name>
                        <fileUnder>H</fileUnder>
                    </ns1:Ward>
                    <ns1:Ward>
                        <ns2:Name>Hyde Park</ns2:Name>
                        <fileUnder>H</fileUnder>
                    </ns1:Ward>
                </ns1:Wards>
            </ns1:Borough>
            <ns1:Borough>
                <ns2:Name>Hackney</ns2:Name>
                <ns1:Wards>
                    <ns1:Ward>
                        <ns2:Name>Brownswood</ns2:Name>
                        <fileUnder>B</fileUnder>
                    </ns1:Ward>
                    <ns1:Ward>
                        <ns2:Name>Cazenove</ns2:Name>
                        <fileUnder>C</fileUnder>
                    </ns1:Ward>
                    <ns1:Ward>
                        <ns2:Name>Clissold</ns2:Name>
                        <fileUnder>C</fileUnder>
                    </ns1:Ward>
                    <ns1:Ward>
                        <ns2:Name>Dalston</ns2:Name>
                        <fileUnder>D</fileUnder>
                    </ns1:Ward>
                    <ns1:Ward>
                        <ns2:Name>De Beauvoir</ns2:Name>
                        <fileUnder>B</fileUnder>
                    </ns1:Ward>
                </ns1:Wards>
            </ns1:Borough>
            <ns1:Borough>
                <ns2:Name>Royal Borough Of Kensington And Chelsea</ns2:Name>
                <ns1:Wards>
                    <ns1:Ward>
                        <ns2:Name>Abingdon</ns2:Name>
                        <fileUnder>A</fileUnder>
                    </ns1:Ward>
                    <ns1:Ward>
                        <ns2:Name>Brompton &amp; Hans Town</ns2:Name>
                        <fileUnder>B</fileUnder>
                    </ns1:Ward>
                    <ns1:Ward>
                        <ns2:Name>Campden</ns2:Name>
                        <fileUnder>C</fileUnder>
                    </ns1:Ward>
                    <ns1:Ward>
                        <ns2:Name>Chelsea Riverside</ns2:Name>
                        <fileUnder>C</fileUnder>
                    </ns1:Ward>
                    <ns1:Ward>
                        <ns2:Name>Colville</ns2:Name>
                        <fileUnder>C</fileUnder>
                    </ns1:Ward>
                </ns1:Wards>
            </ns1:Borough>
        </Boroughs>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let borough_names: Vec<&str> = root
            .pre_ns("namespace1")
            .all("Borough")
            .first(("Name", "namespace2"))
            .iter()
            .filter_map(|n| n.text().ok())
            .collect();
        assert_eq!(
            borough_names,
            vec![
                "City Of Westminster",
                "Hackney",
                "Royal Borough Of Kensington And Chelsea"
            ]
        );
        let wards_initial_letters: Vec<&str> = root
            .pre_ns("namespace1")
            .all("Borough")
            .first("Wards")
            .all("Ward")
            // The "fileUnder" element has no namespace, so explicitly specify an empty string
            // here to tell XmlPath not to look in the assumed "namespace1" namespace.
            .first(("fileUnder", ""))
            .iter()
            .filter_map(|f| f.text().ok())
            .collect();
        println!("wards_initial_letters: {:?}", wards_initial_letters);
        let mut filing_counts = HashMap::with_capacity(8);
        for letter in wards_initial_letters {
            *filing_counts.entry(letter).or_insert(0) += 1;
        }
        assert_eq!(filing_counts.get("B"), Some(&4));
        Ok(())
    }

    #[test]
    fn namespace_path_attributes() -> TestOut {
        let xml = "<root xmlns:ns1=\"namespace1\" xmlns:ns2=\"namespace2\">
            <list ns1:type=\"bits\">
                <entry ns2:availability='y'>bit1</entry>
                <entry ns2:availability='n'>bit2</entry>
                <entry ns2:availability='y'>bit3</entry>
            </list>
            <list ns1:type=\"bobs\">
                <entry ns2:availability='y'>bob1</entry>
                <entry ns2:availability='y'>bob2</entry>
                <entry ns2:availability='n'>bob3</entry>
            </list>
            <ns1:list ns1:type=\"tabs\">
                <ns1:entry availability='n'>tab1</ns1:entry>
                <ns1:entry availability='y'>tab2</ns1:entry>
                <ns1:entry availability='y'>tab3</ns1:entry>
            </ns1:list>
            <list xmlns=\"namespace2\" type=\"pips\">
                <entry availability='n'>pip1</entry>
                <entry availability='y'>pip2</entry>
                <entry availability='n'>pip3</entry>
            </list>
        </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        // All three of these assertions represent the same thing, but with different
        // permutations of the same XmlPath namespace targeting.
        assert_eq!(
            root.all("list")
                .all("entry")
                .with_attribute(("availability", "namespace2"), "y")
                .iter()
                .count(),
            4
        );
        assert_eq!(
            root.all("list")
                .all("entry")
                .pre_ns("namespace2")
                .with_attribute("availability", "y")
                .iter()
                .count(),
            4
        );
        assert_eq!(
            root.pre_ns("namespace2")
                .all(("list", ""))
                .all(("entry", ""))
                .with_attribute("availability", "y")
                .iter()
                .count(),
            4
        );

        // Now check that the actual content is as expected.
        let available_bits_and_bobs: Vec<&str> = root
            .all("list")
            .all("entry")
            .pre_ns("namespace2")
            .with_attribute("availability", "y")
            .iter()
            .filter_map(|e| e.text().ok())
            .collect();
        assert!(available_bits_and_bobs.contains(&"bit1"));
        assert!(available_bits_and_bobs.contains(&"bit3"));
        assert!(available_bits_and_bobs.contains(&"bob1"));
        assert!(available_bits_and_bobs.contains(&"bob2"));
        assert!(!available_bits_and_bobs.contains(&"bit2"));
        assert!(!available_bits_and_bobs.contains(&"bob3"));

        // Now check for an attribute which is not in any namespace.
        let tabs: Vec<&str> = root
            .pre_ns("namespace1")
            .all("list")
            .all("entry")
            // By specifying an explicity empty string, we ask XmlPath to only consider
            // attributes which have no namespace.
            .with_attribute(("availability", ""), "y")
            .iter()
            .filter_map(|e| e.text().ok())
            .collect();
        assert_eq!(tabs, vec!["tab2", "tab3"]);

        // Now check that the attributes do not inherit the default `xmlns=` namespace.
        let pips: Vec<&str> = root
            .pre_ns("namespace2")
            .all("list")
            .all("entry")
            // The default namespace specified by `xmlns=` never applies to attributes, so to
            // target the unprefixed "availability" attribute we need to use an empty namespace.
            .with_attribute(("availability", ""), "y")
            .iter()
            .filter_map(|e| e.text().ok())
            .collect();
        assert_eq!(pips, vec!["pip2"]);
        // Same test again, but with `namespace("")` instead of a NameRef tuple.
        let pips: Vec<&str> = root
            .pre_ns("namespace2")
            .all("list")
            .all("entry")
            .pre_ns("")
            .with_attribute("availability", "y")
            .iter()
            .filter_map(|e| e.text().ok())
            .collect();
        assert_eq!(pips, vec!["pip2"]);

        Ok(())
    }

    #[test]
    fn rustdoc_found_bug_namespace() -> TestOut {
        let xml = "
<!-- The root element declares that the default namespace for it and its descendants should
be the given URI. It also declares that any element/attribute using prefix 'pfx' belongs to a
namespace with a different URI. -->
<root xmlns='example.com/DefaultNamespace' xmlns:pfx='example.com/OtherNamespace'>
    <one>This child element has no prefix, so it inherits the default namespace.</one>
    <pfx:two>This child element has prefix pfx, so inherits the other namespace.</pfx:two>
    <pfx:three pfx:key='value'>Attribute names can be prefixed too.</pfx:three>
    <four key2='value2'>Unprefixed attribute names do *not* inherit namespaces.</four>
    <five xmlns='' key3='value3'>The default namespace can be cleared too.</five>
</root>
";
        let xml_doc = parse_raw_xml(xml)?;
        let root = xml_doc.root();
        let three_key = root
            .pre_ns("example.com/OtherNamespace")
            .req("three")
            .att_req("key")
            .unwrap();
        assert_eq!(three_key, "value");
        assert_eq!(
            root.pre_ns("example.com/OtherNamespace")
                .opt("three")
                .att_opt("key")
                .unwrap(),
            "value"
        );

        let four_key = root
            .pre_ns("example.com/DefaultNamespace")
            .req("four")
            .pre_ns("")
            .att_req("key2")
            .unwrap();
        assert_eq!(four_key, "value2");
        assert_eq!(
            root.pre_ns("example.com/DefaultNamespace")
                .opt("four")
                .pre_ns("")
                .att_opt("key2")
                .unwrap(),
            "value2"
        );

        let five_key = root.req(("five", "")).att_req(("key3", "")).unwrap();
        assert_eq!(five_key, "value3");
        assert_eq!(
            root.opt(("five", "")).att_opt(("key3", "")).unwrap(),
            "value3"
        );

        Ok(())
    }

    #[test]
    fn pre_ns_empty_xml_path() -> TestOut {
        let xml = "<pim>
    <inboxes>
        <inbox>
            <messages>
                <message>
                    <date>202402101605</date>
                    <title>First text.</title>
                </message>
                <message>
                    <date>202402101606</date>
                    <title>Second text.</title>
                </message>
                <message>
                    <date>202402101608</date>
                    <title>Third text.</title>
                </message>
                <message>
                    <date>202402101609</date>
                    <title>Fourth text.</title>
                </message>
            </messages>
        </inbox>
    </inboxes>
</pim>";
        let xml_doc = parse_raw_xml(xml)?;
        let texts: Vec<&str> = xml_doc
            .root()
            .pre_ns("")
            .first("inboxes")
            .first("inbox")
            .first("messages")
            .all("message")
            .first("title")
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(texts.len(), 4);
        assert_eq!(
            texts,
            vec!["First text.", "Second text.", "Third text.", "Fourth text."]
        );
        Ok(())
    }

    #[test]
    fn pre_ns_default_namespace_xml_path() -> TestOut {
        let xml = "<pim xmlns='example.com/ns'>
    <inboxes>
        <inbox>
            <messages>
                <message>
                    <date>202402101605</date>
                    <title>First text.</title>
                </message>
                <message>
                    <date>202402101606</date>
                    <title>Second text.</title>
                </message>
                <message>
                    <date>202402101608</date>
                    <title>Third text.</title>
                </message>
                <message>
                    <date>202402101609</date>
                    <title>Fourth text.</title>
                </message>
            </messages>
        </inbox>
    </inboxes>
</pim>";
        let xml_doc = parse_raw_xml(xml)?;
        let texts: Vec<&str> = xml_doc
            .root()
            .pre_ns("example.com/ns")
            .first("inboxes")
            .first("inbox")
            .first("messages")
            .all("message")
            .first("title")
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(texts.len(), 4);
        assert_eq!(
            texts,
            vec!["First text.", "Second text.", "Third text.", "Fourth text."]
        );
        Ok(())
    }

    #[test]
    fn pre_ns_default_namespace_changes_midway_xml_path() -> TestOut {
        let xml = "<pim xmlns='example.com/ns'>
    <inboxes>
        <inbox>
            <messages xmlns='example.com/msg'>
                <message>
                    <date>202402101605</date>
                    <title>First text.</title>
                </message>
                <message>
                    <date>202402101606</date>
                    <title>Second text.</title>
                </message>
                <message>
                    <date>202402101608</date>
                    <title>Third text.</title>
                </message>
                <message>
                    <date>202402101609</date>
                    <title>Fourth text.</title>
                </message>
            </messages>
        </inbox>
    </inboxes>
</pim>";
        let xml_doc = parse_raw_xml(xml)?;
        let texts: Vec<&str> = xml_doc
            .root()
            .pre_ns("example.com/ns")
            .first("inboxes")
            .first("inbox")
            .pre_ns("example.com/msg")
            .first("messages")
            .all("message")
            .first("title")
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(texts.len(), 4);
        assert_eq!(
            texts,
            vec!["First text.", "Second text.", "Third text.", "Fourth text."]
        );
        Ok(())
    }

    #[test]
    fn pre_ns_default_namespace_overriden_by_prefix_midway_xml_path() -> TestOut {
        let xml = "<pim xmlns='example.com/ns' xmlns:msg='example.com/msg'>
    <inboxes>
        <inbox>
            <msg:messages>
                <msg:message>
                    <msg:date>202402101605</msg:date>
                    <msg:title>First text.</msg:title>
                </msg:message>
                <msg:message>
                    <msg:date>202402101606</msg:date>
                    <msg:title>Second text.</msg:title>
                </msg:message>
                <msg:message>
                    <msg:date>202402101608</msg:date>
                    <msg:title>Third text.</msg:title>
                </msg:message>
                <msg:message>
                    <msg:date>202402101609</msg:date>
                    <msg:title>Fourth text.</msg:title>
                </msg:message>
            </msg:messages>
        </inbox>
    </inboxes>
</pim>";
        let xml_doc = parse_raw_xml(xml)?;
        let texts: Vec<&str> = xml_doc
            .root()
            .pre_ns("example.com/ns")
            .first("inboxes")
            .first("inbox")
            .pre_ns("example.com/msg")
            .first("messages")
            .all("message")
            .first("title")
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(texts.len(), 4);
        assert_eq!(
            texts,
            vec!["First text.", "Second text.", "Third text.", "Fourth text."]
        );
        Ok(())
    }

    #[test]
    fn pre_ns_prefix_overriden_by_default_namespace_midway_xml_path() -> TestOut {
        let xml = "<pim:pim xmlns:pim='example.com/ns'>
    <pim:inboxes>
        <pim:inbox>
            <messages xmlns='example.com/msg'>
                <message>
                    <date>202402101605</date>
                    <title>First text.</title>
                </message>
                <message>
                    <date>202402101606</date>
                    <title>Second text.</title>
                </message>
                <message>
                    <date>202402101608</date>
                    <title>Third text.</title>
                </message>
                <message>
                    <date>202402101609</date>
                    <title>Fourth text.</title>
                </message>
            </messages>
        </pim:inbox>
    </pim:inboxes>
</pim:pim>";
        let xml_doc = parse_raw_xml(xml)?;
        let texts: Vec<&str> = xml_doc
            .root()
            .pre_ns("example.com/ns")
            .first("inboxes")
            .first("inbox")
            .pre_ns("example.com/msg")
            .first("messages")
            .all("message")
            .first("title")
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(texts.len(), 4);
        assert_eq!(
            texts,
            vec!["First text.", "Second text.", "Third text.", "Fourth text."]
        );
        Ok(())
    }

    #[test]
    fn pre_ns_prefix_overriden_by_default_namespace_root_xml_path() -> TestOut {
        let xml = "<pim:pim xmlns:pim='example.com/ns' xmlns='example.com/msg'>
    <pim:inboxes>
        <pim:inbox>
            <messages>
                <message>
                    <date>202402101605</date>
                    <title>First text.</title>
                </message>
                <message>
                    <date>202402101606</date>
                    <title>Second text.</title>
                </message>
                <message>
                    <date>202402101608</date>
                    <title>Third text.</title>
                </message>
                <message>
                    <date>202402101609</date>
                    <title>Fourth text.</title>
                </message>
            </messages>
        </pim:inbox>
    </pim:inboxes>
</pim:pim>";
        let xml_doc = parse_raw_xml(xml)?;
        let texts: Vec<&str> = xml_doc
            .root()
            .pre_ns("example.com/ns")
            .first("inboxes")
            .first("inbox")
            .pre_ns("example.com/msg")
            .first("messages")
            .all("message")
            .first("title")
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(texts.len(), 4);
        assert_eq!(
            texts,
            vec!["First text.", "Second text.", "Third text.", "Fourth text."]
        );
        Ok(())
    }

    #[test]
    fn pre_ns_empty_xml_path_att_filter() -> TestOut {
        let xml = "<pim>
    <inboxes>
        <inbox>
            <messages>
                <message read=\"true\">
                    <date>202402101605</date>
                    <title>First text.</title>
                </message>
                <message read=\"true\">
                    <date>202402101606</date>
                    <title>Second text.</title>
                </message>
                <message read=\"false\">
                    <date>202402101608</date>
                    <title>Third text.</title>
                </message>
                <message read=\"false\">
                    <date>202402101609</date>
                    <title>Fourth text.</title>
                </message>
            </messages>
        </inbox>
    </inboxes>
</pim>";
        let xml_doc = parse_raw_xml(xml)?;
        let texts: Vec<&str> = xml_doc
            .root()
            .pre_ns("")
            .first("inboxes")
            .first("inbox")
            .first("messages")
            .all("message")
            .with_attribute("read", "false")
            .first("title")
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(texts.len(), 2);
        assert_eq!(texts, vec!["Third text.", "Fourth text."]);
        Ok(())
    }

    #[test]
    fn pre_ns_default_namespace_xml_path_att_filter() -> TestOut {
        let xml = "<pim xmlns=\"example.com/ns\">
    <inboxes>
        <inbox>
            <messages>
                <message read=\"true\">
                    <date>202402101605</date>
                    <title>First text.</title>
                </message>
                <message read=\"true\">
                    <date>202402101606</date>
                    <title>Second text.</title>
                </message>
                <message read=\"false\">
                    <date>202402101608</date>
                    <title>Third text.</title>
                </message>
                <message read=\"false\">
                    <date>202402101609</date>
                    <title>Fourth text.</title>
                </message>
            </messages>
        </inbox>
    </inboxes>
</pim>";
        let xml_doc = parse_raw_xml(xml)?;
        let texts: Vec<&str> = xml_doc
            .root()
            .pre_ns("example.com/ns")
            .first("inboxes")
            .first("inbox")
            .first("messages")
            .all("message")
            // Unprefixed attribute does not inherit default namespace, so specify empty.
            .pre_ns("")
            .with_attribute("read", "false")
            // Now switch back to looking within default namespace.
            .pre_ns("example.com/ns")
            .first("title")
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(texts.len(), 2);
        assert_eq!(texts, vec!["Third text.", "Fourth text."]);
        Ok(())
    }

    #[test]
    fn pre_ns_empty_namespace_overriden_by_prefix_xml_path_att_filter() -> TestOut {
        let xml = "<library xmlns:dc='example.com/dc'>
    <section name='science'>
        <books>
            <book dc:isbn='000728487X' dc:type='paperback'>
                <dc:title>Bad Science</dc:title>
                <dc:authors>
                    <dc:author>Ben Goldacre</dc:author>
                </dc:authors>
            </book>
        </books>
    </section>
    <section name='computing'>
        <books>
            <book dc:isbn='0135957052' dc:type='hardback'>
                <dc:title>The Pragmatic Programmer</dc:title>
                <dc:authors>
                    <dc:author>David Thomas</dc:author>
                    <dc:author>Andrew Hunt</dc:author>
                </dc:authors>
            </book>
            <book dc:isbn='9780132350884' dc:type='paperback'>
                <dc:title>The Pragmatic Programmer</dc:title>
                <dc:authors>
                    <dc:author>Robert Martin</dc:author>
                </dc:authors>
            </book>
            <book dc:isbn='0134685997' dc:type='paperback'>
                <dc:title>Effective Java: Third Edition</dc:title>
                <dc:authors>
                    <dc:author>Joshua Bloch</dc:author>
                </dc:authors>
            </book>
            <book dc:isbn='0596007647' dc:type='paperback'>
                <dc:title>XML in a Nutshell: Third Edition</dc:title>
                <dc:authors>
                    <dc:author>Elliotte Harold</dc:author>
                    <dc:author>W Scott Means</dc:author>
                </dc:authors>
            </book>
        </books>
    </section>
</library>";
        let xml_doc = parse_raw_xml(xml)?;
        let texts: Vec<&str> = xml_doc
            .root()
            .all("section")
            .with_attribute("name", "computing")
            .first("books")
            .all("book")
            .pre_ns("example.com/dc")
            .with_attribute("type", "paperback")
            .first("authors")
            .all("author")
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(texts.len(), 4);
        assert_eq!(
            texts,
            vec![
                "Robert Martin",
                "Joshua Bloch",
                "Elliotte Harold",
                "W Scott Means"
            ]
        );
        Ok(())
    }

    #[test]
    fn pre_ns_sidestepped_for_unprefixed_att_xml_path_att_filter() -> TestOut {
        let xml = "<library xmlns:dc='example.com/dc'>
    <section name='science'>
        <books>
            <book dc:isbn='000728487X' type='paperback'>
                <dc:title>Bad Science</dc:title>
                <dc:authors>
                    <dc:author>Ben Goldacre</dc:author>
                </dc:authors>
            </book>
        </books>
    </section>
    <section name='computing'>
        <books>
            <book dc:isbn='0135957052' type='hardback'>
                <dc:title>The Pragmatic Programmer</dc:title>
                <dc:authors>
                    <dc:author>David Thomas</dc:author>
                    <dc:author>Andrew Hunt</dc:author>
                </dc:authors>
            </book>
            <book dc:isbn='9780132350884' type='paperback'>
                <dc:title>The Pragmatic Programmer</dc:title>
                <dc:authors>
                    <dc:author>Robert Martin</dc:author>
                </dc:authors>
            </book>
            <book dc:isbn='0134685997' type='paperback'>
                <dc:title>Effective Java: Third Edition</dc:title>
                <dc:authors>
                    <dc:author>Joshua Bloch</dc:author>
                </dc:authors>
            </book>
            <book dc:isbn='0596007647' type='paperback'>
                <dc:title>XML in a Nutshell: Third Edition</dc:title>
                <dc:authors>
                    <dc:author>Elliotte Harold</dc:author>
                    <dc:author>W Scott Means</dc:author>
                </dc:authors>
            </book>
        </books>
    </section>
</library>";
        let xml_doc = parse_raw_xml(xml)?;
        let texts: Vec<&str> = xml_doc
            .root()
            .all("section")
            .with_attribute("name", "computing")
            .first("books")
            .all("book")
            .pre_ns("example.com/dc")
            // We could have just moved the pre_ns call down a line, but instead let's check
            // that we can still specify the empty namespace explicitly for an attribute.
            .with_attribute(("type", ""), "paperback")
            .first("authors")
            .all("author")
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(texts.len(), 4);
        assert_eq!(
            texts,
            vec![
                "Robert Martin",
                "Joshua Bloch",
                "Elliotte Harold",
                "W Scott Means"
            ]
        );
        Ok(())
    }

    #[test]
    fn pre_ns_default_preset_sidestepped_for_unprefixed_att_xml_path_att_filter() -> TestOut {
        let xml = "<library xmlns='example.com/default' xmlns:dc='example.com/dc'>
    <section name='science'>
        <books>
            <book dc:isbn='000728487X' type='paperback'>
                <dc:title>Bad Science</dc:title>
                <dc:authors>
                    <dc:author>Ben Goldacre</dc:author>
                </dc:authors>
            </book>
        </books>
    </section>
    <section name='computing'>
        <books>
            <book dc:isbn='0135957052' type='hardback'>
                <dc:title>The Pragmatic Programmer</dc:title>
                <dc:authors>
                    <dc:author>David Thomas</dc:author>
                    <dc:author>Andrew Hunt</dc:author>
                </dc:authors>
            </book>
            <book dc:isbn='9780132350884' type='paperback'>
                <dc:title>The Pragmatic Programmer</dc:title>
                <dc:authors>
                    <dc:author>Robert Martin</dc:author>
                </dc:authors>
            </book>
            <book dc:isbn='0134685997' type='paperback'>
                <dc:title>Effective Java: Third Edition</dc:title>
                <dc:authors>
                    <dc:author>Joshua Bloch</dc:author>
                </dc:authors>
            </book>
            <book dc:isbn='0596007647' type='paperback'>
                <dc:title>XML in a Nutshell: Third Edition</dc:title>
                <dc:authors>
                    <dc:author>Elliotte Harold</dc:author>
                    <dc:author>W Scott Means</dc:author>
                </dc:authors>
            </book>
        </books>
    </section>
</library>";
        let xml_doc = parse_raw_xml(xml)?;
        let texts: Vec<&str> = xml_doc
            .root()
            .pre_ns("example.com/default")
            .all("section")
            // This is probably the tidiest way to target unprefixed attributes which are in
            // amongst elements that do have a non-empty namespace.
            .with_attribute(("name", ""), "computing")
            .first("books")
            .all("book")
            // Calling pre_ns just for one attribute is a bit of overkill, but it should work.
            .pre_ns("")
            .with_attribute("type", "paperback")
            .pre_ns("example.com/dc")
            .first("authors")
            .all("author")
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(texts.len(), 4);
        assert_eq!(
            texts,
            vec![
                "Robert Martin",
                "Joshua Bloch",
                "Elliotte Harold",
                "W Scott Means"
            ]
        );
        Ok(())
    }

    #[test]
    fn reject_sgml_incompatible_sequence_root() {
        let xml = "<root>the ]]> seqence cannot appear *anywhere* in XML character data</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());

        let xml = "<root>the ]]&gt; seqence can appear *anywhere* in XML character data</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_ok());
        let xml = "<root>the ]> seqence can appear *anywhere* in XML character data</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_ok());
    }

    #[test]
    fn reject_sgml_incompatible_sequence_root_attribute() {
        let xml = "<root key=']]>'>harmless text</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());

        let xml = "<root key=']]&gt;'>harmless text</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_ok());
        let xml = "<root key=']>'>harmless text</root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_ok());
    }

    #[test]
    fn reject_sgml_incompatible_sequence_child() {
        let xml = "<root><child>the ]]> seqence cannot appear *anywhere* in XML character data</child></root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());

        let xml = "<root><child>the ]]&gt; seqence can appear *anywhere* in XML character data</child></root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_ok());
        let xml = "<root><child>the ]> seqence can appear *anywhere* in XML character data</child></root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_ok());
    }

    #[test]
    fn reject_sgml_incompatible_sequence_child_attribute() {
        let xml = "<root><child key=']]>'>harmless text</child></root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_err());

        let xml = "<root><child key=']]&gt;'>harmless text</child></root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_ok());
        let xml = "<root><child key=']>'>harmless text</child></root>";
        let outcome = parse_raw_xml(xml);
        assert!(outcome.is_ok());
    }

    #[test]
    fn apos_inside_root_attribute() -> TestOut {
        let xml = "<root id='&apos;single-quoted&apos;'>text</root>";
        let xml_doc = parse_raw_xml(xml)?;
        assert_eq!(xml_doc.root().att_req("id").unwrap(), "'single-quoted'");
        Ok(())
    }

    #[test]
    fn quot_inside_root_attribute() -> TestOut {
        let xml = "<root id=\"&quot;double-quoted&quot;\">text</root>";
        let xml_doc = parse_raw_xml(xml)?;
        assert_eq!(xml_doc.root().att_req("id").unwrap(), "\"double-quoted\"");
        Ok(())
    }

    #[test]
    fn empty_xml_lang() -> TestOut {
        let xml = "<root xml:lang=''>text</root>";
        let xml_doc = parse_raw_xml(xml)?;
        assert!(xml_doc.root().language_tag().is_none());
        Ok(())
    }

    #[test]
    fn empty_xml_lang_clears_language_tag() -> TestOut {
        let xml = "<root xml:lang='en'><child xml:lang=''>text</child></root>";
        let xml_doc = parse_raw_xml(xml)?;
        assert!(xml_doc.root().language_tag().is_some());
        let child = xml_doc.root().req("child").element().unwrap();
        assert!(child.language_tag().is_none());
        Ok(())
    }

    #[test]
    fn empty_xml_lang_remains_cleared() -> TestOut {
        let xml =
            "<root xml:lang='en'><child xml:lang=''><grandchild>text</grandchild></child></root>";
        let xml_doc = parse_raw_xml(xml)?;
        assert!(xml_doc.root().language_tag().is_some());
        let child = xml_doc.root().req("child").element().unwrap();
        assert!(child.language_tag().is_none());
        let grandchild = child.req("grandchild").element().unwrap();
        assert!(grandchild.language_tag().is_none());
        Ok(())
    }
}
