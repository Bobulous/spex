use std::io::{Error, Read};

use crate::{parsing::XmlParser, parsing::XmlSupportedCharacterEncoding, xml::XmlDocument};
use sipp::{
    buffer::ByteBuffer,
    decoder::{
        ByteStreamCharDecoder, Utf16BigEndianDecoder, Utf16LittleEndianDecoder, Utf8Decoder,
    },
};

/**
A utility for reading an XML source and returning an [`XmlDocument`] which represents it.

[`XmlDocument`]: ../xml/struct.XmlDocument.html
*/
pub struct XmlReader {}

impl XmlReader {
    /**
    Reads an XML source which is known to be encoded with UTF-8 or UTF-16, and returns an
    [`XmlDocument`] on success.

    This approach detects whether the encoding is UTF-8 or UTF-16, and skips any byte-order mark
    (BOM) automatically.

    # Errors

    This method will return an
    [`std::io::Error`](https://doc.rust-lang.org/std/io/struct.Error.html) if reading of the
    source or parsing of the XML fails (due to well-formedness errors for example).

    # Examples

    ```
    use std::io::{Error, Read};
    use crate::spex::{parsing::XmlReader, xml::XmlDocument};
    use std::fs::File;

    fn read_xml() -> Result<XmlDocument, Error> {
        let file = File::open("test_resources/xml_utf16LE_BOM.xml")?;
        XmlReader::parse_auto(file)
    }
    ```

    # Warning

    Rust will also allow you to pass a mutable (exclusive) reference to a `Read` type. However,
    because `XmlReader` will keep reading from the stream until there is nothing left, there is
    probably no advantage to passing a mutable reference, given that once this method has
    finished there will be nothing useful left to do with the reader. And if you attempt to
    read manually from the reader after a parsing error, be warned that an underlying buffer
    will have already taken a number of characters from the reader, so these characters will no
    longer be accessible to your application.

    [`XmlDocument`]: ../xml/struct.XmlDocument.html
    */
    pub fn parse_auto<R>(reader: R) -> Result<XmlDocument, Error>
    where
        R: Read,
    {
        let mut buffer = ByteBuffer::wrap(reader);
        let first_few_bytes = buffer.peek()?;
        let (encoding, bom_width) =
            XmlSupportedCharacterEncoding::determine_encoding(first_few_bytes)?;
        for _ in 0..bom_width {
            buffer.read_next()?;
        }
        match encoding {
            XmlSupportedCharacterEncoding::Utf8 => {
                let decoder = Utf8Decoder::wrap_buffer(buffer);
                let mut parser = XmlParser::wrap(decoder);
                parser.parse_encodings(Some(encoding.valid_encoding_names()))
            }
            XmlSupportedCharacterEncoding::Utf16BigEndian => {
                let decoder = Utf16BigEndianDecoder::wrap_buffer(buffer);
                let mut parser = XmlParser::wrap(decoder);
                parser.parse_encodings(Some(encoding.valid_encoding_names()))
            }
            XmlSupportedCharacterEncoding::Utf16LittleEndian => {
                let decoder = Utf16LittleEndianDecoder::wrap_buffer(buffer);
                let mut parser = XmlParser::wrap(decoder);
                parser.parse_encodings(Some(encoding.valid_encoding_names()))
            }
        }
    }

    /**
    Reads an XML source provided by the given `sipp::decoder::ByteStreamCharDecoder` type, and
    returns an [`XmlDocument`] on success.

    **IMPORTANT**: when using this approach, the client code must make its own check for a
    byte-order mark (BOM) and must also skip any BOM before calling this `parse_from` method.

    If the XML source is known to be encoded using UTF-8 or UTF-16, then use the [`parse_auto`]
    method instead. This `parse_from` method is only really needed when using a custom byte
    stream decoder.

    # Errors

    This method will return an
    [`std::io::Error`](https://doc.rust-lang.org/std/io/struct.Error.html)
    if reading of the source or parsing of the XML fails (due to well-formedness errors for
    example).

    # Examples

    Even though you should use [`parse_auto`] when the source XML is known to use UTF-16, this
    example shows how you would use `parse_from` by specifying the appropriate byte stream
    decoder, including the checking for and skipping past any byte-order mark.

    ```
    use std::io::{Error, Read};
    use crate::spex::{parsing::XmlReader, xml::XmlDocument};
    use sipp::{
        buffer::ByteBuffer,
        decoder::{ ByteStreamCharDecoder, Utf16LittleEndianDecoder, Utf8Decoder },
    };
    use std::fs::File;

    fn read_utf_16_xml() -> Result<XmlDocument, Error> {
        let file = File::open("test_resources/xml_utf16LE_BOM.xml")?;
        let mut buffer = ByteBuffer::wrap(file);
        // If specifying a decoder, you must detect and skip any BOM manually.
        let peek = buffer.peek()?;
        if peek[0..2] == [0xFF, 0xFE] {
            for _ in 0..2 {
                buffer.read_next()?;
            }
        }
        let decoder = Utf16LittleEndianDecoder::wrap_buffer(buffer);
        XmlReader::parse_from(decoder)
    }
    ```

    [`XmlDocument`]: ../xml/struct.XmlDocument.html
    [`parse_auto`]: #method.parse_auto
    */
    pub fn parse_from<D, R>(decoder: D) -> Result<XmlDocument, Error>
    where
        D: ByteStreamCharDecoder<R>,
        R: Read,
    {
        let mut parser = XmlParser::wrap(decoder);
        // If an encoding is found in an  XML declaration, it is ignored completely.
        parser.parse_encodings(None)
    }
}

#[cfg(test)]
mod tests {
    use std::fs::File;

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_auto_utf8_with_bom() -> Result<(), Error> {
        let file = File::open("test_resources/xml_utf8_BOM.xml")?;
        let doc = XmlReader::parse_auto(file)?;
        let root = doc.root();
        assert_eq!(root.elements().count(), 12);
        let confusing = root.r_child(("神奈川県", "example.com/Athína")).unwrap();
        assert_eq!(confusing.att_req("Αθήνα").unwrap(), "Confusing but valid");
        assert_eq!(
            confusing
                .att_req(("anglicized", "example.com/Athína"))
                .unwrap(),
            "Athens"
        );
        Ok(())
    }

    #[test]
    fn test_auto_utf16_le_with_bom() -> Result<(), Error> {
        let file = File::open("test_resources/xml_utf16LE_BOM.xml")?;
        XmlReader::parse_auto(file)?;
        Ok(())
    }

    #[test]
    fn test_auto_utf16_be_with_bom() -> Result<(), Error> {
        let file = File::open("test_resources/xml_utf16BE_BOM.xml")?;
        XmlReader::parse_auto(file)?;
        Ok(())
    }

    #[test]
    fn test_manual() -> Result<(), Error> {
        let file = File::open("test_resources/xml_utf16LE_BOM.xml")?;
        let mut buffer = ByteBuffer::wrap(file);
        // If specifying a decoder, you must detect and skip any BOM manually.
        let peek = buffer.peek()?;
        if peek[0..2] == [0xFF, 0xFE] {
            for _ in 0..2 {
                buffer.read_next()?;
            }
        }
        let decoder = Utf16LittleEndianDecoder::wrap_buffer(buffer);
        XmlReader::parse_from(decoder)?;
        Ok(())
    }
}
