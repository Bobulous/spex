use std::io::Error;

const BOM_UTF8: &[u8] = &[0xEF, 0xBB, 0xBF];
const BOM_UCS4_BE: &[u8] = &[0x00, 0x00, 0xFE, 0xFF];
const BOM_UCS4_LE: &[u8] = &[0xFF, 0xFE, 0x00, 0x00];
const BOM_UCS4_UOO2143: &[u8] = &[0x00, 0x00, 0xFF, 0xFE];
const BOM_UCS4_UOO3412: &[u8] = &[0xFE, 0xFF, 0x00, 0x00];

const XML_DEC_UTF8: &[u8] = &[0x3C, 0x3F, 0x78, 0x6D];
const XML_DEC_UCS4: &[u8] = &[0x00, 0x00, 0x00, 0x3C];
const XML_DEC_UTF16BE: &[u8] = &[0x00, 0x3C, 0x00, 0x3F];
const XML_DEC_UTF16LE: &[u8] = &[0x3C, 0x00, 0x3F, 0x00];
const XML_DEC_EBCDIC: &[u8] = &[0x4C, 0x6F, 0xA7, 0x94];

// NOTE: These values must be UPPERCASE otherwise text comparison will fail!
const VALID_FOR_UTF8: &[&str] = &["UTF-8", "US-ASCII"];
const VALID_FOR_UTF16BE: &[&str] = &["UTF-16", "UTF-16BE"];
const VALID_FOR_UTF16LE: &[&str] = &["UTF-16", "UTF-16LE"];

#[derive(Debug)]
pub(crate) enum XmlSupportedCharacterEncoding {
    Utf8,
    Utf16BigEndian,
    Utf16LittleEndian,
}

impl XmlSupportedCharacterEncoding {
    pub fn determine_encoding(
        first_few_bytes: &[u8],
    ) -> Result<(XmlSupportedCharacterEncoding, usize), Error> {
        if first_few_bytes.len() < 4 {
            // If the peek returns fewer than four bytes, then it means that the input XML is
            // tiny, and will have no room for a byte-order mark, nor for an XML declaration,
            // so just assume UTF-8 encoding.
            return Ok((XmlSupportedCharacterEncoding::Utf8, 0));
        }
        // println!(
        //     "Trying to determine encoding from first few bytes: {:?}",
        //     &first_few_bytes[0..4]
        // );
        if first_few_bytes[0..3] == *BOM_UTF8 {
            return Ok((XmlSupportedCharacterEncoding::Utf8, BOM_UTF8.len()));
        }
        match first_few_bytes {
            XML_DEC_UTF8 => Ok((XmlSupportedCharacterEncoding::Utf8, 0)),
            XML_DEC_UTF16BE => Ok((XmlSupportedCharacterEncoding::Utf16BigEndian, 0)),
            XML_DEC_UTF16LE => Ok((XmlSupportedCharacterEncoding::Utf16LittleEndian, 0)),
            BOM_UCS4_BE => Err(Error::new(
                std::io::ErrorKind::InvalidData,
                "UCS-4 BE detected, but not supported!",
            )),
            BOM_UCS4_LE => Err(Error::new(
                std::io::ErrorKind::InvalidData,
                "UCS-4 LE character encoding detected, but not supported!",
            )),
            BOM_UCS4_UOO2143 => Err(Error::new(
                std::io::ErrorKind::InvalidData,
                "UCS-4 (with unusual octet order 2143) character encoding detected, but not supported!",
            )),
            BOM_UCS4_UOO3412 => Err(Error::new(
                std::io::ErrorKind::InvalidData,
                "UCS-4 (with unusual octet order 3412) character encoding detected, but not supported!",
            )),
            XML_DEC_UCS4 => Err(Error::new(
                std::io::ErrorKind::InvalidData,
                "UCS-4 (or some other 32-bit character encoding) character encoding detected, but not supported!",
            )),
            XML_DEC_EBCDIC => Err(Error::new(
                std::io::ErrorKind::InvalidData,
                "EBCDIC character encoding detected, but not supported!",
            )),
            _ => {
                if first_few_bytes[0] == 0xFE && first_few_bytes[1] == 0xFF {
                    // UTF-16BE BOM (not followed by 0x00 0x00 which would instead indicate UCS-4).
                    Ok((XmlSupportedCharacterEncoding::Utf16BigEndian, 2))
                } else if first_few_bytes[0] == 0xFF && first_few_bytes[1] == 0xFE {
                    // UTF-16LE BOM (not followed by 0x00 0x00 which would instead indicate UCS-4).
                    Ok((XmlSupportedCharacterEncoding::Utf16LittleEndian, 2))
                } else {
                    // Assume anything else indicates UTF-8 with no byte-order mark, and no XML declaration.
                    // println!("Failed to find any defined byte pattern. Will assume UTF-8 without BOM or XML declaration!");
                    Ok((XmlSupportedCharacterEncoding::Utf8, 0))
                }
            }
        }
    }

    pub fn valid_encoding_names(&self) -> &[&str] {
        match self {
            Self::Utf8 => VALID_FOR_UTF8,
            Self::Utf16BigEndian => VALID_FOR_UTF16BE,
            Self::Utf16LittleEndian => VALID_FOR_UTF16LE,
        }
    }
}
