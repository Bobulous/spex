use std::{
    error::Error,
    fmt::{Display, Formatter, Result},
};

/**
An enumeration of error scenarios which can occur while extracting or traversing XML.

Note that this type does not contain error scenarios which occur during reading/parsing of the
XML into an [`XmlDocument`]. That type will always be
[`std::io::Error`](https://doc.rust-lang.org/std/io/struct.Error.html).

[`XmlDocument`]: ../xml/struct.XmlDocument.html
*/
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum XmlError {
    /**
    The `text()` method was called while targeting an element which has non-simple content (it
    contains child elements or processing instructions).
    */
    NotPureText,
    /**
    An attribute considered required/mandatory was not actually found within the element where
    it was expected. The payload is an immutable `str` which describes the missing attribute.
    */
    AttributeAbsent(Box<str>),
    /**
    An element considered required/mandatory was not actually found within the parent element
    where it was expected. The payload is an immutable `str` which describes the missing
    element.
    */
    ElementAbsent(Box<str>),
    /**
    The text content of an element or attribute did not fit the expected format or type, for
    example a value expected to be numeric is found to be alphabetic. The payload is an
    immutable `str` which describes the reason the text is considered invalid/unexpected.
    */
    InvalidText(Box<str>),
    /**
    An attempt was made to create an [`ExtendedLanguageRange`] with an invalid language range.
    The payload is an immutable `str` which describes the problem with the given range.

    [`ExtendedLanguageRange`]: ../extraction/struct.ExtendedLanguageRange.html
    */
    InvalidLanguageRange(Box<str>),
}
impl Display for XmlError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match self {
            XmlError::AttributeAbsent(m) => {
                write!(f, "Required XML attribute not found: {:?}", m)
            }
            XmlError::ElementAbsent(s) => {
                write!(f, "Required XML element not found: {:?}", s)
            }
            XmlError::NotPureText => {
                write!(
                    f,
                    "Element unexpectedly contains mix of text and child elements."
                )
            }
            XmlError::InvalidText(t) => {
                write!(
                    f,
                    "Element/attribute contains invalid text content: {:?}",
                    t
                )
            }
            XmlError::InvalidLanguageRange(t) => {
                write!(f, "Extended language range is invalid: {:?}", t)
            }
        }
    }
}

impl Error for XmlError {}
