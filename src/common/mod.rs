/*!
Structures common to multiple other modules within this package.
*/
mod xml_error;

pub use self::xml_error::XmlError;
