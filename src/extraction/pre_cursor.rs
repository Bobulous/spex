use crate::{
    common::XmlError, extraction::NameRef, extraction::OptionalCursor, extraction::PresetNamespace,
    extraction::RequiredCursor, extraction::XmlPath, xml::Element,
};

pub struct PreCursor<'e> {
    points_at: &'e Element,
    default_namespace: Option<PresetNamespace>,
}

impl<'e> PreCursor<'e> {
    pub(crate) fn new(
        points_at: &'e Element,
        default_namespace: Option<PresetNamespace>,
    ) -> PreCursor<'e> {
        PreCursor {
            points_at,
            default_namespace,
        }
    }

    // This method is only used for unit testing.
    #[cfg(test)]
    pub(crate) fn default_namespace(&self) -> Option<PresetNamespace> {
        self.default_namespace
    }

    /**
    Converts this cursor into an `OptionalCursor`, then points it at the **first** child element
    with the given name if such an element exists, otherwise marks the cursor as having an
    "acceptably absent" state.
    */
    pub fn opt<'b, N>(self, full_name: N) -> OptionalCursor<'e>
    where
        N: Into<NameRef<'b>>,
    {
        let target_name = full_name.into();
        match self
            .points_at
            .o_child((target_name, self.default_namespace))
        {
            Some(c) => OptionalCursor::new(c, self.default_namespace),
            None => OptionalCursor::absent(),
        }
    }

    /**
    Converts this cursor into a `RequiredCursor`, then points it at the **first** child element
    with the given name if such an element exists, otherwise marks the cursor as having an
    "invalid/error" state.
    */
    pub fn req<'b, N>(self, full_name: N) -> RequiredCursor<'e>
    where
        N: Into<NameRef<'b>>,
    {
        match self
            .points_at
            .r_child((full_name.into(), self.default_namespace))
        {
            Ok(c) => RequiredCursor::new(c, self.default_namespace),
            Err(x) => {
                let message = match x {
                    XmlError::ElementAbsent(f) => f,
                    _ => "Failed to find required child element."
                        .to_string()
                        .into_boxed_str(),
                };
                RequiredCursor::invalid(message)
            }
        }
    }

    /**
    Converts this cursor into an `XmlPath` and instructs it to consider all of the child
    elements (of the element which created this cursor) with the given name.
    */
    pub fn all<'n, N>(self, name: N) -> XmlPath<'e, 'n>
    where
        'e: 'n,
        N: Into<NameRef<'n>>,
    {
        let mut path = XmlPath::new(self.points_at, self.default_namespace);
        path.all(name);
        path
    }

    /**
    Converts this cursor into an `XmlPath` and instructs it to consider the first child
    element (of the element which created this cursor) with the given name.
    */
    pub fn first<'n, N>(self, name: N) -> XmlPath<'e, 'n>
    where
        'e: 'n,
        N: Into<NameRef<'n>>,
    {
        let mut path = XmlPath::new(self.points_at, self.default_namespace);
        path.first(name);
        path
    }

    /**
    Converts this cursor into an `XmlPath` and instructs it to consider the last child
    element (of the element which created this cursor) with the given name.
    */
    pub fn last<'n, N>(self, name: N) -> XmlPath<'e, 'n>
    where
        'e: 'n,
        N: Into<NameRef<'n>>,
    {
        let mut path = XmlPath::new(self.points_at, self.default_namespace);
        path.last(name);
        path
    }

    /**
    Converts this cursor into an `XmlPath` and instructs it to consider the nth (where 0 refers
    to the first) child element (of the element which created this cursor) with the given name.
    */
    pub fn nth<'n, N>(self, name: N, n: usize) -> XmlPath<'e, 'n>
    where
        'e: 'n,
        N: Into<NameRef<'n>>,
    {
        let mut path = XmlPath::new(self.points_at, self.default_namespace);
        path.nth(name, n);
        path
    }
}

#[cfg(test)]
mod tests {
    use std::io::Error;

    use crate::{xml::ElementBuilder, xml::Name};

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    type TestOut = Result<(), Error>;

    #[test]
    fn new_no_default_namespace() -> TestOut {
        let builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(builder);
        let pre_cursor = PreCursor::new(&root, None);
        assert_eq!(pre_cursor.points_at.name().local_part(), "root");
        assert_eq!(pre_cursor.points_at.name().namespace(), None);
        assert!(pre_cursor.default_namespace.is_none());
        assert!(pre_cursor.default_namespace().is_none());
        Ok(())
    }

    #[test]
    fn new_with_default_namespace() -> TestOut {
        let builder = ElementBuilder::new(Name::new("ServiceRS", ""));
        let root = Element::new(builder);
        let pre_cursor = PreCursor::new(&root, Some(PresetNamespace::new("some-namespace")));
        assert_eq!(pre_cursor.points_at.name().local_part(), "ServiceRS");
        assert_eq!(pre_cursor.points_at.name().namespace(), None);
        assert_eq!(
            pre_cursor.default_namespace.unwrap().value(),
            "some-namespace"
        );
        assert_eq!(
            pre_cursor.default_namespace().unwrap().value(),
            "some-namespace"
        );
        Ok(())
    }

    #[test]
    fn opt_no_default_namespace_no_child() -> TestOut {
        let builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(builder);
        let pre_cursor = PreCursor::new(&root, None);
        let opt_cursor = pre_cursor.opt("child");
        assert!(opt_cursor.default_namespace().is_none());
        assert!(opt_cursor.element().is_none());
        Ok(())
    }

    #[test]
    fn opt_with_default_namespace_no_child() -> TestOut {
        let builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(builder);
        let pre_cursor = PreCursor::new(&root, Some(PresetNamespace::new("some-namespace")));
        assert_eq!(
            pre_cursor.default_namespace().unwrap().value(),
            "some-namespace"
        );
        let opt_cursor = pre_cursor.opt("child");
        // Note: there's no point in checking whether the opt_cursor contains a default
        // namespace, because as soon as opt tries to target a child element which does not
        // exist, the cursor can no longer match any further children and so it stops keeping
        // track of the default namespace.
        assert!(opt_cursor.element().is_none());
        Ok(())
    }

    #[test]
    fn opt_no_default_namespace_child_exists_in_namespace() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let child_builder = ElementBuilder::new(Name::new("child", "sub_namespace"));
        root_builder.add_child_element(Element::new(child_builder));
        let root = Element::new(root_builder);
        let pre_cursor = PreCursor::new(&root, None);
        let opt_cursor = pre_cursor.opt(("child", "sub_namespace"));
        assert!(opt_cursor.default_namespace().is_none());
        let child = opt_cursor.element().unwrap();
        assert_eq!(child.name().local_part(), "child");
        assert_eq!(child.name().namespace().unwrap(), "sub_namespace");
        Ok(())
    }

    #[test]
    fn opt_no_default_namespace_child_exists_without_namespace() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let child_builder = ElementBuilder::new(Name::new("child", ""));
        root_builder.add_child_element(Element::new(child_builder));
        let root = Element::new(root_builder);
        let pre_cursor = PreCursor::new(&root, None);
        let opt_cursor = pre_cursor.opt("child");
        assert!(opt_cursor.default_namespace().is_none());
        let child = opt_cursor.element().unwrap();
        assert_eq!(child.name().local_part(), "child");
        assert!(child.name().namespace().is_none());
        Ok(())
    }

    #[test]
    fn opt_with_default_namespace_child_exists_in_namespace() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let child_builder = ElementBuilder::new(Name::new("child", "sub_namespace"));
        root_builder.add_child_element(Element::new(child_builder));
        let root = Element::new(root_builder);
        let pre_cursor = PreCursor::new(&root, Some(PresetNamespace::new("sub_namespace")));
        let opt_cursor = pre_cursor.opt("child");
        assert_eq!(
            opt_cursor.default_namespace().unwrap().value(),
            "sub_namespace"
        );
        let child = opt_cursor.element().unwrap();
        assert_eq!(child.name().local_part(), "child");
        assert_eq!(child.name().namespace().unwrap(), "sub_namespace");
        Ok(())
    }

    #[test]
    fn opt_with_default_namespace_child_exists_without_namespace() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let child_builder = ElementBuilder::new(Name::new("child", ""));
        root_builder.add_child_element(Element::new(child_builder));
        let root = Element::new(root_builder);
        let pre_cursor = PreCursor::new(&root, Some(PresetNamespace::new("sub_namespace")));
        let opt_cursor = pre_cursor.opt(("child", ""));
        assert_eq!(
            opt_cursor.default_namespace().unwrap().value(),
            "sub_namespace"
        );
        let child = opt_cursor.element().unwrap();
        assert_eq!(child.name().local_part(), "child");
        assert!(child.name().namespace().is_none());
        Ok(())
    }

    #[test]
    fn req_no_default_namespace_no_child() -> TestOut {
        let builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(builder);
        let pre_cursor = PreCursor::new(&root, None);
        let req_cursor = pre_cursor.req("child");
        assert!(req_cursor.default_namespace().is_none());
        assert!(req_cursor.element().is_err());
        Ok(())
    }

    #[test]
    fn req_with_default_namespace_no_child() -> TestOut {
        let builder = ElementBuilder::new(Name::new("root", ""));
        let root = Element::new(builder);
        let pre_cursor = PreCursor::new(&root, Some(PresetNamespace::new("some-namespace")));
        assert_eq!(
            pre_cursor.default_namespace().unwrap().value(),
            "some-namespace"
        );
        let req_cursor = pre_cursor.req("child");
        // Note: there's no point in checking whether the req_cursor contains a default
        // namespace, because as soon as req tries to target a child element which does not
        // exist, the cursor can no longer match any further children and so it stops keeping
        // track of the default namespace.
        assert!(req_cursor.element().is_err());
        Ok(())
    }

    #[test]
    fn req_no_default_namespace_child_exists_in_namespace() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let child_builder = ElementBuilder::new(Name::new("child", "sub_namespace"));
        root_builder.add_child_element(Element::new(child_builder));
        let root = Element::new(root_builder);
        let pre_cursor = PreCursor::new(&root, None);
        let req_cursor = pre_cursor.req(("child", "sub_namespace"));
        assert!(req_cursor.default_namespace().is_none());
        let child = req_cursor.element().unwrap();
        assert_eq!(child.name().local_part(), "child");
        assert_eq!(child.name().namespace().unwrap(), "sub_namespace");
        Ok(())
    }

    #[test]
    fn req_no_default_namespace_child_exists_without_namespace() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let child_builder = ElementBuilder::new(Name::new("child", ""));
        root_builder.add_child_element(Element::new(child_builder));
        let root = Element::new(root_builder);
        let pre_cursor = PreCursor::new(&root, None);
        let req_cursor = pre_cursor.req("child");
        assert!(req_cursor.default_namespace().is_none());
        let child = req_cursor.element().unwrap();
        assert_eq!(child.name().local_part(), "child");
        assert!(child.name().namespace().is_none());
        Ok(())
    }

    #[test]
    fn req_with_default_namespace_child_exists_in_namespace() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let child_builder = ElementBuilder::new(Name::new("child", "sub_namespace"));
        root_builder.add_child_element(Element::new(child_builder));
        let root = Element::new(root_builder);
        let pre_cursor = PreCursor::new(&root, Some(PresetNamespace::new("sub_namespace")));
        let req_cursor = pre_cursor.req("child");
        assert_eq!(
            req_cursor.default_namespace().unwrap().value(),
            "sub_namespace"
        );
        let child = req_cursor.element().unwrap();
        assert_eq!(child.name().local_part(), "child");
        assert_eq!(child.name().namespace().unwrap(), "sub_namespace");
        Ok(())
    }

    #[test]
    fn req_with_default_namespace_child_exists_without_namespace() -> TestOut {
        let mut root_builder = ElementBuilder::new(Name::new("root", ""));
        let child_builder = ElementBuilder::new(Name::new("child", ""));
        root_builder.add_child_element(Element::new(child_builder));
        let root = Element::new(root_builder);
        let pre_cursor = PreCursor::new(&root, Some(PresetNamespace::new("sub_namespace")));
        let req_cursor = pre_cursor.req(("child", ""));
        assert_eq!(
            req_cursor.default_namespace().unwrap().value(),
            "sub_namespace"
        );
        let child = req_cursor.element().unwrap();
        assert_eq!(child.name().local_part(), "child");
        assert!(child.name().namespace().is_none());
        Ok(())
    }
}
