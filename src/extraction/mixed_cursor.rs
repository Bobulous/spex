use crate::{
    common::XmlError, extraction::NameRef, extraction::OptionalCursor, extraction::PresetNamespace,
    extraction::RequiredCursor, xml::Element,
};

pub struct MixedCursor<'a> {
    default_namespace: Option<PresetNamespace>,
    status: Status<'a>,
}

enum Status<'a> {
    Found(&'a Element),
    Absent,
    Invalid(Box<str>),
}

impl<'a> MixedCursor<'a> {
    pub(crate) fn from_req(r: RequiredCursor<'a>) -> Self {
        let default_namespace = r.default_namespace();
        let status = match r.status() {
            crate::extraction::required_cursor::Status::Found(e) => Status::Found(e),
            crate::extraction::required_cursor::Status::Invalid(s) => Status::Invalid(s),
        };
        MixedCursor {
            default_namespace,
            status,
        }
    }

    pub(crate) fn from_opt(r: OptionalCursor<'a>) -> Self {
        let default_namespace = r.default_namespace();
        let status = match r.status() {
            crate::extraction::optional_cursor::Status::Found(e) => Status::Found(e),
            crate::extraction::optional_cursor::Status::Absent => Status::Absent,
        };
        MixedCursor {
            default_namespace,
            status,
        }
    }

    /**
    Sets the preset namespace name which will be used from this point forward in this method
    chain by calls to methods such as `req`, `opt`, `first`, `all`, etc, so that they know which
    namespace name should be presumed if no explicit namespace name is provided when calling
    those methods as part of this chain.
    */
    pub fn pre_ns(mut self, default_namespace: &'static str) -> Self {
        if let Status::Found(_) = self.status {
            let default_namespace = if default_namespace.is_empty() {
                None
            } else {
                Some(PresetNamespace::new(default_namespace))
            };
            self.default_namespace = default_namespace;
        }
        self
    }

    /**
    Points this cursor at the **first** child element (of the element which the cursor is
    currently pointing at) with the given name, if such an element exists. Otherwise marks the
    cursor as having an "acceptably absent" state.

    Note that if the state of the cursor was already "absent" or "invalid" before calling this
    method, then the state will remain the way it was, whether or not the named child element
    exists.
    */
    pub fn opt<'b, N>(mut self, full_name: N) -> Self
    where
        N: Into<NameRef<'b>>,
    {
        if let Status::Found(e) = self.status {
            match e.o_child((full_name.into(), self.default_namespace)) {
                Some(c) => self.status = Status::Found(c),
                None => self.status = Status::Absent,
            }
        }
        self
    }

    /**
    Points this cursor at the **first** child element (of the element which the cursor is
    currently pointing at) with the given name, if such an element exists. Otherwise marks the
    cursor as having an "invalid/error" state.

    Note that if the state of the cursor was already "absent" or "invalid" before calling this
    method, then the state will remain the way it was, whether or not the named child element
    exists.
    */
    pub fn req<'b, N>(mut self, full_name: N) -> Self
    where
        N: Into<NameRef<'b>>,
    {
        if let Status::Found(e) = self.status {
            match e.r_child((full_name.into(), self.default_namespace)) {
                Ok(c) => self.status = Status::Found(c),
                Err(x) => {
                    let message = match x {
                        XmlError::ElementAbsent(f) => f,
                        _ => "Failed to find required child element."
                            .to_string()
                            .into_boxed_str(),
                    };
                    self.status = Status::Invalid(message);
                }
            }
        }
        self
    }

    /**
    Returns a reference to the element which this cursor is ponting at if it exists, or returns
    an `XmlError` if the cursor state is "invalid" (meaning that the cursor was pointing to an
    element which exists and then `req` was called with a non-existent child element), or
    returns `None` if the cursor state is "absent" (meaning that the cursor was pointing to an
    element which exists and then `opt` was called with a non-existent child element).

    # Errors

    This method will return an `XmlError` if anywhere along the cursor method chain a valid
    cursor was asked to point to a required child element which was not found.
    */
    pub fn element(self) -> Result<Option<&'a Element>, XmlError> {
        match self.status {
            Status::Found(e) => Ok(Some(e)),
            Status::Absent => Ok(None),
            Status::Invalid(m) => Err(XmlError::ElementAbsent(m)),
        }
    }

    /**
    Returns the text content of the **optional** attribute (within the element at which this
    cursor is pointing) with the given name, or returns an `XmlError` if the cursor state is
    "invalid" (meaning that the cursor was pointing to an element which exists and then `req`
    was called with a non-existent child element), or returns `None` if the cursor state is
    "absent" (meaning that the cursor was pointing to an element which exists and then `opt` was
    called with a non-existent child element) or if the attribute does not exist.

    # Errors

    This method will return an `XmlError` if anywhere along the cursor method chain a valid
    cursor was asked to point to a required child element which was not found.
    */
    pub fn att_opt<'b, N>(self, att_name: N) -> Result<Option<&'a str>, XmlError>
    where
        N: Into<NameRef<'b>>,
    {
        match self.status {
            Status::Found(e) => Ok(e.att_opt((att_name.into(), self.default_namespace))),
            Status::Absent => Ok(None),
            Status::Invalid(m) => Err(XmlError::ElementAbsent(m)),
        }
    }

    /**
    Returns the text content of the **required** attribute (within the element at which this
    cursor is pointing) with the given name, or returns `None` if the cursor state is "absent"
    (meaning that the cursor was pointing to an element which exists and then `opt` was called
    with a non-existent child element), or returns an `XmlError` if the cursor state is
    "invalid" (meaning that the cursor was pointing to an element which exists and then `req`
    was called with a non-existent child element) or if the attribute does not exist.

    # Errors

    This method will return an `XmlError` if anywhere along the cursor method chain a valid
    cursor was asked to point to a required child element which was not found, or if the cursor
    target element exists but the required attribute is not found.
    */
    pub fn att_req<'b, N>(self, att_name: N) -> Result<Option<&'a str>, XmlError>
    where
        N: Into<NameRef<'b>>,
    {
        match self.status {
            Status::Found(e) => Ok(Some(e.att_req((att_name.into(), self.default_namespace))?)),
            Status::Absent => Ok(None),
            Status::Invalid(m) => Err(XmlError::ElementAbsent(m)),
        }
    }

    /**
    Returns the text content of the element (at which this cursor is pointing), or returns an
    `XmlError` if the cursor status is "invalid" (meaning that the cursor was pointing to an
    element which exists and then `req` was called with a non-existent child element) or the
    element does not contain simple content, or returns `None` if the cursor state is "absent"
    (meaning that the cursor was pointing to an element which exists and then `opt` was called
    with a non-existent child element).

    # Errors

    This method will return an `XmlError` if anywhere along the cursor method chain a valid
    cursor was asked to point to a required child element which was not found, or if the cursor
    target element exists but contains non-simple content (child elements or processing
    instructions).
    */
    pub fn text(self) -> Result<Option<&'a str>, XmlError> {
        match self.status {
            Status::Found(e) => match e.text() {
                Ok(t) => Ok(Some(t)),
                Err(m) => Err(m),
            },
            Status::Absent => Ok(None),
            Status::Invalid(m) => Err(XmlError::ElementAbsent(m)),
        }
    }
}

#[cfg(test)]
mod tests {

    use std::io::Error;

    use crate::parsing::XmlReader;
    use crate::xml::ElementBuilder;
    use crate::xml::Name;
    use crate::xml::XmlDocument;

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    fn parse_raw_xml(raw_xml: &str) -> Result<XmlDocument, Error> {
        let bytes = raw_xml.as_bytes();
        XmlReader::parse_auto(bytes)
    }

    #[test]
    fn empty_root() -> Result<(), Error> {
        let root = Element::new(ElementBuilder::new(Name::new("root", "")));
        let cursor = root.req("nonexistent");
        assert!(cursor.element().is_err());
        Ok(())
    }

    #[test]
    fn test_required_then_optional() -> Result<(), Error> {
        let xml = "<root mode='demo'>
            <child id='1'>
                <name type='first'>Barry</name>
            </child>
            <child id='2'>
                <name type='first'>Philomena</name>
            </child>
        </root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let first_child_name = root.req("child").opt("name").element().unwrap();
        assert_eq!(first_child_name.unwrap().text().unwrap(), "Barry");

        Ok(())
    }

    #[test]
    fn root_with_grandchild() -> Result<(), Error> {
        let xml = "
<root>
    <child>
        <grandchild>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let grandchild = root.opt("child").req("grandchild").element();
        assert!(grandchild.is_ok());
        let grandchild = grandchild.unwrap().unwrap();
        assert_eq!(grandchild.name().local_part(), "grandchild");
        assert_eq!(grandchild.name().namespace(), None);
        assert_eq!(
            root.req("child").opt("grandchild").text().unwrap(),
            Some("text")
        );

        Ok(())
    }

    #[test]
    fn namespaces_explicit() -> Result<(), Error> {
        let xml = "
<root xmlns='example.com/ns'>
    <child>
        <grandchild>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let grandchild = root
            .opt(("child", "example.com/ns"))
            .req(("grandchild", "example.com/ns"))
            .element();
        assert!(grandchild.is_ok());
        let grandchild = grandchild.unwrap().unwrap();
        assert_eq!(grandchild.name().local_part(), "grandchild");
        assert_eq!(grandchild.name().namespace(), Some("example.com/ns"));

        // Without specifying the namespace, nothing should be found.
        assert!(root.req("child").opt("grandchild").element().is_err());

        Ok(())
    }

    #[test]
    fn namespaces_using_preset() -> Result<(), Error> {
        let xml = "
<a xmlns='example.com/ns1'>
    <b>
        <c xmlns='example.com/ns2'>
            <d>
                <e xmlns='example.com/ns3'>
                    <f>text</f>
                </e>
            </d>
        </c>
    </b>
</a>
";
        let doc = parse_raw_xml(xml)?;
        let alpha = doc.root();
        let foxtrot = alpha
            .pre_ns("example.com/ns1")
            .req("b")
            .pre_ns("example.com/ns2")
            .opt("c")
            .req("d")
            .pre_ns("example.com/ns3")
            .opt("e")
            .req("f")
            .element();
        assert!(foxtrot.is_ok());
        let foxtrot = foxtrot.unwrap().unwrap();
        assert_eq!(foxtrot.text().unwrap(), "text");

        let foxtrot_text = alpha
            .pre_ns("example.com/ns1")
            .opt("b")
            .pre_ns("example.com/ns2")
            .req("c")
            .opt("d")
            .pre_ns("example.com/ns3")
            .req("e")
            .opt("f")
            .text()
            .unwrap()
            .unwrap();
        assert_eq!(foxtrot_text, "text");
        Ok(())
    }

    #[test]
    fn att_opt() -> Result<(), Error> {
        let xml = "
<root>
    <child id='1'>
        <grandchild id='2'>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let grandchild_id = root.opt("child").req("grandchild").att_opt("id");
        assert!(grandchild_id.is_ok());
        assert_eq!(grandchild_id.unwrap(), Some("2"));

        let no_such_att = root.req("child").opt("grandchild").att_opt("mode");
        assert!(no_such_att.unwrap().is_none());

        let no_grandchild_id = root.opt("child").req("disinherited").att_opt("id");
        assert!(no_grandchild_id.is_err());
        Ok(())
    }

    #[test]
    fn att_req() -> Result<(), Error> {
        let xml = "
<root>
    <child id='1'>
        <grandchild id='2'>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let grandchild_id = root.opt("child").req("grandchild").att_req("id");
        assert!(grandchild_id.is_ok());
        assert_eq!(grandchild_id.unwrap(), Some("2"));

        let no_such_att = root.req("child").opt("grandchild").att_req("mode");
        assert!(no_such_att.is_err());

        let no_grandchild_id = root.opt("child").req("disinherited").att_req("id");
        assert!(no_grandchild_id.is_err());
        Ok(())
    }

    #[test]
    fn att_opt_namespaces() -> Result<(), Error> {
        let xml = "
<root xmlns:p='example.com/p'>
    <child p:id='1'>
        <grandchild p:id='2'>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let grandchild_id = root
            .opt("child")
            .req("grandchild")
            .att_opt(("id", "example.com/p"));
        assert!(grandchild_id.is_ok());
        assert_eq!(grandchild_id.unwrap(), Some("2"));

        let namespace_missing = root.req("child").opt("grandchild").att_opt("id");
        assert!(namespace_missing.is_ok());
        assert!(namespace_missing.unwrap().is_none());
        Ok(())
    }

    #[test]
    fn att_req_namespaces() -> Result<(), Error> {
        let xml = "
<root xmlns:p='example.com/p'>
    <child p:id='1'>
        <grandchild p:id='2'>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let grandchild_id = root
            .req("child")
            .opt("grandchild")
            .att_req(("id", "example.com/p"));
        assert!(grandchild_id.is_ok());
        assert_eq!(grandchild_id.unwrap(), Some("2"));

        let namespace_missing = root.opt("child").req("grandchild").att_req("id");
        assert!(namespace_missing.is_err());
        Ok(())
    }
}
