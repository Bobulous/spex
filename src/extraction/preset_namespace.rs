#[derive(Clone, Copy)]
pub struct PresetNamespace {
    value: &'static str,
}

impl PresetNamespace {
    pub(crate) fn new(default_namespace: &'static str) -> Self {
        PresetNamespace {
            value: default_namespace,
        }
    }

    pub fn value(&self) -> &'static str {
        self.value
    }
}
