/*!
Structures related to the traversal and extraction of data from an existing `XmlDocument` or
`Element` tree.
*/
mod extended_language_range;
mod mixed_cursor;
mod name_ref;
mod optional_cursor;
mod pre_cursor;
mod preset_namespace;
mod required_cursor;
mod xml_path;

pub use self::extended_language_range::ExtendedLanguageRange;
pub(crate) use self::mixed_cursor::MixedCursor;
pub(crate) use self::name_ref::NameRef;
pub(crate) use self::optional_cursor::OptionalCursor;
pub(crate) use self::pre_cursor::PreCursor;
pub(crate) use self::preset_namespace::PresetNamespace;
pub(crate) use self::required_cursor::RequiredCursor;
pub(crate) use self::xml_path::XmlPath;
