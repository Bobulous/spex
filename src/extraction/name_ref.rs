use crate::extraction::PresetNamespace;

#[derive(Debug, Clone, Copy)]
pub struct NameRef<'a> {
    local_part: &'a str,
    namespace: Option<&'a str>,
}

impl<'a> NameRef<'a> {
    #[cfg(test)]
    pub(crate) fn new(local_part: &'a str, namespace: Option<&'a str>) -> NameRef<'a> {
        NameRef {
            local_part,
            namespace,
        }
    }

    // Consumes this NameRef and returns a new NameRef with the same local_part but None as the
    // namespace.
    pub(crate) fn without_namespace(self) -> NameRef<'a> {
        NameRef {
            local_part: self.local_part,
            namespace: None,
        }
    }

    /**
    Returns the local part of the name which this `NameRef` is referring to.
    */
    pub fn local_part(&self) -> &str {
        self.local_part
    }

    /**
    Returns the namespace name of the name which this `NameRef` is referring to, or returns
    `None` if the target name is not within a namespace (namespace name is empty).
    */
    pub fn namespace(&self) -> Option<&str> {
        self.namespace
    }
}

// This allows developers to use a single string slice to create a NameRef without a namespace.
impl<'a> From<&'a str> for NameRef<'a> {
    fn from(value: &'a str) -> Self {
        NameRef {
            local_part: value,
            namespace: None,
        }
    }
}

// This implementation allows developers to use a tuple to specify local-part and namespace together.
impl<'a> From<(&'a str, &'a str)> for NameRef<'a> {
    fn from((local_part, namespace): (&'a str, &'a str)) -> Self {
        NameRef {
            local_part,
            namespace: Some(namespace),
        }
    }
}

// TODO: Work out whether this is really desirable (basically clones every reference).
impl<'a> From<&'a NameRef<'a>> for NameRef<'a> {
    fn from(value: &'a NameRef<'a>) -> Self {
        NameRef {
            local_part: value.local_part,
            namespace: value.namespace,
        }
    }
}

// This is needed to help fill in the namespace name when a preset namespace has been set within
// a method chain.
impl<'a> From<(NameRef<'a>, Option<PresetNamespace>)> for NameRef<'a> {
    fn from((name_ref, def_ns): (NameRef<'a>, Option<PresetNamespace>)) -> Self {
        match name_ref.namespace {
            Some(ns) => NameRef {
                local_part: name_ref.local_part,
                namespace: Some(ns),
            },
            None => NameRef {
                local_part: name_ref.local_part,
                namespace: match def_ns {
                    Some(d) => Some(d.value()),
                    None => None,
                },
            },
        }
    }
}
