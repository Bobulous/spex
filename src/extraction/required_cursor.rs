use crate::{
    common::XmlError, extraction::MixedCursor, extraction::NameRef, extraction::PresetNamespace,
    xml::Element,
};

pub struct RequiredCursor<'a> {
    default_namespace: Option<PresetNamespace>,
    status: Status<'a>,
}

pub(crate) enum Status<'a> {
    Found(&'a Element),
    Invalid(Box<str>),
}

impl<'a> RequiredCursor<'a> {
    pub(crate) fn new(e: &'a Element, default_namespace: Option<PresetNamespace>) -> Self {
        Self {
            default_namespace,
            status: Status::Found(e),
        }
    }

    pub(crate) fn status(self) -> Status<'a> {
        self.status
    }

    pub(crate) fn invalid<S>(m: S) -> Self
    where
        S: Into<Box<str>>,
    {
        Self {
            default_namespace: None,
            status: Status::Invalid(m.into()),
        }
    }

    pub(crate) fn default_namespace(&self) -> Option<PresetNamespace> {
        self.default_namespace
    }

    /**
    Sets the preset namespace name which will be used from this point forward in this method
    chain by calls to methods such as `req`, `opt`, `first`, `all`, etc, so that they know which
    namespace name should be presumed if no explicit namespace name is provided when calling
    those methods as part of this chain.
    */
    pub fn pre_ns(mut self, default_namespace: &'static str) -> Self {
        if let Status::Found(_) = self.status {
            let default_namespace = if default_namespace.is_empty() {
                None
            } else {
                Some(PresetNamespace::new(default_namespace))
            };
            self.default_namespace = default_namespace;
        }
        self
    }

    /**
    Points this cursor at the **first** child element (of the element which the cursor is
    currently pointing at) with the given name, if such an element exists. Otherwise marks the
    cursor as having an "invalid/error" state.

    Note that if the state of the cursor was already "invalid" before calling this method, then
    the state will remain the way it was, whether or not the named child element exists.
    */
    pub fn req<'b, N>(mut self, full_name: N) -> Self
    where
        N: Into<NameRef<'b>>,
    {
        if let Status::Found(e) = self.status {
            match e.r_child((full_name.into(), self.default_namespace)) {
                Ok(c) => self.status = Status::Found(c),
                Err(x) => {
                    let message = match x {
                        XmlError::ElementAbsent(f) => f,
                        _ => "Failed to find required child element."
                            .to_string()
                            .into_boxed_str(),
                    };
                    self.status = Status::Invalid(message);
                }
            }
        }
        self
    }

    /**
    Converts this cursor into a `MixedCursor`, then points it at the **first** child element
    with the given name if such an element exists, otherwise marks the cursor as having an
    "acceptably absent" state.

    Note that if the state of the cursor was already "invalid" before calling this method, then
    the state will remain the way it was, whether or not the named child element exists.
    */
    pub fn opt<'b, N>(self, full_name: N) -> MixedCursor<'a>
    where
        N: Into<NameRef<'b>>,
    {
        let mixed_cursor = MixedCursor::from_req(self);
        mixed_cursor.opt(full_name)
    }

    /**
    Returns a reference to the element which this cursor is ponting at if it exists, or returns
    an `XmlError` if the cursor state is "invalid" (meaning that the cursor was pointing to an
    element which exists and then `req` was called with a non-existent child element).

    # Errors

    This method will return an `XmlError` if anywhere along the cursor method chain a valid
    cursor was asked to point to a required child element which was not found.
    */
    pub fn element(self) -> Result<&'a Element, XmlError> {
        match self.status {
            Status::Found(e) => Ok(e),
            Status::Invalid(m) => Err(XmlError::ElementAbsent(m)),
        }
    }

    /**
    Returns the text content of the **required** attribute (within the element at which this
    cursor is pointing) with the given name, or returns an `XmlError` if the attribute does not
    exist, or if the cursor state is "invalid" (meaning that the cursor was pointing to an
    element which exists and then `req` was called with a non-existent child element).

    # Errors

    This method will return an `XmlError` if anywhere along the cursor method chain a valid
    cursor was asked to point to a required child element which was not found, or if the cursor
    target element exists but the required attribute is not found.
    */
    pub fn att_req<'b, N>(self, att_name: N) -> Result<&'a str, XmlError>
    where
        N: Into<NameRef<'b>>,
    {
        match self.status {
            Status::Found(e) => Ok(e.att_req((att_name.into(), self.default_namespace))?),
            Status::Invalid(m) => Err(XmlError::ElementAbsent(m)),
        }
    }

    /**
    Returns the text content of the **optional** attribute (within the element at which this
    cursor is pointing) with the given name, or returns an `XmlError` if the cursor state is
    "invalid" (meaning that the cursor was pointing to an element which exists and then `req`
    was called with a non-existent child element), or returns `None` if the attribute does not
    exist.

    # Errors

    This method will return an `XmlError` if anywhere along the cursor method chain a valid
    cursor was asked to point to a required child element which was not found.
    */
    pub fn att_opt<'b, N>(self, att_name: N) -> Result<Option<&'a str>, XmlError>
    where
        N: Into<NameRef<'b>>,
    {
        match self.status {
            Status::Found(e) => Ok(e.att_opt((att_name.into(), self.default_namespace))),
            Status::Invalid(m) => Err(XmlError::ElementAbsent(m)),
        }
    }

    /**
    Returns the text content of the element (at which this cursor is pointing), or returns an
    `XmlError` if the element does not contain simple content, or if the cursor state is
    "invalid" (meaning that the cursor was pointing to an element which exists and then `req`
    was called with a non-existent child element).

    # Errors

    This method will return an `XmlError` if anywhere along the cursor method chain a valid
    cursor was asked to point to a required child element which was not found, or if the cursor
    target element exists but contains non-simple content (child elements or processing
    instructions).
    */
    pub fn text(self) -> Result<&'a str, XmlError> {
        match self.status {
            Status::Found(e) => match e.text() {
                Ok(t) => Ok(t),
                Err(m) => Err(m),
            },
            Status::Invalid(m) => Err(XmlError::ElementAbsent(m)),
        }
    }
}

#[cfg(test)]
mod tests {

    use std::io::Error;

    use crate::parsing::XmlReader;
    use crate::xml::ElementBuilder;
    use crate::xml::Name;
    use crate::xml::XmlDocument;

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    fn parse_raw_xml(raw_xml: &str) -> Result<XmlDocument, Error> {
        let bytes = raw_xml.as_bytes();
        XmlReader::parse_auto(bytes)
    }

    #[test]
    fn empty_root() -> Result<(), Error> {
        let root = Element::new(ElementBuilder::new(Name::new("root", "")));
        let cursor = root.req("nonexistent");
        assert!(cursor.element().is_err());
        Ok(())
    }

    #[test]
    fn root_with_child() -> Result<(), Error> {
        let xml = "
<root>
    <child>text</child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let child = root.req("child").element();
        assert!(child.is_ok());
        let child = child.unwrap();
        assert_eq!(child.name().local_part(), "child");
        assert_eq!(root.req("child").text().unwrap(), "text");

        Ok(())
    }

    #[test]
    fn root_with_grandchild() -> Result<(), Error> {
        let xml = "
<root>
    <child>
        <grandchild>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let grandchild = root.req("child").req("grandchild").element();
        assert!(grandchild.is_ok());
        let grandchild = grandchild.unwrap();
        assert_eq!(grandchild.name().local_part(), "grandchild");
        assert_eq!(grandchild.name().namespace(), None);
        assert_eq!(root.req("child").req("grandchild").text().unwrap(), "text");

        Ok(())
    }

    #[test]
    fn root_without_grandchild() -> Result<(), Error> {
        let xml = "
<root>
    <child>text</child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let grandchild = root.req("child").req("grandchild").element();
        assert!(grandchild.is_err());
        assert!(root.req("child").req("grandchild").text().is_err());

        Ok(())
    }

    #[test]
    fn namespaces_explicit() -> Result<(), Error> {
        let xml = "
<root xmlns='example.com/ns'>
    <child>
        <grandchild>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let grandchild = root
            .req(("child", "example.com/ns"))
            .req(("grandchild", "example.com/ns"))
            .element();
        assert!(grandchild.is_ok());
        let grandchild = grandchild.unwrap();
        assert_eq!(grandchild.name().local_part(), "grandchild");
        assert_eq!(grandchild.name().namespace(), Some("example.com/ns"));

        // Without specifying the namespace, nothing should be found.
        assert!(root.req("child").req("grandchild").element().is_err());

        Ok(())
    }

    #[test]
    fn namespaces_using_preset() -> Result<(), Error> {
        let xml = "
<a xmlns='example.com/ns1'>
    <b>
        <c xmlns='example.com/ns2'>
            <d>
                <e xmlns='example.com/ns3'>
                    <f>text</f>
                </e>
            </d>
        </c>
    </b>
</a>
";
        let doc = parse_raw_xml(xml)?;
        let alpha = doc.root();
        let foxtrot = alpha
            .pre_ns("example.com/ns1")
            .req("b")
            .pre_ns("example.com/ns2")
            .req("c")
            .req("d")
            .pre_ns("example.com/ns3")
            .req("e")
            .req("f")
            .element();
        assert!(foxtrot.is_ok());
        let foxtrot = foxtrot.unwrap();
        assert_eq!(foxtrot.text().unwrap(), "text");

        let foxtrot_text = alpha
            .pre_ns("example.com/ns1")
            .req("b")
            .pre_ns("example.com/ns2")
            .req("c")
            .req("d")
            .pre_ns("example.com/ns3")
            .req("e")
            .req("f")
            .text()
            .unwrap();
        assert_eq!(foxtrot_text, "text");

        Ok(())
    }

    #[test]
    fn att_opt() -> Result<(), Error> {
        let xml = "
<root>
    <child id='1'>
        <grandchild id='2'>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let child_id = root.req("child").att_opt("id");
        assert!(child_id.is_ok());
        assert_eq!(child_id.unwrap(), Some("1"));

        let no_such_att = root.req("child").att_opt("mode");
        assert!(no_such_att.unwrap().is_none());

        let no_child_id = root.req("sibling").att_opt("id");
        assert!(no_child_id.is_err());

        let grandchild_id = root.req("child").req("grandchild").att_opt("id");
        assert!(grandchild_id.is_ok());
        assert_eq!(grandchild_id.unwrap(), Some("2"));

        let no_such_att = root.req("child").req("grandchild").att_opt("mode");
        assert!(no_such_att.unwrap().is_none());

        let no_grandchild_id = root.req("child").req("disinherited").att_opt("id");
        assert!(no_grandchild_id.is_err());
        Ok(())
    }

    #[test]
    fn att_req() -> Result<(), Error> {
        let xml = "
<root>
    <child id='1'>
        <grandchild id='2'>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let child_id = root.req("child").att_req("id");
        assert!(child_id.is_ok());
        assert_eq!(child_id.unwrap(), "1");

        let no_such_att = root.req("child").att_req("mode");
        assert!(no_such_att.is_err());

        let no_child_id = root.req("sibling").att_req("id");
        assert!(no_child_id.is_err());

        let grandchild_id = root.req("child").req("grandchild").att_req("id");
        assert!(grandchild_id.is_ok());
        assert_eq!(grandchild_id.unwrap(), "2");

        let no_such_att = root.req("child").req("grandchild").att_req("mode");
        assert!(no_such_att.is_err());

        let no_grandchild_id = root.req("child").req("disinherited").att_req("id");
        assert!(no_grandchild_id.is_err());
        Ok(())
    }

    #[test]
    fn att_opt_namespaces() -> Result<(), Error> {
        let xml = "
<root xmlns:p='example.com/p'>
    <child p:id='1'>
        <grandchild p:id='2'>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let child_id = root.req("child").pre_ns("example.com/p").att_opt("id");
        assert!(child_id.is_ok());
        assert_eq!(child_id.unwrap(), Some("1"));

        let namespace_missing = root.req("child").att_opt("id");
        assert!(namespace_missing.is_ok());
        assert!(namespace_missing.unwrap().is_none());

        let grandchild_id = root
            .req("child")
            .req("grandchild")
            .att_opt(("id", "example.com/p"));
        assert!(grandchild_id.is_ok());
        assert_eq!(grandchild_id.unwrap(), Some("2"));

        let namespace_missing = root.req("child").req("grandchild").att_opt("id");
        assert!(namespace_missing.is_ok());
        assert!(namespace_missing.unwrap().is_none());
        Ok(())
    }

    #[test]
    fn att_req_namespaces() -> Result<(), Error> {
        let xml = "
<root xmlns:p='example.com/p'>
    <child p:id='1'>
        <grandchild p:id='2'>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let child_id = root.req("child").pre_ns("example.com/p").att_req("id");
        assert!(child_id.is_ok());
        assert_eq!(child_id.unwrap(), "1");

        let namespace_missing = root.req("child").att_req("id");
        assert!(namespace_missing.is_err());

        let grandchild_id = root
            .req("child")
            .req("grandchild")
            .att_req(("id", "example.com/p"));
        assert!(grandchild_id.is_ok());
        assert_eq!(grandchild_id.unwrap(), "2");

        let namespace_missing = root.req("child").req("grandchild").att_req("id");
        assert!(namespace_missing.is_err());
        Ok(())
    }
}
