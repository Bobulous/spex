use crate::{
    extraction::ExtendedLanguageRange, extraction::NameRef, extraction::PresetNamespace,
    xml::Content, xml::Element,
};

pub struct XmlPath<'e, 'n> {
    start_from: &'e Element,
    assume_namespace: Option<PresetNamespace>,
    steps: Vec<Step<'n>>,
}

struct Step<'n> {
    name: NameRef<'n>,
    attribute: Option<(NameRef<'n>, &'n str)>,
    lang_range: Option<&'n ExtendedLanguageRange<'n>>,
    coverage: Coverage,
}

enum Coverage {
    All,
    First,
    Last,
    Nth(usize),
}

#[derive(Debug)]
struct SiblingIterator<'e, 'n> {
    siblings: &'e [Content],
    matching_name: &'n NameRef<'n>,
    matching_attribute: Option<(&'n NameRef<'n>, &'n str)>,
    filter_lang_range: Option<&'n ExtendedLanguageRange<'n>>,
    at_index: usize,
    finished: bool,
}

/*
Note: I spent a couple of hours trying to implement `IntoIterator`
for `XmlPath`, but it became too knotty because when an `Element`
is used to call (for example) `all` it creates an owned `XmlPath`.
But when an `XmlPath` is used to call `all` it takes and returns
a `&mut XmlPath`. And I couldn't find a way to get the trait to
cover both owned and &mut variables at the same time. Which meant
that some uses worked fine (such as
`for e in root.all("a").all("b")`) while other uses would give a
compiler error (such as `for e in root.all("a")`).
*/
struct PathIterator<'e, 'n> {
    path: &'n XmlPath<'e, 'n>,
    at_depth: usize,
    iterator_stack: Vec<SiblingIterator<'e, 'n>>,
}

// 'e is lifetime of Element reference
// 'n is lifetime of NameRef references
impl<'e, 'n> XmlPath<'e, 'n>
where
    'e: 'n,
{
    pub(crate) fn new(start_from: &'e Element, assume_namespace: Option<PresetNamespace>) -> Self {
        XmlPath {
            start_from,
            assume_namespace,
            steps: Vec::with_capacity(2),
        }
    }

    /**
    Sets the preset namespace name which will be used from this point forward in this method
    chain by calls to methods `all`, `first`, `last`, and `nth`, so that they know which
    namespace name should be presumed if no explicit namespace name is provided when calling
    them as part of this chain.
    */
    pub fn pre_ns(&mut self, default_namespace: &'static str) -> &mut Self {
        let default_namespace = if default_namespace.is_empty() {
            None
        } else {
            Some(PresetNamespace::new(default_namespace))
        };
        self.assume_namespace = default_namespace;
        self
    }

    fn resolve_target_name<N>(
        full_name: N,
        assume_namespace: Option<PresetNamespace>,
    ) -> NameRef<'n>
    where
        N: Into<NameRef<'n>>,
    {
        let full_name = full_name.into();
        let mut resolved: NameRef<'n> = (full_name, assume_namespace).into();
        // If the namespace is an (explicitly) empty string then convert it to None.
        if resolved.namespace().is_some_and(|n| n.is_empty()) {
            // I could not get resolved.namespace().take() to work from here, so this
            // without_namespace method creates a replacement object with namespace:None.
            resolved = resolved.without_namespace();
        }
        resolved
    }

    /**
    Instructs this `XmlPath` to consider all of the child elements (of the element(s) considered
    by the previous step in this path) with the given name.
    */
    pub fn all<N>(&mut self, name: N) -> &mut Self
    where
        N: Into<NameRef<'n>>,
    {
        self.steps.push(Step {
            name: XmlPath::resolve_target_name(name, self.assume_namespace),
            attribute: None,
            lang_range: None,
            coverage: Coverage::All,
        });
        self
    }

    /**
    Instructs this `XmlPath` to consider the first child element (of the element(s) considered
    by the previous step in this path) with the given name.
    */
    pub fn first<N>(&mut self, name: N) -> &mut Self
    where
        N: Into<NameRef<'n>>,
    {
        self.steps.push(Step {
            name: XmlPath::resolve_target_name(name, self.assume_namespace),
            attribute: None,
            lang_range: None,
            coverage: Coverage::First,
        });
        self
    }

    /**
    Instructs this `XmlPath` to consider the last child element (of the element(s) considered
    by the previous step in this path) with the given name.
    */
    pub fn last<N>(&mut self, name: N) -> &mut Self
    where
        N: Into<NameRef<'n>>,
    {
        self.steps.push(Step {
            name: XmlPath::resolve_target_name(name, self.assume_namespace),
            attribute: None,
            lang_range: None,
            coverage: Coverage::Last,
        });
        self
    }

    /**
    Instructs this `XmlPath` to consider the nth (where 0 means first) child element (of the
    element(s) considered by the previous step in this path) with the given name.
    */
    pub fn nth<N>(&mut self, name: N, n: usize) -> &mut Self
    where
        N: Into<NameRef<'n>>,
    {
        self.steps.push(Step {
            name: XmlPath::resolve_target_name(name, self.assume_namespace),
            attribute: None,
            lang_range: None,
            coverage: Coverage::Nth(n),
        });
        self
    }

    /**
    Instructs this `XmlPath` to adjust the requirement of the current step so that it only
    accepts an element which also has the specified attribute name-value pair.

    # Examples

    If you want to iterate all of the "box" elements which have an attribute `type` with the
    value `available`, within all elements named "warehouse" which appear within the element
    you're currently holding in a variable named `root`, then you could use this:

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, Element, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    # <stuff>
    #    <warehouse location='LON'>
    #        <box type='on-loan'>monitors</box>
    #        <box type='available'>GPUs</box>
    #    </warehouse>
    #    <warehouse location='MAN'>
    #        <box type='on-loan'>soundcards</box>
    #        <box type='available'>keyboards</box>
    #    </warehouse>
    # </stuff>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #
    #
    # fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
    #     let root = xml_doc.root();
    #
    # let mut box_count = 0;
    for b in root.all("warehouse").all("box").with_attribute("type", "available").iter() {
        println!("Found available box containing: {}", b.text()?);
    # box_count += 1;
    }
    # assert_eq!(box_count, 2);
    #
    #     Ok(())
    # }
    ```

    If you want to narrow it down further so that you only consider the "warehouse" elements
    which have a `location` attribute with value `LON`, then you could use this:

    ```
    # use std::fs::File;
    # use spex::{parsing::XmlReader, xml::{XmlDocument, Element, ProcessingInstruction,
    # WhiteSpaceHandling}, common::XmlError};
    # fn main() {
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    #    let xml = "
    # <stuff>
    #    <warehouse location='LON'>
    #        <box type='on-loan'>monitors</box>
    #        <box type='available'>GPUs</box>
    #    </warehouse>
    #    <warehouse location='MAN'>
    #        <box type='on-loan'>soundcards</box>
    #        <box type='available'>keyboards</box>
    #    </warehouse>
    # </stuff>
    # ";
    #    let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #
    #
    # fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
    #     let root = xml_doc.root();
    #
    # let mut box_count = 0;
    for b in root.all("warehouse")
        .with_attribute("location", "LON")
        .all("box")
        .with_attribute("type", "available")
        .iter() {
        println!("Found available box in London containing: {}", b.text()?);
    # box_count += 1;
    }
    # assert_eq!(box_count, 1);
    #
    #     Ok(())
    # }
    ```
    */
    pub fn with_attribute<N>(&mut self, name: N, value: &'n str) -> &mut Self
    where
        N: Into<NameRef<'n>>,
    {
        if let Some(step) = self.steps.last_mut() {
            // let resolved: NameRef<'n> = (name.into(), self.assume_namespace).into();
            let resolved = XmlPath::resolve_target_name(name, self.assume_namespace);
            step.attribute = Some((resolved, value));
        }
        self
    }

    /**
    Instructs this `XmlPath` to adjust the requirement of the current step so that it only
    accepts an element which also has a language tag which is accepted by the given extended
    language range. This uses "extended filtering" as described in
    [RFC-4647](https://datatracker.ietf.org/doc/html/rfc4647#section-3.3.2).

    # Examples

    If you want to iterate the titles of all books, but only the titles which satisfy the
    extended language range "fr", then you can use something like this:

    ```
    # use spex::{parsing::XmlReader, xml::XmlDocument, common::XmlError,
    # extraction::ExtendedLanguageRange};
    # fn main() {
    #    // Panic if either step generates an error.
    #    let xml_doc = read_xml().unwrap();
    #    extract_xml(xml_doc).unwrap();
    # }
    #
    # // The XML parsing methods might throw an std::io::Error, so they go into their own method.
    # fn read_xml() -> Result<XmlDocument, std::io::Error> {
    # let xml = "
    # <root>
    #     <book>
    #         <title xml:lang='en-US'>Home</title>
    #         <title xml:lang='sv-SE'>Hemma</title>
    #         <title xml:lang='fr-FR'>Chez moi</title>
    #     </book>
    #     <book>
    #         <title xml:lang='en-US'>Hellhound</title>
    #         <title xml:lang='sv-SE'>Helveteshund</title>
    #         <title xml:lang='fr-FR'>Chien d l'enfer</title>
    #     </book>
    # </root>
    # ";
    # let xml_doc = XmlReader::parse_auto(xml.as_bytes());
    #    xml_doc
    # }
    #
    # fn extract_xml(xml_doc: XmlDocument) -> Result<(), XmlError> {
    # let root = xml_doc.root();
    # let mut title_count = 0;
    let french = ExtendedLanguageRange::new("fr").unwrap();
    for french_title in root.all("book").all("title").filter_lang_range(&french).iter() {
        # title_count += 1;
        println!("Found book title in French: {}", french_title.text()?);
    }
    # assert_eq!(title_count, 2);
    #    Ok(())
    # }
    ```
    */
    pub fn filter_lang_range(&mut self, range: &'n ExtendedLanguageRange<'n>) -> &mut Self {
        if let Some(step) = self.steps.last_mut() {
            step.lang_range = Some(range);
        }
        self
    }

    /**
    Turns this `XmlPath` into an iterator, which will provide references to each element at the
    end of the path. If the path criteria are not satisfied by any element, then the iterator
    will provide nothing.
    */
    // Without adding +'n to the return type, we get an error because the
    // impl Iterator itself doesn't promise to live as long as 'n
    // (it just promises that the iterated type will last as long as 'e).
    pub fn iter(&'n self) -> impl Iterator<Item = &'e Element> + 'n {
        PathIterator::new(self)
    }
}

impl<'e, 'n> PathIterator<'e, 'n>
where
    'e: 'n,
{
    fn new(path: &'n XmlPath<'e, 'n>) -> Self {
        let mut path_iterator = PathIterator {
            path,
            at_depth: 1,
            iterator_stack: Vec::new(),
        };
        let first_step = &path_iterator.path.steps.first().unwrap();
        let n = &first_step.name;
        let sibling_iterator = SiblingIterator {
            siblings: path_iterator.path.start_from.content(),
            matching_name: n,
            matching_attribute: if let Some((name, value)) = &first_step.attribute {
                Some((&name, value))
            } else {
                None
            },
            filter_lang_range: if let Some(range) = &first_step.lang_range {
                Some(range)
            } else {
                None
            },
            at_index: 0,
            finished: false,
        };
        path_iterator.iterator_stack.push(sibling_iterator);
        path_iterator
    }
}

impl<'e, 'n> Iterator for PathIterator<'e, 'n>
where
    'e: 'n,
{
    type Item = &'e Element;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(sibling_iterator) = self.iterator_stack.last_mut() {
                let path_step = self.path.steps.get(self.at_depth - 1).unwrap();
                let outcome = match path_step.coverage {
                    Coverage::All => sibling_iterator.find_next_match(),
                    Coverage::First => sibling_iterator.find_first_match(),
                    Coverage::Last => sibling_iterator.find_last_match(),
                    Coverage::Nth(n) => sibling_iterator.find_nth_match(n),
                };
                if let Some(matching_element) = outcome {
                    if self.at_depth == self.path.steps.len() {
                        return Some(matching_element);
                    }
                    let new_path_step = self.path.steps.get(self.at_depth).unwrap();
                    let child_sibling_iterator = SiblingIterator {
                        siblings: matching_element.content(),
                        matching_name: &new_path_step.name,
                        matching_attribute: {
                            if let Some((name, value)) = &new_path_step.attribute {
                                Some((&name, value))
                            } else {
                                None
                            }
                        },
                        filter_lang_range: {
                            if let Some(range) = &new_path_step.lang_range {
                                Some(range)
                            } else {
                                None
                            }
                        },
                        at_index: 0,
                        finished: false,
                    };
                    self.iterator_stack.push(child_sibling_iterator);
                    self.at_depth += 1;
                } else {
                    self.iterator_stack.pop();
                    if !self.iterator_stack.is_empty() {
                        self.at_depth -= 1;
                    }
                }
            } else {
                return None;
            }
        }
    }
}

impl<'e, 'n> SiblingIterator<'e, 'n> {
    fn find_next_match(&mut self) -> Option<&'e Element> {
        let target_name = self.matching_name;
        let length = self.siblings.len();
        let start_index = self.at_index;
        for i in start_index..length {
            let s = self.siblings.get(i).unwrap();
            match s {
                Content::Text(_) => continue,
                Content::PI(_) => continue,
                Content::Child(e) => {
                    if e.is_named(target_name)
                        && self.check_attribute_match(e)
                        && self.check_lang_range_match(e)
                    {
                        self.at_index = i + 1;
                        return Some(e);
                    }
                }
            }
        }
        self.finished = true;
        None
    }

    fn check_attribute_match(&self, e: &Element) -> bool {
        if let Some((att_name, target_value)) = self.matching_attribute {
            if let Some(value) = e.att_opt(att_name) {
                value == target_value
            } else {
                // No such attribute name found in element.
                false
            }
        } else {
            // No attribute criteria specified, so element is considered a match.
            true
        }
    }

    fn check_lang_range_match(&self, e: &Element) -> bool {
        if let Some(range) = self.filter_lang_range {
            if let Some(lang) = e.language_tag() {
                lang.satisfies_filter(range)
            } else {
                // The element has not specified (nor inherited) any language tag,
                // so it can only match an extended language range which is empty
                // or is simply the "*" wildcard.
                range.subtags().is_empty()
                    || (range.subtags().len() == 1 && range.subtags()[0] == "*")
            }
        } else {
            // No language range specified, so element is considered a match.
            true
        }
    }

    fn find_first_match(&mut self) -> Option<&'e Element> {
        if self.finished {
            return None;
        }
        let target_name = self.matching_name;
        let length = self.siblings.len();
        let start_index = self.at_index;
        for i in start_index..length {
            let s = self.siblings.get(i).unwrap();
            match s {
                Content::Text(_) => continue,
                Content::PI(_) => continue,
                Content::Child(e) => {
                    if e.is_named(target_name)
                        && self.check_attribute_match(e)
                        && self.check_lang_range_match(e)
                    {
                        self.finished = true;
                        return Some(e);
                    }
                }
            }
        }
        None
    }

    fn find_last_match(&mut self) -> Option<&'e Element> {
        if self.finished {
            return None;
        }
        let target_name = self.matching_name;
        let length = self.siblings.len();
        let start_index = self.at_index;
        for i in (start_index..length).rev() {
            let s = self.siblings.get(i).unwrap();
            match s {
                Content::Text(_) => continue,
                Content::PI(_) => continue,
                Content::Child(e) => {
                    if e.is_named(target_name)
                        && self.check_attribute_match(e)
                        && self.check_lang_range_match(e)
                    {
                        self.finished = true;
                        return Some(e);
                    }
                }
            }
        }
        None
    }

    fn find_nth_match(&mut self, n: usize) -> Option<&'e Element> {
        if self.finished {
            return None;
        }
        let target_name = self.matching_name;
        let length = self.siblings.len();
        let start_index = self.at_index;
        let mut match_count = 0;
        for i in start_index..length {
            let s = self.siblings.get(i).unwrap();
            match s {
                Content::Text(_) => continue,
                Content::PI(_) => continue,
                Content::Child(e) => {
                    if e.is_named(target_name)
                        && self.check_attribute_match(e)
                        && self.check_lang_range_match(e)
                    {
                        if match_count == n {
                            self.finished = true;
                            return Some(e);
                        }
                        match_count += 1;
                    }
                }
            }
        }
        None
    }
}

#[cfg(test)]
mod tests {
    use std::io::Error;

    use crate::parsing::XmlReader;
    use crate::xml::XmlDocument;

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    fn parse_raw_xml(raw_xml: &str) -> Result<XmlDocument, Error> {
        let bytes = raw_xml.as_bytes();
        XmlReader::parse_auto(bytes)
    }

    #[test]
    fn no_such_path() -> Result<(), Error> {
        let xml = "<root></root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let mut path = XmlPath {
            start_from: root,
            assume_namespace: None,
            steps: Vec::with_capacity(4),
        };

        path.all("child").all("grandchild").all("inner");
        let elements: Vec<&Element> = path.iter().collect();
        assert!(elements.is_empty());

        Ok(())
    }

    #[test]
    fn all_children() -> Result<(), Error> {
        let xml = "<root><child>first</child><child>second</child></root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let mut child_references = Vec::with_capacity(2);
        for child in XmlPath::new(root, None).all("child").iter() {
            child_references.push(child);
        }
        assert_eq!(child_references.len(), 2);
        Ok(())
    }

    #[test]
    fn all_great_grandchildren() -> Result<(), Error> {
        let xml = "<root>
    <child>
        <grandchild>
            <inner>1</inner>
        </grandchild>
        <grandchild>
            <inner>2</inner>
        </grandchild>
    </child>
    <child><!-- No grandchildren --></child>
    <child>
        <grandchild>
            <inner>3</inner>
        </grandchild>
    </child>
    <child>
        <grandchild>
            <inner>4</inner>
        </grandchild>
        <grandchild>
            <inner>5</inner>
        </grandchild>
        <grandchild>
            <inner>6</inner>
        </grandchild>
    </child>
</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let mut path = XmlPath {
            start_from: root,
            assume_namespace: None,
            steps: Vec::with_capacity(4),
        };

        path.all("child").all("grandchild").all("inner");
        let elements: Vec<&Element> = path.iter().collect();
        let result: Vec<&str> = elements.iter().map(|e| e.text().unwrap()).collect();
        assert_eq!(result, vec!["1", "2", "3", "4", "5", "6"]);

        Ok(())
    }

    #[test]
    fn first_child_last_grandchild() -> Result<(), Error> {
        let xml = "<root>
    <child>
        <grandchild>
            <inner>1</inner>
        </grandchild>
        <grandchild>
            <inner>2</inner>
        </grandchild>
    </child>
    <child><!-- No grandchildren --></child>
    <child>
        <grandchild>
            <inner>3</inner>
        </grandchild>
    </child>
    <child>
        <grandchild>
            <inner>4</inner>
        </grandchild>
        <grandchild>
            <inner>5</inner>
        </grandchild>
        <grandchild>
            <inner>6</inner>
        </grandchild>
    </child>
</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let mut path = XmlPath {
            start_from: root,
            assume_namespace: None,
            steps: Vec::with_capacity(4),
        };

        path.first("child").last("grandchild").all("inner");
        let elements: Vec<&Element> = path.iter().collect();
        let result: Vec<&str> = elements.iter().map(|e| e.text().unwrap()).collect();
        assert_eq!(result, vec!["2"]);

        Ok(())
    }

    #[test]
    fn first_child_first_grandchild() -> Result<(), Error> {
        let xml = "<root>
    <child>
        <grandchild>
            <inner>1</inner>
        </grandchild>
        <grandchild>
            <inner>2</inner>
        </grandchild>
    </child>
    <child><!-- No grandchildren --></child>
    <child>
        <grandchild>
            <inner>3</inner>
        </grandchild>
    </child>
    <child>
        <grandchild>
            <inner>4</inner>
        </grandchild>
        <grandchild>
            <inner>5</inner>
        </grandchild>
        <grandchild>
            <inner>6</inner>
        </grandchild>
    </child>
</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let mut path = XmlPath {
            start_from: root,
            assume_namespace: None,
            steps: Vec::with_capacity(4),
        };

        path.first("child").first("grandchild").all("inner");
        let elements: Vec<&Element> = path.iter().collect();
        let result: Vec<&str> = elements.iter().map(|e| e.text().unwrap()).collect();
        assert_eq!(result, vec!["1"]);

        Ok(())
    }

    #[test]
    fn last_child_first_grandchild() -> Result<(), Error> {
        let xml = "<root>
    <child>
        <grandchild>
            <inner>1</inner>
        </grandchild>
        <grandchild>
            <inner>2</inner>
        </grandchild>
    </child>
    <child><!-- No grandchildren --></child>
    <child>
        <grandchild>
            <inner>3</inner>
        </grandchild>
    </child>
    <child>
        <grandchild>
            <inner>4</inner>
        </grandchild>
        <grandchild>
            <inner>5</inner>
        </grandchild>
        <grandchild>
            <inner>6</inner>
        </grandchild>
    </child>
</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let mut path = XmlPath {
            start_from: root,
            assume_namespace: None,
            steps: Vec::with_capacity(4),
        };

        path.last("child").first("grandchild").all("inner");
        let elements: Vec<&Element> = path.iter().collect();
        let result: Vec<&str> = elements.iter().map(|e| e.text().unwrap()).collect();
        assert_eq!(result, vec!["4"]);

        Ok(())
    }

    #[test]
    fn last_child_last_grandchild() -> Result<(), Error> {
        let xml = "<root>
    <child>
        <grandchild>
            <inner>1</inner>
        </grandchild>
        <grandchild>
            <inner>2</inner>
        </grandchild>
    </child>
    <child><!-- No grandchildren --></child>
    <child>
        <grandchild>
            <inner>3</inner>
        </grandchild>
    </child>
    <child>
        <grandchild>
            <inner>4</inner>
        </grandchild>
        <grandchild>
            <inner>5</inner>
        </grandchild>
        <grandchild>
            <inner>6</inner>
        </grandchild>
    </child>
</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let mut path = XmlPath {
            start_from: root,
            assume_namespace: None,
            steps: Vec::with_capacity(4),
        };

        path.last("child").last("grandchild").all("inner");
        let elements: Vec<&Element> = path.iter().collect();
        let result: Vec<&str> = elements.iter().map(|e| e.text().unwrap()).collect();
        assert_eq!(result, vec!["6"]);

        Ok(())
    }

    #[test]
    fn second_child() -> Result<(), Error> {
        let xml = "<root>
    <child>
        <grandchild>
            <inner>1</inner>
        </grandchild>
        <grandchild>
            <inner>2</inner>
        </grandchild>
    </child>
    <child><!-- No grandchildren --></child>
    <child>
        <grandchild>
            <inner>3</inner>
        </grandchild>
    </child>
    <child>
        <grandchild>
            <inner>4</inner>
        </grandchild>
        <grandchild>
            <inner>5</inner>
        </grandchild>
        <grandchild>
            <inner>6</inner>
        </grandchild>
    </child>
</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let mut path = XmlPath {
            start_from: root,
            assume_namespace: None,
            steps: Vec::with_capacity(4),
        };

        path.nth("child", 1).all("grandchild").all("inner");
        let elements: Vec<&Element> = path.iter().collect();
        assert!(elements.is_empty());

        Ok(())
    }

    #[test]
    fn third_child() -> Result<(), Error> {
        let xml = "<root>
    <child>
        <grandchild>
            <inner>1</inner>
        </grandchild>
        <grandchild>
            <inner>2</inner>
        </grandchild>
    </child>
    <child><!-- No grandchildren --></child>
    <child>
        <grandchild>
            <inner>3</inner>
        </grandchild>
    </child>
    <child>
        <grandchild>
            <inner>4</inner>
        </grandchild>
        <grandchild>
            <inner>5</inner>
        </grandchild>
        <grandchild>
            <inner>6</inner>
        </grandchild>
    </child>
</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let mut path = XmlPath {
            start_from: root,
            assume_namespace: None,
            steps: Vec::with_capacity(4),
        };

        path.nth("child", 2).all("grandchild").all("inner");
        let elements: Vec<&Element> = path.iter().collect();
        let result: Vec<&str> = elements.iter().map(|e| e.text().unwrap()).collect();
        assert_eq!(result, vec!["3"]);

        Ok(())
    }

    #[test]
    fn fourth_child_third_grandchild() -> Result<(), Error> {
        let xml = "<root>
    <child>
        <grandchild>
            <inner>1</inner>
        </grandchild>
        <grandchild>
            <inner>2</inner>
        </grandchild>
    </child>
    <child><!-- No grandchildren --></child>
    <child>
        <grandchild>
            <inner>3</inner>
        </grandchild>
    </child>
    <child>
        <grandchild>
            <inner>4</inner>
        </grandchild>
        <grandchild>
            <inner>5</inner>
        </grandchild>
        <grandchild>
            <inner>6</inner>
        </grandchild>
    </child>
</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let mut path = XmlPath {
            start_from: root,
            assume_namespace: None,
            steps: Vec::with_capacity(4),
        };

        path.nth("child", 3).nth("grandchild", 2).all("inner");
        let elements: Vec<&Element> = path.iter().collect();
        let result: Vec<&str> = elements.iter().map(|e| e.text().unwrap()).collect();
        assert_eq!(result, vec!["6"]);

        Ok(())
    }

    #[test]
    fn no_matching_attribute() -> Result<(), Error> {
        let xml = "<root>
    <child>
        <grandchild>
            <inner>1</inner>
        </grandchild>
        <grandchild>
            <inner>2</inner>
        </grandchild>
    </child>
    <child><!-- No grandchildren --></child>
    <child>
        <grandchild>
            <inner>3</inner>
        </grandchild>
    </child>
    <child>
        <grandchild>
            <inner>4</inner>
        </grandchild>
        <grandchild>
            <inner>5</inner>
        </grandchild>
        <grandchild>
            <inner>6</inner>
        </grandchild>
    </child>
</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let mut path = XmlPath {
            start_from: root,
            assume_namespace: None,
            steps: Vec::with_capacity(4),
        };

        path.all("child")
            .all("grandchild")
            .with_attribute("type", "eldest")
            .all("inner");
        let elements: Vec<&Element> = path.iter().collect();
        assert!(elements.is_empty());

        Ok(())
    }

    #[test]
    fn with_matching_attribute() -> Result<(), Error> {
        let xml = "<root>
    <child type='eldest'>
        <grandchild type='eldest'>
            <inner>1</inner>
        </grandchild>
        <grandchild type='youngest'>
            <inner>2</inner>
        </grandchild>
    </child>
    <child><!-- No grandchildren --></child>
    <child>
        <grandchild type='eldest'>
            <inner>3</inner>
        </grandchild>
    </child>
    <child type='youngest'>
        <grandchild type='eldest'>
            <inner>4</inner>
        </grandchild>
        <grandchild>
            <inner>5</inner>
        </grandchild>
        <grandchild type='youngest'>
            <inner>6</inner>
        </grandchild>
    </child>
</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let mut path = XmlPath {
            start_from: root,
            assume_namespace: None,
            steps: Vec::with_capacity(4),
        };

        path.all("child")
            .all("grandchild")
            .with_attribute("type", "eldest")
            .all("inner");
        let elements: Vec<&Element> = path.iter().collect();
        let result: Vec<&str> = elements.iter().map(|e| e.text().unwrap()).collect();
        assert_eq!(result, vec!["1", "3", "4"]);

        Ok(())
    }

    #[test]
    fn fully_matching_attributes() -> Result<(), Error> {
        let xml = "<root>
    <child type='eldest'>
        <grandchild type='eldest'>
            <inner>1</inner>
        </grandchild>
        <grandchild type='youngest'>
            <inner>2</inner>
        </grandchild>
    </child>
    <child><!-- No grandchildren --></child>
    <child>
        <grandchild type='eldest'>
            <inner>3</inner>
        </grandchild>
    </child>
    <child type='youngest'>
        <grandchild type='eldest'>
            <inner>4</inner>
        </grandchild>
        <grandchild>
            <inner>5</inner>
        </grandchild>
        <grandchild type='youngest'>
            <inner>6</inner>
        </grandchild>
    </child>
</root>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let mut path = XmlPath {
            start_from: root,
            assume_namespace: None,
            steps: Vec::with_capacity(4),
        };

        path.all("child")
            .with_attribute("type", "youngest")
            .all("grandchild")
            .with_attribute("type", "eldest")
            .all("inner");
        let elements: Vec<&Element> = path.iter().collect();
        let result: Vec<&str> = elements.iter().map(|e| e.text().unwrap()).collect();
        assert_eq!(result, vec!["4"]);

        Ok(())
    }

    #[test]
    fn all_matching_language_range() -> Result<(), Error> {
        let xml = "<FeaturedImages xml:lang=\"en\">
        <Note>Top five images from Wikimedia Commons Picture Of The Year 2022</Note>
        <Source>https://commons.wikimedia.org/wiki/Commons:Picture_of_the_Year/2022/Results</Source>
        <Image>
            <Filename>Phalacrocorax carbo, Egretta garzetta and Mareca strepera in Taudha Lake.jpg</Filename>
            <Caption xml:lang='en'>Great Cormorant (Phalacrocorax carbo), Little Egret (Egretta garzetta) and Gadwall (Mareca strepera) in Taudaha Lake, near Katmandu, Nepal</Caption>
            <Caption xml:lang='cs'>kormorán velký, volavka stříbřitá a kopřivka obecná na jezeře Taudaha na předměstí Káthmándú v Nepálu</Caption>
            <Caption xml:lang='de'>Großer Kormoran (Phalacrocorax carbo), Kleiner Reiher (Egretta garzetta) und Schnatterente (Mareca strepera) im Taudaha-See, bei Kathmandu, Nepal</Caption>
            <Caption xml:lang='pl'>Kormoran (Phalacrocorax carbo), czapla nadobna (Egretta garzetta) i krakwa (Mareca strepera) na jeziorze Taudaha, niedaleko Katmandu, Nepal</Caption>
            <Caption xml:lang='zh'>陶达哈湖的普通鸬鹚（Phalacrocorax carbo）、白鹭（Egretta garzetta）和赤膀鸭（Mareca strepera），该湖位于尼泊尔加德满都附近</Caption>
            <Caption xml:lang='es'>Cormorán grande (Phalacrocorax carbo), garceta común (Egretta garzetta) y ánade friso (Mareca strepera) en el lago Taudaha, cerca de Katmandú, Nepal.</Caption>
        </Image>
        <Image>
            <Filename>Ethiopia Banna tribe kids.jpg</Filename>
            <Caption xml:lang='en'>Kids of Banna tribe in Ethiopia playing on wooden stilts</Caption>
            <Caption xml:lang='zh-Hans'>埃塞尔比亚班纳部落的孩子们游玩木制高跷。</Caption>
            <Caption xml:lang='zh'>埃塞尔比亚班纳部落的孩子们游玩木制高跷。</Caption>
            <Caption xml:lang='tr'>Etiyopya'da Banna Kabilesi'ndeki vücutları gelneksel vücut boyasıyla boyanmış çocuklar tahtadan yapılmış cambaz ayaklıklarıyla oynuyorlar.</Caption>
        </Image>
        <Image>
            <Filename>Pillars of Creation (NIRCam Image).jpg</Filename>
            <Caption xml:lang='en'>Young stars form in \"The Pillars of Creation\" as seen by NASA's James Webb Space Telescope's near-infrared camera</Caption>
            <Caption xml:lang='cs'>Sloupy stvoření v Orlí mlhovině na snímku z Vesmírného dalekohledu Jamese Webba</Caption>
            <Caption xml:lang='fa'>ستون‌های آفرینش در سحابی عقاب که توسط تلسکوپ فضایی جیمز وب به تصویر کشیده شده است</Caption>
            <Caption xml:lang='pl'>\"Filary Stworzenia\" w Mgławicy Orzeł sfotografowane przez Kosmiczny Teleskop Jamesa Webba</Caption>
            <Caption xml:lang='de'>Sternentstehung im Adlernebel aufgenommen von der NASA mit einem James-Webb-Weltraumteleskop</Caption>
            <Caption xml:lang='es'>Se forman estrellas jóvenes en 'Los Pilares de la Creación', visto por la cámara de infrarrojo cercano del telescopio espacial James Webb de la NASA</Caption>
            <Caption xml:lang='sv'>Unga stjärnor bildas i skapelsens pelare, som ses av den nära-infraröda kameran på NASA:s James Webb-teleskopet</Caption>
        </Image>
        <Image>
            <Filename>Church of light.jpg</Filename>
            <Caption xml:lang='en'>Northern lights (Aurora borealis) over the Víkurkirkja church at Vík í Mýrdal, Iceland</Caption>
            <Caption xml:lang='de'>Polarlicht über der Kirche in Vík í Mýrdal in Island.</Caption>
            <Caption xml:lang='cs'>polární záře nad kostelem v islandské vesnici Vík í Mýrdal</Caption>
            <Caption xml:lang='fr'>Aurore boréale au-dessus de l'église Víkurkirkja à Vík í Mýrdal, Islande</Caption>
            <Caption xml:lang='pt'>Luzes do Norte (Aurora boreal) sobre a igreja Víkurkirkja em Vík í Mýrdal, Islândia</Caption>
        </Image>
        <Image>
            <Filename xml:lang='de'>Siebenpunkt-Marienkäfer (Coccinella septempunctata) auf Blüte im FFH-Gebiet \"Viernheimer Waldheide und angrenzende Flächen\".jpg</Filename>
            <Caption xml:lang='en'>Seven-spot ladybird (Coccinella septempunctata), taken in Viernheimer Waldheide Special Area of Conservation, Germany</Caption>
            <Caption xml:lang='fr'>Coccinelle à sept points (Coccinella septempunctata), prise dans la zone spéciale de conservation de Viernheimer Waldheide, Allemagne</Caption>
            <Caption xml:lang='uk'>Сонечко семиточкове (Coccinella septempunctata), виловлене в спеціальному заповіднику Viernheimer Waldheide, Німеччина</Caption>
            <Caption xml:lang='la'>Coccinella septempunctata</Caption>
            <Caption xml:lang='de'>Siebenpunkt-Marienkäfer (Coccinella septempunctata) auf Blüte im FFH-Gebiet</Caption>
            <Caption xml:lang='sv'>Sjuprickig nyckelpiga (Coccinella septempunctata) i Tyskland</Caption>
        </Image>
</FeaturedImages>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let svenska = ExtendedLanguageRange::new("sv").unwrap();
        let result: Vec<&str> = XmlPath::new(root, None)
            .all("Image")
            .all("Caption")
            .filter_lang_range(&svenska)
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(result.len(), 2);
        assert_eq!(result, vec!["Unga stjärnor bildas i skapelsens pelare, som ses av den nära-infraröda kameran på NASA:s James Webb-teleskopet",
        "Sjuprickig nyckelpiga (Coccinella septempunctata) i Tyskland"]);

        let chinese = ExtendedLanguageRange::new("zh").unwrap();
        let result: Vec<&str> = XmlPath::new(root, None)
            .all("Image")
            .all("Caption")
            .filter_lang_range(&chinese)
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(result.len(), 3);
        assert_eq!(
            result,
            vec!["陶达哈湖的普通鸬鹚（Phalacrocorax carbo）、白鹭（Egretta garzetta）和赤膀鸭（Mareca strepera），该湖位于尼泊尔加德满都附近",
                "埃塞尔比亚班纳部落的孩子们游玩木制高跷。",
                "埃塞尔比亚班纳部落的孩子们游玩木制高跷。"
            ]
        );

        let chinese_simplified = ExtendedLanguageRange::new("zh-Hans").unwrap();
        let result: Vec<&str> = XmlPath::new(root, None)
            .all("Image")
            .all("Caption")
            .filter_lang_range(&chinese_simplified)
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(result.len(), 1);
        assert_eq!(result, vec!["埃塞尔比亚班纳部落的孩子们游玩木制高跷。"]);
        Ok(())
    }

    #[test]
    fn first_matching_language_range() -> Result<(), Error> {
        let xml = "<FeaturedImages xml:lang=\"en\">
        <Note>Top five images from Wikimedia Commons Picture Of The Year 2022</Note>
        <Source>https://commons.wikimedia.org/wiki/Commons:Picture_of_the_Year/2022/Results</Source>
        <Image>
            <Filename>Phalacrocorax carbo, Egretta garzetta and Mareca strepera in Taudha Lake.jpg</Filename>
            <Caption>Great Cormorant (Phalacrocorax carbo), Little Egret (Egretta garzetta) and Gadwall (Mareca strepera) in Taudaha Lake, near Katmandu, Nepal</Caption>
            <Caption xml:lang='cs'>kormorán velký, volavka stříbřitá a kopřivka obecná na jezeře Taudaha na předměstí Káthmándú v Nepálu</Caption>
            <Caption xml:lang='de'>Großer Kormoran (Phalacrocorax carbo), Kleiner Reiher (Egretta garzetta) und Schnatterente (Mareca strepera) im Taudaha-See, bei Kathmandu, Nepal</Caption>
            <Caption xml:lang='pl'>Kormoran (Phalacrocorax carbo), czapla nadobna (Egretta garzetta) i krakwa (Mareca strepera) na jeziorze Taudaha, niedaleko Katmandu, Nepal</Caption>
            <Caption xml:lang='zh'>陶达哈湖的普通鸬鹚（Phalacrocorax carbo）、白鹭（Egretta garzetta）和赤膀鸭（Mareca strepera），该湖位于尼泊尔加德满都附近</Caption>
            <Caption xml:lang='es'>Cormorán grande (Phalacrocorax carbo), garceta común (Egretta garzetta) y ánade friso (Mareca strepera) en el lago Taudaha, cerca de Katmandú, Nepal.</Caption>
        </Image>
        <Image>
            <Filename>Ethiopia Banna tribe kids.jpg</Filename>
            <Caption>Kids of Banna tribe in Ethiopia playing on wooden stilts</Caption>
            <Caption xml:lang='zh-Hans'>埃塞尔比亚班纳部落的孩子们游玩木制高跷。</Caption>
            <Caption xml:lang='zh'>埃塞尔比亚班纳部落的孩子们游玩木制高跷。</Caption>
            <Caption xml:lang='tr'>Etiyopya'da Banna Kabilesi'ndeki vücutları gelneksel vücut boyasıyla boyanmış çocuklar tahtadan yapılmış cambaz ayaklıklarıyla oynuyorlar.</Caption>
        </Image>
        <Image>
            <Filename>Pillars of Creation (NIRCam Image).jpg</Filename>
            <Caption>Young stars form in \"The Pillars of Creation\" as seen by NASA's James Webb Space Telescope's near-infrared camera</Caption>
            <Caption xml:lang='cs'>Sloupy stvoření v Orlí mlhovině na snímku z Vesmírného dalekohledu Jamese Webba</Caption>
            <Caption xml:lang='fa'>ستون‌های آفرینش در سحابی عقاب که توسط تلسکوپ فضایی جیمز وب به تصویر کشیده شده است</Caption>
            <Caption xml:lang='pl'>\"Filary Stworzenia\" w Mgławicy Orzeł sfotografowane przez Kosmiczny Teleskop Jamesa Webba</Caption>
            <Caption xml:lang='de'>Sternentstehung im Adlernebel aufgenommen von der NASA mit einem James-Webb-Weltraumteleskop</Caption>
            <Caption xml:lang='es'>Se forman estrellas jóvenes en 'Los Pilares de la Creación', visto por la cámara de infrarrojo cercano del telescopio espacial James Webb de la NASA</Caption>
            <Caption xml:lang='sv'>Unga stjärnor bildas i skapelsens pelare, som ses av den nära-infraröda kameran på NASA:s James Webb-teleskopet</Caption>
        </Image>
        <Image>
            <Filename>Church of light.jpg</Filename>
            <Caption>Northern lights (Aurora borealis) over the Víkurkirkja church at Vík í Mýrdal, Iceland</Caption>
            <Caption xml:lang='de'>Polarlicht über der Kirche in Vík í Mýrdal in Island.</Caption>
            <Caption xml:lang='cs'>polární záře nad kostelem v islandské vesnici Vík í Mýrdal</Caption>
            <Caption xml:lang='fr'>Aurore boréale au-dessus de l'église Víkurkirkja à Vík í Mýrdal, Islande</Caption>
            <Caption xml:lang='pt'>Luzes do Norte (Aurora boreal) sobre a igreja Víkurkirkja em Vík í Mýrdal, Islândia</Caption>
        </Image>
        <Image>
            <Filename xml:lang='de'>Siebenpunkt-Marienkäfer (Coccinella septempunctata) auf Blüte im FFH-Gebiet \"Viernheimer Waldheide und angrenzende Flächen\".jpg</Filename>
            <Caption xml:lang='en'>Seven-spot ladybird (Coccinella septempunctata), taken in Viernheimer Waldheide Special Area of Conservation, Germany</Caption>
            <Caption xml:lang='fr'>Coccinelle à sept points (Coccinella septempunctata), prise dans la zone spéciale de conservation de Viernheimer Waldheide, Allemagne</Caption>
            <Caption xml:lang='uk'>Сонечко семиточкове (Coccinella septempunctata), виловлене в спеціальному заповіднику Viernheimer Waldheide, Німеччина</Caption>
            <Caption xml:lang='la'>Coccinella septempunctata</Caption>
            <Caption xml:lang='de'>Siebenpunkt-Marienkäfer (Coccinella septempunctata) auf Blüte im FFH-Gebiet</Caption>
            <Caption xml:lang='sv'>Sjuprickig nyckelpiga (Coccinella septempunctata) i Tyskland</Caption>
        </Image>
</FeaturedImages>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let english = ExtendedLanguageRange::new("en").unwrap();
        let result: Vec<&str> = XmlPath::new(root, None)
            .all("Image")
            .first("Caption")
            .filter_lang_range(&english)
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(result.len(), 5);

        let chinese = ExtendedLanguageRange::new("zh").unwrap();
        let result: Vec<&str> = XmlPath::new(root, None)
            .all("Image")
            .first("Caption")
            .filter_lang_range(&chinese)
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(result.len(), 2);
        assert_eq!(
            result,
            vec!["陶达哈湖的普通鸬鹚（Phalacrocorax carbo）、白鹭（Egretta garzetta）和赤膀鸭（Mareca strepera），该湖位于尼泊尔加德满都附近",
                "埃塞尔比亚班纳部落的孩子们游玩木制高跷。",
            ]
        );

        let chinese_simplified = ExtendedLanguageRange::new("zh-Hans").unwrap();
        let result: Vec<&str> = XmlPath::new(root, None)
            .all("Image")
            .first("Caption")
            .filter_lang_range(&chinese_simplified)
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(result.len(), 1);
        assert_eq!(result, vec!["埃塞尔比亚班纳部落的孩子们游玩木制高跷。"]);
        Ok(())
    }

    #[test]
    fn nth_matching_language_range() -> Result<(), Error> {
        let xml = "<Musings>
        <Day number='1'>
            <Thought xml:lang='en'>Is it raining?</Thought>
            <Thought xml:lang='sv'>Regnar det?</Thought>
            <Thought xml:lang='en'>Why can't I pay with cash?</Thought>
            <Thought xml:lang='sv'>Varför får jag inte betala kontant?</Thought>
        </Day>
        <Day number='2' xml:lang='en'>
            <Thought>What is the cat doing?</Thought>
            <Thought>Where have I seen her before?</Thought>
            <Thought>What time is it now?</Thought>
            <Thought>Is dinner ready yet?</Thought>
            <Thought>I've not practised my Swedish today.</Thought>
        </Day>
        <Day number='3'>
            <Thought xml:lang='en'>Gothenburg is a really nice place to visit.</Thought>
            <Thought xml:lang='sv'>Göteborg är ett riktigt trevligt ställe att besöka.</Thought>
            <Thought xml:lang='en'>My library card has expired.</Thought>
            <Thought xml:lang='sv'>Mitt lånekort har gått ut.</Thought>
            <Thought xml:lang='en'>How do you say Supercalifragilisticexpialidocious in Swedish?</Thought>
            <Thought xml:lang='sv'>Jag vet inte.</Thought>
        </Day>
</Musings>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let english = ExtendedLanguageRange::new("en").unwrap();
        let svenska = ExtendedLanguageRange::new("sv").unwrap();

        let result: Vec<&str> = XmlPath::new(root, None)
            .all("Day")
            .nth("Thought", 2)
            .filter_lang_range(&english)
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(result.len(), 2);
        assert_eq!(
            result,
            vec![
                "What time is it now?",
                "How do you say Supercalifragilisticexpialidocious in Swedish?"
            ]
        );

        let result: Vec<&str> = XmlPath::new(root, None)
            .all("Day")
            .nth("Thought", 0)
            .filter_lang_range(&svenska)
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(result.len(), 2);
        assert_eq!(
            result,
            vec![
                "Regnar det?",
                "Göteborg är ett riktigt trevligt ställe att besöka."
            ]
        );

        Ok(())
    }

    #[test]
    fn last_matching_language_range() -> Result<(), Error> {
        let xml = "<Musings>
        <Day number='1'>
            <Thought xml:lang='en'>Is it raining?</Thought>
            <Thought xml:lang='sv'>Regnar det?</Thought>
            <Thought xml:lang='en'>Why can't I pay with cash?</Thought>
            <Thought xml:lang='sv'>Varför får jag inte betala kontant?</Thought>
        </Day>
        <Day number='2' xml:lang='en'>
            <Thought>What is the cat doing?</Thought>
            <Thought>Where have I seen her before?</Thought>
            <Thought>What time is it now?</Thought>
            <Thought>Is dinner ready yet?</Thought>
            <Thought>I've not practised my Swedish today.</Thought>
        </Day>
        <Day number='3'>
            <Thought xml:lang='en'>Gothenburg is a really nice place to visit.</Thought>
            <Thought xml:lang='sv'>Göteborg är ett riktigt trevligt ställe att besöka.</Thought>
            <Thought xml:lang='en'>My library card has expired.</Thought>
            <Thought xml:lang='sv'>Mitt lånekort har gått ut.</Thought>
            <Thought xml:lang='en'>How do you say Supercalifragilisticexpialidocious in Swedish?</Thought>
            <Thought xml:lang='sv'>Jag vet inte.</Thought>
        </Day>
</Musings>";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();

        let english = ExtendedLanguageRange::new("en").unwrap();
        let svenska = ExtendedLanguageRange::new("sv").unwrap();

        let result: Vec<&str> = XmlPath::new(root, None)
            .all("Day")
            .last("Thought")
            .filter_lang_range(&english)
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(result.len(), 3);
        assert_eq!(
            result,
            vec![
                "Why can't I pay with cash?",
                "I've not practised my Swedish today.",
                "How do you say Supercalifragilisticexpialidocious in Swedish?"
            ]
        );

        let result: Vec<&str> = XmlPath::new(root, None)
            .all("Day")
            .last("Thought")
            .filter_lang_range(&svenska)
            .iter()
            .map(|e| e.text().unwrap())
            .collect();
        assert_eq!(result.len(), 2);
        assert_eq!(
            result,
            vec!["Varför får jag inte betala kontant?", "Jag vet inte."]
        );

        Ok(())
    }
}
