use std::fmt::Display;

use crate::common::XmlError;

/**
Represents an extended language range, for filtering of language tags attached to XML element
and attribute content.

See the formal definition of "extended language range" in
[RFC 4647: Matching of Language Tags](https://datatracker.ietf.org/doc/html/rfc4647).
*/
#[derive(Debug, Eq, PartialEq, Hash)]
pub struct ExtendedLanguageRange<'a> {
    subtags: Vec<&'a str>,
}

impl<'a> ExtendedLanguageRange<'a> {
    /**
    Creates and returns a new `ExtendedLanguageRange`.

    An `ExtendedLanguageRange` is used for filtering XML elements based on the explicit or
    inherited `xml:lang` language tag value which applies to those XML elements. See the method
    `XmlPath.filter_lang_range`.

    # Errors

    This method returns an `XmlError` if the given range is empty or invalid according to
    [RFC 4647: Matching of Language Tags](https://datatracker.ietf.org/doc/html/rfc4647).

    # Examples

    These are all valid according to
    [RFC 4647](https://datatracker.ietf.org/doc/html/rfc4647) and will return an
    `ExtendedLanguageRange`.

    ```
    # use spex::extraction::ExtendedLanguageRange;
    let english = ExtendedLanguageRange::new("en").unwrap();
    let english_britain = ExtendedLanguageRange::new("en-GB").unwrap();
    let english_usa = ExtendedLanguageRange::new("en-US").unwrap();
    let german_switzerland_96 = ExtendedLanguageRange::new("de-CH-1996").unwrap();
    let private_use_subtag = ExtendedLanguageRange::new("de-DE-x-goethe").unwrap();
    ```

    These are all invalid, and will return an `XmlError::InvalidLanguageRange`.

    ```
    # use spex::extraction::ExtendedLanguageRange;
    assert!(ExtendedLanguageRange::new("").is_err());
    assert!(ExtendedLanguageRange::new("-GB").is_err());
    assert!(ExtendedLanguageRange::new("subtagtoolong-SE").is_err());
    assert!(ExtendedLanguageRange::new("sv--x-whatever").is_err());
    ```
    */
    pub fn new(range: &'a str) -> Result<ExtendedLanguageRange<'a>, XmlError> {
        if range.is_empty() {
            return Err(XmlError::InvalidLanguageRange(
                "Extended language range cannot be empty."
                    .to_string()
                    .into_boxed_str(),
            ));
        }
        let subtags: Vec<&'a str> = range
            .split('-')
            .enumerate()
            // Wildcards in anything after the first subtag are
            // ignored/skipped during extended filtering matches,
            // so don't even bother to store them.
            .filter(|(index, s)| *index == 0 || *s != "*")
            .map(|(_, s)| s)
            .collect();
        let first_subtag = subtags.first().unwrap();
        if !first_subtag
            .chars()
            .all(|c| c.is_ascii_alphabetic() || c == '*')
        {
            return Err(XmlError::InvalidLanguageRange(
                "First subtag of extended language range must be ASCII alphabetic."
                    .to_string()
                    .into_boxed_str(),
            ));
        }
        if !(1..=8).contains(&first_subtag.len()) {
            return Err(XmlError::InvalidLanguageRange(
                "First subtag of extended language range must have length in [1, 8]."
                    .to_string()
                    .into_boxed_str(),
            ));
        }
        for i in 1..subtags.len() {
            let subsequent_subtag = subtags.get(i).unwrap();
            if !subsequent_subtag
                .chars()
                .all(|c| c.is_ascii_alphanumeric() || c == '*')
            {
                return Err(XmlError::InvalidLanguageRange(
                    "Subsequent subtag of extended language range must be ASCII alphanumeric."
                        .to_string()
                        .into_boxed_str(),
                ));
            }
            if !(1..=8).contains(&subsequent_subtag.len()) {
                return Err(XmlError::InvalidLanguageRange(
                    "Subsequent subtag of extended language range must have length in [1, 8]."
                        .to_string()
                        .into_boxed_str(),
                ));
            }
        }
        Ok(ExtendedLanguageRange { subtags })
    }

    pub(crate) fn subtags(&self) -> &[&'a str] {
        &self.subtags
    }
}

impl<'a> Display for ExtendedLanguageRange<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("ExtendedLanguageRange: \"")?;
        let mut need_separator = false;
        for subtag in &self.subtags {
            if need_separator {
                f.write_str("-")?;
            }
            f.write_str(subtag)?;
            need_separator = true;
        }
        f.write_str("\"")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    type TestOut = Result<(), XmlError>;

    #[test]
    fn empty_range() -> TestOut {
        let outcome = ExtendedLanguageRange::new("");
        assert!(matches!(outcome, Err(XmlError::InvalidLanguageRange(_))));
        Ok(())
    }

    #[test]
    fn range_initial_subtag_empty() -> TestOut {
        let outcome = ExtendedLanguageRange::new("-SE");
        assert!(matches!(outcome, Err(XmlError::InvalidLanguageRange(_))));
        Ok(())
    }

    #[test]
    fn range_contains_empty_subtags() -> TestOut {
        let outcome = ExtendedLanguageRange::new("sv--x-whatever");
        assert!(matches!(outcome, Err(XmlError::InvalidLanguageRange(_))));
        Ok(())
    }

    #[test]
    fn range_initial_subtag_overly_long() -> TestOut {
        let outcome = ExtendedLanguageRange::new("thatscifilanguage-Space-1999");
        assert!(matches!(outcome, Err(XmlError::InvalidLanguageRange(_))));
        Ok(())
    }

    #[test]
    fn range_contains_overly_long_subtags() -> TestOut {
        let outcome = ExtendedLanguageRange::new("gb-CORBLIMEYMARYPOPPINS");
        assert!(matches!(outcome, Err(XmlError::InvalidLanguageRange(_))));
        Ok(())
    }

    #[test]
    fn subtags_single() -> TestOut {
        let range = ExtendedLanguageRange::new("de")?;
        let subtags = range.subtags();
        assert_eq!(subtags.len(), 1);
        assert_eq!(subtags[0], "de");
        Ok(())
    }

    #[test]
    fn subtags_double() -> TestOut {
        let range = ExtendedLanguageRange::new("sv-SE")?;
        let subtags = range.subtags();
        assert_eq!(subtags.len(), 2);
        assert_eq!(subtags[0], "sv");
        assert_eq!(subtags[1], "SE");
        Ok(())
    }

    #[test]
    fn subtags_multi() -> TestOut {
        let range = ExtendedLanguageRange::new("en-a-bbb-x-a-ccc")?;
        let subtags = range.subtags();
        assert_eq!(subtags.len(), 6);
        assert_eq!(subtags[0], "en");
        assert_eq!(subtags[1], "a");
        assert_eq!(subtags[2], "bbb");
        assert_eq!(subtags[3], "x");
        assert_eq!(subtags[4], "a");
        assert_eq!(subtags[5], "ccc");
        Ok(())
    }

    #[test]
    fn wildcard() -> TestOut {
        let range = ExtendedLanguageRange::new("*")?;
        let subtags = range.subtags();
        assert_eq!(subtags.len(), 1);
        assert_eq!(subtags[0], "*");
        Ok(())
    }

    #[test]
    fn wildcard_in_middle() -> TestOut {
        let range = ExtendedLanguageRange::new("de-*-DE")?;
        let subtags = range.subtags();
        assert_eq!(subtags.len(), 2);
        assert_eq!(subtags[0], "de");
        assert_eq!(subtags[1], "DE");
        Ok(())
    }

    #[test]
    fn wildcard_at_end() -> TestOut {
        let range = ExtendedLanguageRange::new("de-*")?;
        let subtags = range.subtags();
        assert_eq!(subtags.len(), 1);
        assert_eq!(subtags[0], "de");
        Ok(())
    }

    #[test]
    fn display_wildcard() -> TestOut {
        let range = ExtendedLanguageRange::new("*")?;
        assert_eq!(format!("{}", range), "ExtendedLanguageRange: \"*\"");
        Ok(())
    }

    #[test]
    fn display_simple() -> TestOut {
        let range = ExtendedLanguageRange::new("sv")?;
        assert_eq!(format!("{}", range), "ExtendedLanguageRange: \"sv\"");
        Ok(())
    }

    #[test]
    fn display_regional() -> TestOut {
        let range = ExtendedLanguageRange::new("sv-SE")?;
        assert_eq!(format!("{}", range), "ExtendedLanguageRange: \"sv-SE\"");
        Ok(())
    }

    #[test]
    fn display_private() -> TestOut {
        let range = ExtendedLanguageRange::new("en-a-bbb-x-a-ccc")?;
        assert_eq!(
            format!("{}", range),
            "ExtendedLanguageRange: \"en-a-bbb-x-a-ccc\""
        );
        Ok(())
    }

    #[test]
    fn display_sub_wildcard() -> TestOut {
        let range = ExtendedLanguageRange::new("en-*-GB")?;
        assert_eq!(format!("{}", range), "ExtendedLanguageRange: \"en-GB\"");
        Ok(())
    }
}
