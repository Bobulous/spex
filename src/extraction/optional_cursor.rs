use crate::{
    common::XmlError, extraction::MixedCursor, extraction::NameRef, extraction::PresetNamespace,
    xml::Element,
};

pub struct OptionalCursor<'a> {
    default_namespace: Option<PresetNamespace>,
    status: Status<'a>,
}

pub(crate) enum Status<'a> {
    Found(&'a Element),
    Absent,
}

impl<'a> OptionalCursor<'a> {
    pub(crate) fn new(e: &'a Element, default_namespace: Option<PresetNamespace>) -> Self {
        Self {
            default_namespace,
            status: Status::Found(e),
        }
    }

    pub(crate) fn status(self) -> Status<'a> {
        self.status
    }

    pub(crate) fn absent() -> Self {
        Self {
            default_namespace: None,
            status: Status::Absent,
        }
    }

    pub(crate) fn default_namespace(&self) -> Option<PresetNamespace> {
        self.default_namespace
    }

    /**
    Sets the preset namespace name which will be used from this point forward in this method
    chain by calls to methods such as `req`, `opt`, `first`, `all`, etc, so that they know which
    namespace name should be presumed if no explicit namespace name is provided when calling
    those methods as part of this chain.
    */
    pub fn pre_ns(mut self, default_namespace: &'static str) -> Self {
        if let Status::Found(_) = self.status {
            let default_namespace = if default_namespace.is_empty() {
                None
            } else {
                Some(PresetNamespace::new(default_namespace))
            };
            self.default_namespace = default_namespace;
        }
        self
    }

    /**
    Points this cursor at the **first** child element (of the element which the cursor is
    currently pointing at) with the given name, if such an element exists. Otherwise marks the
    cursor as having an "acceptably absent" state.

    Note that if the state of the cursor was already "absent" before calling this method, then
    the state will remain the way it was, whether or not the named child element exists.
    */
    pub fn opt<'b, N>(mut self, full_name: N) -> Self
    where
        N: Into<NameRef<'b>>,
    {
        if let Status::Found(e) = self.status {
            match e.o_child((full_name.into(), self.default_namespace)) {
                Some(c) => self.status = Status::Found(c),
                None => self.status = Status::Absent,
            }
        }
        self
    }

    /**
    Converts this cursor into a `MixedCursor`, then points it at the **first** child element (of
    the element which the cursor is currently pointing at) with the given name if such an
    element exists, otherwise marks the cursor as having an "invalid/error" state.

    Note that if the state of the cursor was already "absent" before calling this method, then
    the state will remain the way it was, whether or not the named child element exists.
    */
    pub fn req<'b, N>(self, full_name: N) -> MixedCursor<'a>
    where
        N: Into<NameRef<'b>>,
    {
        let mixed_cursor = MixedCursor::from_opt(self);
        mixed_cursor.req(full_name)
    }

    /**
    Returns a reference to the element which this cursor is ponting at if it exists, or returns
    `None` if the cursor state is "absent" (meaning that the cursor was pointing to an
    element which exists and then `opt` was called with a non-existent child element).
    */
    pub fn element(self) -> Option<&'a Element> {
        match self.status {
            Status::Found(e) => Some(e),
            Status::Absent => None,
        }
    }

    /**
    Returns the text content of the **optional** attribute (within the element at which this
    cursor is pointing) with the given name, or returns `None` if the attribute does not exist
    or if the cursor state is "absent" (meaning that the cursor was pointing to an element which
    exists and then `opt` was called with a non-existent child element).
    */
    pub fn att_opt<'b, N>(self, att_name: N) -> Option<&'a str>
    where
        N: Into<NameRef<'b>>,
    {
        match self.status {
            Status::Found(e) => e.att_opt((att_name.into(), self.default_namespace)),
            Status::Absent => None,
        }
    }

    /**
    Returns the text content of the **required** attribute (within the element at which this
    cursor is pointing) with the given name, or returns `None` if the cursor state is "absent"
    (meaning that the cursor was pointing to an element which exists and then `opt` was called
    with a non-existent child element), or returns an `XmlError` if the attribute does not
    exist.

    # Errors

    This method will return an `XmlError` if the cursor target element exists but the required
    attribute is not found.
    */
    pub fn att_req<'b, N>(self, att_name: N) -> Result<Option<&'a str>, XmlError>
    where
        N: Into<NameRef<'b>>,
    {
        match self.status {
            Status::Found(e) => Ok(Some(e.att_req((att_name.into(), self.default_namespace))?)),
            Status::Absent => Ok(None),
        }
    }

    /**
    Returns the text content of the element (at which this cursor is pointing), or returns an
    `XmlError` if the element does not contain simple content, or returns `None` if the
    cursor state is "absent" (meaning that the cursor was pointing to an element which exists
    and then `opt` was called with a non-existent child element).

    # Errors

    This method will return an `XmlError` if the cursor target element exists but contains
    non-simple content (child elements or processing instructions).
    */
    pub fn text(self) -> Result<Option<&'a str>, XmlError> {
        match self.status {
            Status::Found(e) => match e.text() {
                Ok(t) => Ok(Some(t)),
                Err(m) => Err(m),
            },
            Status::Absent => Ok(None),
        }
    }
}

#[cfg(test)]
mod tests {

    use std::io::Error;

    use crate::parsing::XmlReader;
    use crate::xml::ElementBuilder;
    use crate::xml::Name;
    use crate::xml::XmlDocument;

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    fn parse_raw_xml(raw_xml: &str) -> Result<XmlDocument, Error> {
        let bytes = raw_xml.as_bytes();
        XmlReader::parse_auto(bytes)
    }

    #[test]
    fn empty_root() -> Result<(), Error> {
        let root = Element::new(ElementBuilder::new(Name::new("root", "")));
        let cursor = root.opt("nonexistent");
        assert!(cursor.element().is_none());
        assert_eq!(root.opt("nonexistent").text().unwrap(), None);
        Ok(())
    }

    #[test]
    fn root_with_child() -> Result<(), Error> {
        let xml = "
<root>
    <child>text</child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let child = root.opt("child").element();
        assert!(child.is_some());
        let child = child.unwrap();
        assert_eq!(child.name().local_part(), "child");
        assert_eq!(root.opt("child").text().unwrap(), Some("text"));

        Ok(())
    }

    #[test]
    fn root_with_grandchild() -> Result<(), Error> {
        let xml = "
<root>
    <child>
        <grandchild>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let grandchild = root.opt("child").opt("grandchild").element();
        assert!(grandchild.is_some());
        let grandchild = grandchild.unwrap();
        assert_eq!(grandchild.name().local_part(), "grandchild");
        assert_eq!(grandchild.name().namespace(), None);
        assert_eq!(
            root.opt("child").opt("grandchild").text().unwrap(),
            Some("text")
        );

        Ok(())
    }

    #[test]
    fn root_without_grandchild() -> Result<(), Error> {
        let xml = "
<root>
    <child>text</child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let grandchild = root.opt("child").opt("grandchild").element();
        assert!(grandchild.is_none());
        assert_eq!(root.opt("child").opt("grandchild").text().unwrap(), None);

        Ok(())
    }

    #[test]
    fn namespaces_explicit() -> Result<(), Error> {
        let xml = "
<root xmlns='example.com/ns'>
    <child>
        <grandchild>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let grandchild = root
            .opt(("child", "example.com/ns"))
            .opt(("grandchild", "example.com/ns"))
            .element();
        assert!(grandchild.is_some());
        let grandchild = grandchild.unwrap();
        assert_eq!(grandchild.name().local_part(), "grandchild");
        assert_eq!(grandchild.name().namespace(), Some("example.com/ns"));

        // Without specifying the namespace, nothing should be found.
        assert!(root.opt("child").opt("grandchild").element().is_none());

        Ok(())
    }

    #[test]
    fn namespaces_using_preset() -> Result<(), Error> {
        let xml = "
<a xmlns='example.com/ns1'>
    <b>
        <c xmlns='example.com/ns2'>
            <d>
                <e xmlns='example.com/ns3'>
                    <f>text</f>
                </e>
            </d>
        </c>
    </b>
</a>
";
        let doc = parse_raw_xml(xml)?;
        let alpha = doc.root();
        let foxtrot = alpha
            .pre_ns("example.com/ns1")
            .opt("b")
            .pre_ns("example.com/ns2")
            .opt("c")
            .opt("d")
            .pre_ns("example.com/ns3")
            .opt("e")
            .opt("f")
            .element();
        assert!(foxtrot.is_some());
        let foxtrot = foxtrot.unwrap();
        assert_eq!(foxtrot.text().unwrap(), "text");

        let foxtrot_text = alpha
            .pre_ns("example.com/ns1")
            .opt("b")
            .pre_ns("example.com/ns2")
            .opt("c")
            .opt("d")
            .pre_ns("example.com/ns3")
            .opt("e")
            .opt("f")
            .text()
            .unwrap();
        assert_eq!(foxtrot_text, Some("text"));

        Ok(())
    }

    #[test]
    fn att_opt() -> Result<(), Error> {
        let xml = "
<root>
    <child id='1'>
        <grandchild id='2'>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let child_id = root.opt("child").att_opt("id");
        assert!(child_id.is_some());
        assert_eq!(child_id.unwrap(), "1");

        let no_such_att = root.opt("child").att_opt("mode");
        assert!(no_such_att.is_none());

        let no_child_id = root.opt("sibling").att_opt("id");
        assert!(no_child_id.is_none());

        let grandchild_id = root.opt("child").opt("grandchild").att_opt("id");
        assert!(grandchild_id.is_some());
        assert_eq!(grandchild_id.unwrap(), "2");

        let no_such_att = root.opt("child").opt("grandchild").att_opt("mode");
        assert!(no_such_att.is_none());

        let no_grandchild_id = root.opt("child").opt("disinherited").att_opt("id");
        assert!(no_grandchild_id.is_none());
        Ok(())
    }

    #[test]
    fn att_req() -> Result<(), Error> {
        let xml = "
<root>
    <child id='1'>
        <grandchild id='2'>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let child_id = root.opt("child").att_req("id");
        assert!(child_id.is_ok());
        assert_eq!(child_id.unwrap(), Some("1"));

        let no_such_att = root.opt("child").att_req("mode");
        assert!(no_such_att.is_err());

        let no_child_id = root.opt("sibling").att_req("id");
        assert!(no_child_id.is_ok());
        assert!(no_child_id.unwrap().is_none());

        let grandchild_id = root.opt("child").opt("grandchild").att_req("id");
        assert!(grandchild_id.is_ok());
        assert_eq!(grandchild_id.unwrap(), Some("2"));

        let no_such_att = root.opt("child").opt("grandchild").att_req("mode");
        assert!(no_such_att.is_err());

        let no_grandchild_id = root.opt("child").opt("disinherited").att_req("id");
        assert!(no_grandchild_id.is_ok());
        assert!(no_grandchild_id.unwrap().is_none());
        Ok(())
    }

    #[test]
    fn att_opt_namespaces() -> Result<(), Error> {
        let xml = "
<root xmlns:p='example.com/p'>
    <child p:id='1'>
        <grandchild p:id='2'>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let child_id = root.opt("child").pre_ns("example.com/p").att_opt("id");
        assert!(child_id.is_some());
        assert_eq!(child_id.unwrap(), "1");

        let namespace_missing = root.opt("child").att_opt("id");
        assert!(namespace_missing.is_none());

        let grandchild_id = root
            .opt("child")
            .opt("grandchild")
            .att_opt(("id", "example.com/p"));
        assert!(grandchild_id.is_some());
        assert_eq!(grandchild_id.unwrap(), "2");

        let namespace_missing = root.opt("child").opt("grandchild").att_opt("id");
        assert!(namespace_missing.is_none());
        Ok(())
    }

    #[test]
    fn att_req_namespaces() -> Result<(), Error> {
        let xml = "
<root xmlns:p='example.com/p'>
    <child p:id='1'>
        <grandchild p:id='2'>text</grandchild>
    </child>
</root>
";
        let doc = parse_raw_xml(xml)?;
        let root = doc.root();
        let child_id = root.opt("child").pre_ns("example.com/p").att_req("id");
        assert!(child_id.is_ok());
        assert_eq!(child_id.unwrap(), Some("1"));

        let namespace_missing = root.opt("child").att_req("id");
        assert!(namespace_missing.is_err());

        let grandchild_id = root
            .opt("child")
            .opt("grandchild")
            .att_req(("id", "example.com/p"));
        assert!(grandchild_id.is_ok());
        assert_eq!(grandchild_id.unwrap(), Some("2"));

        let namespace_missing = root.opt("child").opt("grandchild").att_req("id");
        assert!(namespace_missing.is_err());
        Ok(())
    }
}
