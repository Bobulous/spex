use std::{fs::File, io::Error};

use spex::{extraction::ExtendedLanguageRange, parsing::XmlReader, xml::XmlDocument};

const NS: &str = "example.com/i18n";

fn main() -> Result<(), Error> {
    let properties = Properties::load("examples/i18n.xml")?;

    let english = ExtendedLanguageRange::new("en").unwrap();
    let svenska = ExtendedLanguageRange::new("sv").unwrap();

    // "en" will match with both xml:lang="en" and xml:lang="en_GB", but our get_value method
    // will return the first matching entry it finds, and our XML lists the general "en" entry
    // first, so that's the one we expect to get back.
    assert_eq!(properties.get_value("button_ok", &english), Some("ok"));
    assert_eq!(
        properties.get_value("button_cancel", &svenska),
        Some("avbryt")
    );
    assert_eq!(
        properties.get_value("option_discard", &svenska),
        Some("släng")
    );
    assert_eq!(
        properties.get_value("option_continue", &english),
        Some("continue")
    );

    // Now check that more-specific language entries can also be retrieved.
    // "en-GB" is too specific to match with xml:lang="en" so it will skip the general entry.
    let english_brit = ExtendedLanguageRange::new("en-GB").unwrap();
    assert_eq!(
        properties.get_value("button_ok", &english_brit),
        Some("whatevs")
    );
    assert_eq!(
        properties.get_value("option_continue", &english_brit),
        Some("go on then")
    );

    // For a language which is not yet defined in our XML file, we expect None to be returned.
    let dansk = ExtendedLanguageRange::new("dk").unwrap();
    assert_eq!(properties.get_value("option_continue", &dansk), None);

    Ok(())
}

struct Properties {
    doc: XmlDocument,
}

impl Properties {
    fn load(filename: &str) -> Result<Properties, Error> {
        let file = File::open(filename)?;
        let doc = XmlReader::parse_auto(file)?;
        Ok(Properties { doc })
    }

    fn get_value(&self, handle: &str, lang_range: &ExtendedLanguageRange<'_>) -> Option<&str> {
        self.doc
            .root()
            // We want to iterate all elements which are found on a specific path which
            // satisfies element name checks and attribute checks, so we can create an XmlPath.
            // There is only one "parts" element, so we may as well use `first`, which will give
            // us an XmlPath object which will begin by looking for the first "parts" element
            // of this root element.
            .first(("parts", NS))
            // Next, we want to *consider* all of the elements named "parts", so we use `all`
            // which will look for all elements named "part" which are children of "parts".
            .all(("part", NS))
            // We only want the "part" element which has an "id" attribute whose label is
            // identical to the given handle.
            .with_attribute("id", handle)
            // We want to *consider* all elements named "label", so we use `all` to ask the
            // XmlPath to look for all "label" elements which are child elements of any "part"
            // element that has matched the criteria so far.
            .all(("label", NS))
            // But we only want the "label" elements which have an xml:lang which satisfies the
            // given extended language range.
            .filter_lang_range(lang_range)
            // Now create an iterator over references to every Element which fits the criteria
            // of the XmlPath.
            .iter()
            // We only want one "label" element (whatever is the first to satisfy all of the
            // above requirements) so use `next()` to grab only the first Element from the
            // iterator.
            .next()
            // And we only actually want the text content from the matching "label" element,
            // so map from Option<&Element> to Option<&str> and let that be returned.
            .and_then(|e| e.text().ok())
    }
}
