use std::{fs::File, io::Error};

use spex::{common::XmlError, parsing::XmlReader, xml::Element, xml::XmlDocument};

fn main() -> Result<(), String> {
    // Attempt to produce an XmlDocument from the XML within the specified file.
    match read_file_as_xml("examples/EventCalendar.xml") {
        Err(file_err) => Err(format!(
            "Something went wrong while trying to read the XML file: {}.",
            file_err
        )),
        Ok(xml_doc) => {
            // Grab the root Element from the document. This will represent the "CalendarRS" root
            // element of the XML document within the file.
            let root = xml_doc.root();

            // Now let's move extraction into a new method, so that it can handle the new type of
            // Result error (XmlError), because this main method is only expecting to emit errors of
            // type &str, and that will stop us using the question-mark syntax to handle
            // XmlError errors returned by the methods offered by the Element type.
            match print_events_to_console(root) {
                Ok(()) => {
                    println!("Extraction completed without error!");
                }
                Err(extract_error) => {
                    return Err(format!(
                        "Something was not as expected while extracting from the XML tree: {}",
                        extract_error
                    ));
                }
            }

            Ok(())
        }
    }
}

fn read_file_as_xml(filename: &str) -> Result<XmlDocument, Error> {
    // Open the file which contains the XML content.
    let xml_file = File::open(filename)?;
    // Now use XmlReader to parse the XML content of the file and produce an XmlDocument.
    let xml_doc = XmlReader::parse_auto(xml_file)?;
    Ok(xml_doc)
}

fn print_events_to_console(root: &Element) -> Result<(), XmlError> {
    let event_list = root.req("EventList").element()?;
    for event in event_list.elements().filter(|e| e.is_named("Event")) {
        // The XML unfortunately uses different element names for different event types, but
        // the XML structure within each is identical. So just check which element name we
        // find and then hold that element in a non-specifically named `event_type` variable.
        let event_type = if let Some(journey) = event.opt("Journey").element() {
            println!("#########################");
            println!("Found a journey event!");
            journey
        } else if let Some(festival) = event.opt("Festival").element() {
            println!("#########################");
            println!("Found a festival event!");
            festival
        } else if let Some(club) = event.opt("Club").element() {
            println!("#########################");
            println!("Found a club event!");
            club
        } else {
            println!("######################");
            println!("Found an unexpected event type!");
            return Ok(());
        };

        let name = event.req("Name").text()?;
        println!("Event name: {}", name);

        // Now we want to grab the (initial) location and start date. The XML has a deep
        // structure, forcing us to descend into several child elements to get to the actual
        // data. Rather than capturing each element in a variable, we can just chain together
        // calls to `req` to reach down to the element of interest. Note that according to the
        // design of this XML document, both the "Start" and "From" elements are required, so
        // we use `req` to point to each of them. Because this chain of cursor methods involves
        // `req`, we put a `?` after the call to `element()` so that if any of the required
        // elements is not found then an error will be thrown.
        let start_from = event_type.req("Start").req("From").element()?;

        // Now that we're holding the "From" element, and we just want the text from the two
        // child elements, we can use the `req` then `text()` methods to grab the text content
        // (or throw an error), without bothering to assign the elements to new variables.
        let start_location = start_from.req("Location").text()?;
        println!("Start location is {}", start_location);
        let start_date = start_from.req("Date").text()?;
        println!("Start date is {}", start_date);

        // According to the design of this XML document, the "End" element is optional, so we
        // now need to use the `opt` method to point to it. But if the "End" element exists,
        // then it must contain an "At" method, so we use `req` to point to the "At" method to
        // indicate that we consider it an error if "At" is not found within "End". Because this
        // chain of cursor methods involves both `opt` and `req` we need the `?` after
        // `element()` to throw an error if any required element is missing, and we also need to
        // use `if let Some(end_at)` to skip this block of code if any optional element is
        // missing.
        if let Some(end_at) = event_type.opt("End").req("At").element()? {
            let end_location = end_at.req("Location").text()?;
            println!("End location is {}", end_location);
            let end_date = end_at.req("Date").text()?;
            println!("End date is {}", end_date);
        }

        // The XML document design inexplicably states that "Availability" and all of its
        // descendants are optional, so we only need to use `opt`. Because we're only using
        // `opt` we need to use `if let Some(availability)` to skip this code if any of the
        // elements is missing, but we do not need to use `?` after `element()` because it is
        // not an error if optional elements are missing (and `req` is not used in this method
        // chain).
        if let Some(availability) = event
            .opt("Availability")
            .opt("RemainingAvailability")
            .element()
        {
            // Note, however, that we always have to use `?` after `text()` because that method
            // will return an error if the content of the element contains child elements or
            // processing instructions.
            if let Some(min_avail) = availability.opt("MinQty").text()? {
                println!("Only {} tickets remaining! Buy now!!!", min_avail);
            }
        }

        // Price is also nested absurdly deeply. The entire branch is required by the XML design
        // so just use `req` all the way, and don't forget the `?` after `element()` to check
        // for an error outcome.
        let price = event
            .req("Price")
            .req("PerPerson")
            .req("IncTax")
            .element()?;
        println!(
            "The price is {}{} per person, inclusive of taxes.",
            price.req("Amount").text()?,
            price.req("Currency").text()?
        );

        println!(
            "The supplier name is: {}",
            event.req("Supplier").req("Company").req("Name").text()?
        );
    }
    Ok(())
}
